import {imagePipe} from '../pipes/image-pipe.js';
import {parsPipe} from '../pipes/pars-pipe.js';
import {MoviesService} from '../services/movies-service.js';

class SlideShowPars extends HTMLElement {
    constructor() {
		super();

		/** @type {Array<Array<import("../services/movies-service.js").Pelicula>>} */
		this.movies = [];
        this.movieService = new MoviesService();
	}

	set _movies(movies = []) {
		this.movies = parsPipe(movies);
        
        // ocultamos el swiper para que pueda construir el contenido
        // sin deformar la pantalla
        this.swiperContainer.style.display = 'none';
        this.swiperContainer.innerHTML = this._movies.map((arrayMovies) => (`
            <swiper-slide>
                <ion-row>
                    ${arrayMovies.map(movie => (`
                        <ion-col size="12">
                            <ion-card movie-id="${movie.id}" class="poster">
                                <img src="${imagePipe(movie.poster_path)}" />
                            </ion-card> 
                        </ion-col>
                    `)).join('')}
                </ion-row>
            </swiper-slide>
        `)).join('');
        
        // colocamos el boton de buscar mas al final del array
        this.swiperContainer.innerHTML += (`
            <swiper-slide class="slide-more">
                <ion-button size="large" class="btn-more">
                    <ion-icon name="add" slot="icon-only"></ion-icon>
                </ion-button>
            </swiper-slide>
        `);

        // una vez terminado mostramos la estructura
        this.swiperContainer.style.display = '';

        const button = this.swiperContainer.querySelector('ion-button');
        if (!button) return;

        // evento para solicitar mas peliculas
        button.addEventListener('click', () => {
            const event = new CustomEvent('load-more', {
                bubbles: false,
                detail: {}
            });

            this.dispatchEvent(event);
        });

        // evento de detalle
        const cards = Array.from(this.swiperContainer.querySelectorAll('ion-card'));
        if (cards.length === 0) return;

        cards.forEach(card => {
            const movieId = card.getAttribute('movie-id');
            card.addEventListener('click', this.openDetail.bind(this, movieId));
        });
	}

	get _movies() {
		return this.movies;
	}

	connectedCallback() {
		this.innerHTML = (`
			<swiper-container free-mode slides-per-view="3.3"></swiper-container>
		`);

		this.render();
	}

	render() {
		this.swiperContainer = this.querySelector('swiper-container');
        if (!this.swiperContainer) return;
	}

	async openDetail(movieId) {
        try {
            const [movieDetail, movieCredits] = await Promise.all([
				this.movieService.getMovieDetail(movieId), 
				this.movieService.getMovieCredits(movieId),
			]);
			
			const modalController = Object.assign(document.createElement('ion-modal'), {
				backdropDismiss: false,
				component: 'movie-detail',
				htmlAttributes: { // pasamos los props al ion-modal
					movieDetail,
					movieCredits				
				}
			});
    
            this.appendChild(modalController);
    
            await modalController.present();
            await modalController.onDidDismiss();
    
            modalController.remove();

        } catch (error) {
            console.error(error);
        }
	}
}

window.customElements.define('slideshow-pars', SlideShowPars);