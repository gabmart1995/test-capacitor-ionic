import {imagePipe} from '../pipes/image-pipe.js';
import {StorageService} from '../services/storage-service.js';

class MovieDetail extends HTMLElement {
    constructor() {
        super();

        /** @type {import("../services/movies-service.js").PeliculaDetalle | null} */
        this.movie = null;

        /** @type {import("../services/movies-service.js").RespuestaCredits | null} */
        this.credits = null;
        
        /** @type {HTMLElement | null} */
        this.modalController = null;
        
        this.hide = true;
        this.existsInFavorite = false;
        
        this.storageService = StorageService.getInstance();
    }

    set _existsInFavorite(existsInFavorite = false) {
        this.existsInFavorite = existsInFavorite;

        const icon = this.querySelector('#favorites > ion-icon');
        if (!icon) return;

        icon.name = this.existsInFavorite ? 'star' : 'star-outline'; 
    }

    connectedCallback() {
        // recogemos los atributos del padre antes de renderizar
        this.modalController = this.parentElement;
        if (this.modalController) {
            this.movie = this.modalController.htmlAttributes['movieDetail'];
            this.credits = this.modalController.htmlAttributes['movieCredits'];
        }
        
        // luego cargamos el contenido
        this.innerHTML = (`
            <ion-content>
                <ion-label class="titulo">
                    <h1>${this.movie.title}</h1>
                </ion-label>

                <!-- imagen de fondo -->
                ${this.movie.backdrop_path ? (`<img alt="${this.movie.title}" src="${imagePipe(this.movie.backdrop_path)}" />`) : ''}
                
                <ion-grid>
                    <ion-row>
                        <ion-col size="4" class="poster-detalle-sobre">
                            <ion-card class="poster">
                                <img src="${imagePipe(this.movie.poster_path)}"  />
                            </ion-card>
                        </ion-col>
                        <ion-col>
                            <ion-item>
                                <ion-icon slot="start" color="primary" name="thumbs-up"></ion-icon>
                                <ion-label>Rating:</ion-label>
                                <ion-note slot="end" color="primary">
                                    ${this.movie.vote_average}
                                </ion-note>
                            </ion-item>
                            <ion-item>
                                <ion-icon slot="start" color="primary" name="ribbon"></ion-icon>
                                <ion-label>Votos:</ion-label>
                                <ion-note slot="end" color="primary">
                                    ${this.movie.vote_count}
                                </ion-note>
                            </ion-item>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                
                <!-- overview -->
                <ion-grid fixed>
                    <ion-row>
                        <ion-col size="12">
                            <ion-label id="overview">${this.movie.overview.slice(0, 150)} ...</ion-label>
                            <ion-label id="show-more" color="primary">Leer más</ion-label>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            ${this.movie.genres.map(genre => (`
                                <ion-chip color="primary">
                                    <ion-label>${genre.name}</ion-label>
                                </ion-chip>
                            `)).join('')}
                        </ion-col>
                    </ion-row>
                </ion-grid>

                <ion-grid>
                    <ion-row>
                        <ion-col>
                            <ion-label>Casting:</ion-label>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <swiper-container slides-per-view="3.3" freemode space-beetween="-5">
                    ${this.credits.cast.map(cast => (`
                        <swiper-slide>
                            <ion-card class="card-actor">
                               ${cast.profile_path ? 
                                    (`<img src="${imagePipe(cast.profile_path)}" />`) : 
                                    (`<img src="assets/no-avatar.jpg" />`)} 
                                <ion-card-content>
                                    <ion-label>${cast.name}</ion-label>
                                </ion-card-content> 
                            </ion-card>
                        </swiper-slide>
                    `)).join('')}
                </swiper-container>

            </ion-content>

            <ion-footer class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-button color="primary" id="back">
                            <ion-icon name="arrow-back-outline" slot="start"></ion-icon>
                            <ion-label>Regresar</ion-label>
                        </ion-button>
                    </ion-buttons>
                    <ion-buttons slot="end">
                        <ion-button id="favorites">
                            <ion-icon name="star-outline" slot="end"></ion-icon>
                            <ion-label>Favorito</ion-label>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-footer>
        `);

        this.onMount();
    }

    async onMount() {
        const buttonClose = this.querySelector('#back');
        if (!buttonClose) return;

        const favoriteButton = this.querySelector('#favorites');
        if (!favoriteButton) return;

        const showMore = this.querySelector('#show-more');
        if (!showMore) return;
        
        showMore.addEventListener('click', this.deployText.bind(this));
        buttonClose.addEventListener('click', this.closeModal.bind(this));
        favoriteButton.addEventListener('click', this.setFavorite.bind(this));

        // muestra la informacion de favoritos
        this._existsInFavorite = await this.storageService.existMovie(this.movie.id);
    }

    deployText() {
        this.hide = !this.hide;

        const overview = this.querySelector('#overview');
        const showMore = this.querySelector('#show-more');

        if (!overview || !showMore) return;

        if (!this.hide) {
            overview.innerText = this.movie.overview;
            showMore.style.display = 'none';
        } 
    }

    async setFavorite() {
        try {
            const message = await this.storageService.saveMovie(this.movie);
            const toastController = Object.assign(document.createElement('ion-toast'), {
                message,
                duration: 1500,
                position: 'bottom'
            });

            this._existsInFavorite = await this.storageService.existMovie(this.movie.id);
            
            this.appendChild(toastController);

            await toastController.present();
            await toastController.onDidDismiss();

            toastController.remove();


        } catch (error) {
            console.error(error);
        }
    }

    closeModal() {
        if (this.modalController) {
            this.modalController.dismiss();
        }
    }
}

window.customElements.define('movie-detail', MovieDetail);