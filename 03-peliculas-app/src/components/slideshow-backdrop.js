import {imagePipe} from '../pipes/image-pipe.js';
import {MoviesService} from '../services/movies-service.js';

class SlideShowBackdrop extends HTMLElement {
	constructor() {
		super();

		/** @type {Array<import("../services/movies-service.js").Pelicula>} */
		this.movies = [];
		this.movieService = new MoviesService();
		this.swiperContainer = null;
	}

	set _movies(movies = []) {
		this.movies = movies;
		
        this.swiperContainer.innerHTML = this._movies
			.filter(movie => movie.backdrop_path)
			.map(movie => (`
				<swiper-slide>
					<ion-card movie-id="${movie.id}">
						<img src="${imagePipe(movie.backdrop_path)}" />
					</ion-card> 
				</swiper-slide>
			`)).join('');

		const cards = Array.from(this.querySelectorAll('ion-card'));
		if (cards.length === 0) return;
		
		cards.forEach(card => {
			const movieId = card.getAttribute('movie-id')
			card.addEventListener('click', this.openDetail.bind(this, movieId));
		});
	}

	get _movies() {
		return this.movies;
	}

	connectedCallback() {
		this.innerHTML = (`
			<swiper-container free-mode slides-per-view="1.1"></swiper-container>
		`);

		this.render();
	}

	render() {
		this.swiperContainer = this.querySelector('swiper-container');
        if (!this.swiperContainer) return;
	}

	async openDetail(movieId) {
		try {
			const [movieDetail, movieCredits] = await Promise.all([
				this.movieService.getMovieDetail(movieId), 
				this.movieService.getMovieCredits(movieId),
			]);
			
			const modalController = Object.assign(document.createElement('ion-modal'), {
				backdropDismiss: false,
				component: 'movie-detail',
				htmlAttributes: { // pasamos los props al ion-modal
					movieDetail,
					movieCredits				
				}
			});

			this.appendChild(modalController);
	
			await modalController.present();
			await modalController.onDidDismiss();
	
			modalController.remove();

        } catch (error) {
            console.error(error);
        }
	}
}

window.customElements.define('slideshow-backdrop', SlideShowBackdrop);
	