import {imagePipe} from '../pipes/image-pipe.js';
import {MoviesService} from '../services/movies-service.js';

class SlideShowPoster extends HTMLElement {
	constructor() {
		super();

		/** @type {Array<import("../services/movies-service.js").Pelicula> | Array<import('../services/movies-service.js').PeliculaDetalle>} */
		this.movies = [];
		this.movieService = new MoviesService();
	}

	set _movies(movies = []) {
		this.movies = movies;
        this.swiperContainer.innerHTML = this._movies.map(movie => (`
            <swiper-slide>
               <ion-card movie-id="${movie.id}" class="poster">
                    <img src="${imagePipe(movie.poster_path)}" />
               </ion-card> 
            </swiper-slide>
        `)).join('');

		const cards = Array.from(this.querySelectorAll('ion-card'));
		if (cards.length === 0) return;
		
		cards.forEach(card => {
			const movieId = card.getAttribute('movie-id')
			card.addEventListener('click', this.openDetail.bind(this, movieId));
		});
	}

	get _movies() {
		return this.movies;
	}

	connectedCallback() {
		this.innerHTML = (`
			<swiper-container free-mode slides-per-view="3.3"></swiper-container>
		`);

		this.render();
	}

	render() {
		this.swiperContainer = this.querySelector('swiper-container');
        if (!this.swiperContainer) return;
	}

	/**
     * abre el modal del detalle
     * @param {string} movieId identificador de la pelicula
     */
	async openDetail(movieId) {
        try {
            const [movieDetail, movieCredits] = await Promise.all([
				this.movieService.getMovieDetail(movieId), 
				this.movieService.getMovieCredits(movieId),
			]);
			
			const modalController = Object.assign(document.createElement('ion-modal'), {
				backdropDismiss: false,
				component: 'movie-detail',
				htmlAttributes: { // pasamos los props al ion-modal
					movieDetail,
					movieCredits				
				}
			});
    
            this.appendChild(modalController);
    
            await modalController.present();
            await modalController.onDidDismiss();
    
            modalController.remove();

        } catch (error) {
            console.error(error);
        }
	}
}

window.customElements.define('slideshow-poster', SlideShowPoster);
	