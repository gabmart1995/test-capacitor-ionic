class PageInit extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-app>
                <ion-tabs>
                    <ion-tab tab="movies" component="page-movies"></ion-tab>
                    <ion-tab tab="search" component="page-search"></ion-tab>
                    <ion-tab tab="favorites" component="page-favorites"></ion-tab>
                    <ion-tab-bar slot="bottom">
                        <ion-tab-button tab="movies">
                            <ion-icon name="videocam-outline"></ion-icon>
                            Peliculas
                        </ion-tab-button>
                        <ion-tab-button tab="search">
                            <ion-icon name="search-outline"></ion-icon>
                            Buscar
                        </ion-tab-button>
                        <ion-tab-button tab="favorites">
                            <ion-icon name="star-outline"></ion-icon>
                            Favoritos
                        </ion-tab-button>
                    </ion-tab-bar>
                </ion-tabs>
            </ion-app>
        `);
    }
}

window.customElements.define('app-ionic', PageInit);