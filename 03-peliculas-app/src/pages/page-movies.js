import {MoviesService} from '../services/movies-service.js';

class PageMovies extends HTMLElement {
    constructor() {
        super();

        /** @type {MoviesService} */
        this.moviesService = new MoviesService();

        /** @type {Array<import('../services/movies-service.js').Pelicula>} */
        this.recentMovies = [];
        
        /** @type {Array<import('../services/movies-service.js').Pelicula>} */
        this.popularMovies = [];

        this.slideBackdrop = null;
        this.slidePosters = null;
        this.slidePosterPopulars = null;
    }

    set _recentMovies(recentMovies = []) {
        this.recentMovies = recentMovies;        
        this.slideBackdrop._movies = this.recentMovies;
        this.slidePosters._movies = this.recentMovies;
    }

    set _popularMovies(popularMovies = []) {
        this.popularMovies = popularMovies;        
        this.slidePosterPopulars._movies = this.popularMovies;
    }

    get _popularMovies() {
        return this.popularMovies;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-content>
                <ion-grid fixed>
                    <ion-row>
                        <ion-col>
                            <h3>Peliculas Nuevas</h3>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <!-- slide de peliculas -->
                <slideshow-backdrop></slideshow-backdrop>
                <ion-grid fixed>
                    <ion-row>
                        <ion-col>
                            <h3>Cartelera</h3>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <!-- slide de posters -->
                <slideshow-poster id="peliculas"></slideshow-poster>
                <ion-grid fixed>
                    <ion-row>
                        <ion-col>
                            <h3>Peliculas Populares</h3>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <!-- slide de peliculas populares -->
                <slideshow-pars></slideshow-pars>
            </ion-content>
        `);

        this.onMount();
    }
    
    onMount() {
        this.render();
        this.moviesService.getFeature()
            .then(data => {
                this._recentMovies = data.results;
                return this.moviesService.getPopulars();
            })
            .then(data => {
                this._popularMovies = data.results;
            })
            .catch(console.error);      
    }

    render() {
        this.slideBackdrop = this.querySelector('slideshow-backdrop');
        if (!this.slideBackdrop) return;

        this.slidePosters = this.querySelector('#peliculas');
        if (!this.slidePosters) return;

        this.slidePosterPopulars = this.querySelector('slideshow-pars');
        if (!this.slidePosterPopulars) return;

        this.slidePosterPopulars.addEventListener('load-more', this.loadMore.bind(this));
    }

    loadMore() {
        this.moviesService.getPopulars()
            .then(data => this._popularMovies = this._popularMovies.concat(data.results))
            .catch(console.error)
    }
}

window.customElements.define('page-movies', PageMovies);