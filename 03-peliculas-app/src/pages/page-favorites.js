/**
 * @typedef {Array<{
 *  genre: string,
 *  movies: Array<import('../services/movies-service').PeliculaDetalle>
 * }>} MovieForGenre
 */

import {MoviesService} from '../services/movies-service.js';
import {StorageService} from '../services/storage-service.js';

class PageFavorites extends HTMLElement {
    constructor() {
        super();

        /** @type {Array<import("../services/movies-service").PeliculaDetalle>} */
        this.movies = [];

        /** @type {Array<import('../services/movies-service').Genre>} */
        this.genres = [];

        /** @type {MovieForGenre} */
        this.moviesForGenre = []

        this.storageService = StorageService.getInstance();
        this.movieService = new MoviesService();

        this.slidePoster = null;
    }

    set _movies(movies = []) {
        this.movies = movies;
    }

    get _movies() {
        return this.movies;
    }

    set _genres(genres = []) {
        this.genres = genres;
    }

    get _genres() {
        return this.genres;
    }

    set _moviesForGenre(moviesForGenre = []) {
        this.moviesForGenre = moviesForGenre;
        
        const genresElement = this.querySelector('#genres');    
        if (!genresElement) return;

        genresElement.innerHTML = '';

        // se construye el dom manualmente 
        // porque el slideshow de los posters tiene
        // una propiedad setter que construye los slides
        this.moviesForGenre.forEach(movieGenre => {
            // creamos los elementos
            const ionGrid = document.createElement('ion-grid');
            const ionRow = document.createElement('ion-row');
            const ionCol = document.createElement('ion-col');
            const header = document.createElement('h4');
            const slidePoster = document.createElement('slideshow-poster');
            
            // establecemos los titulos
            header.innerText = movieGenre.genre;

            // insertamos en el DOM
            ionCol.appendChild(header);
            ionRow.appendChild(ionCol);
            ionGrid.appendChild(ionRow);

            genresElement.appendChild(ionGrid);
            genresElement.appendChild(slidePoster);
            
            // establemos el slider de las peliculas
            slidePoster._movies = movieGenre.movies;
        });
    }

    connectedCallback() {
        this.innerHTML = (`
            <ion-content>
                <ion-grid fixed>
                    <ion-row>
                        <ion-col>
                            <h3>Favoritos</h3>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <div id="genres"></div>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        // permite suscribirse al cambio del store
        this.storageService.subscribe(this.loadFavorites.bind(this));
        this.loadFavorites();
    }
    
    
    loadFavorites() {
        this.storageService.loadFavorites()
            .then(movies => {
                this._movies = movies
                return this.movieService.getGenreMovies();
            })
            .then(genres => {
                this._genres = genres;
                this.setMoviesForGenre(this._genres, this._movies);
            })
            .catch(console.error); 
    }

    /**
     * Establece un listado de peliculas por genero
     * @param {Array<import('../services/movies-service').Genre>} genres 
     * @param {Array<import('../services/movies-service').PeliculaDetalle>} movies 
     */
    setMoviesForGenre(genres, movies) {
        this._moviesForGenre = genres.map(genre => ({
            genre: genre.name,
            movies: movies.filter(movie => {
                return movie.genres.some(genreMovie => genreMovie.id === genre.id);
            }),
        }));
    }

    disconnectedCallback() {
        this.storageService.unsubscribe();
    }
}

window.customElements.define('page-favorites', PageFavorites);