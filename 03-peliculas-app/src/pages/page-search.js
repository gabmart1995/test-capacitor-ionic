import {MoviesService} from '../services/movies-service.js';
import {imagePipe} from '../pipes/image-pipe.js';

class PageSearch extends HTMLElement {
    constructor() {
        super();
        this.ideas = ['spiderman', 'avengers', 'top gun', 'ready player one'];
        this.searchbar = null;
        this.movieService = new MoviesService();

        /** @type {Array<import('../services/movies-service.js').Pelicula>} */
        this.movies = [];
        this.isSearching = false;
    }

    set _movies(movies = []) {
        this.movies = movies;

        const list = this.querySelector('ion-list');
        if (!list) return;

        const grid = this.querySelector('ion-grid#content');
        if (!grid) return;

        if (this.movies.length === 0) {
            list.style.display = '';
            grid.style.display = 'none';

        } else {
            const [row] = grid.children; 
            
            // inyectamos los nuevos elementos
            row.innerHTML = this.movies.map(movie => (`
                <ion-col size="6">
                    <ion-card movie-id="${movie.id}">
                        <img src="${imagePipe(movie.poster_path)}" />
                        <ion-card-subtitle>
                            ${movie.release_date}
                        </ion-card-subtitle>
                        <ion-card-content>
                            <h2>${movie.title}</h2>
                        </ion-card-content>
                    </ion-card>
                </ion-col>
            `)).join('');
            
            // mostramos los nuevos elementos
            list.style.display = 'none';
            grid.style.display = '';

            // incluir el evento del 
            const cards = Array.from(grid.querySelectorAll('ion-card'));
            if (cards.length === 0) return;

            cards.forEach(card => {
                const movieId = card.getAttribute('movie-id');
                card.addEventListener('click', this.openDetail.bind(this, movieId));
            });
            
        }
    }

    set _isSearching(isSearching = false) {
        this.isSearching = isSearching;

        const spinner = this.querySelector('ion-grid#spinner');
        if (!spinner) return;

        this.isSearching ? spinner.style.display = '' :  spinner.style.display = 'none';
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-title>Buscar Pelicula</ion-title>
                </ion-toolbar>
                <ion-toolbar>
                    <ion-searchbar placeholder="Buscar pelicula" animated debounce="700" value=""></ion-searchbar>
                </ion-toolbar>
            </ion-header>
            <ion-content>
                <ion-grid id="spinner">
                    <ion-row>
                        <ion-col class="ion-text-center">
                            <ion-spinner name="crescent"></ion-spinner>
                        </ion-col>
                    </ion-row>
                </ion-grid>
                <ion-list>
                    <ion-list-header>
                        <ion-label>Ideas</ion-label>
                    </ion-list-header>
                    ${this.ideas.map(idea => (`
                        <ion-item>
                            <ion-label color="primary">${idea}</ion-label>
                        </ion-item>
                    `)).join('')}
                </ion-list>
                <ion-grid id="content">
                    <ion-row></ion-row>
                </ion-grid>
            </ion-content>
        `)

        this.onMount();
    }

    onMount() {
        this._isSearching = false;
        this._movies = [];

        this.searchbar = this.querySelector('ion-searchbar');
        if (!this.searchbar) return;

        const items = this.querySelectorAll('ion-list ion-item > ion-label');
        if (items.length === 0) return;

        items.forEach(item => {
            item.addEventListener('click', this.onClickIdeas.bind(this));
        });
        
        this.searchbar.addEventListener('ionInput', this.searchMovie.bind(this));
    }
    
    onClickIdeas(event) {
        const {target} = event;
        const value = target.innerText;
    
        this.searchbar.value = value;
        this._isSearching = true;
    
        // mantenemos un debounce
        setTimeout(() => {
            // buscamos en el servicio
            this.movieService.searchMovie(value)
                .then(movies => {
                    this._isSearching = false;
                    this._movies = movies.results
                })
                .catch(console.error)
    
        }, this.searchbar.debounce);
    }

    searchMovie(event) {
        const {value} = event.target;

        if (value.length === 0) {
            this._isSearching = false;
            this._movies = [];
            return;
        } 

        this._isSearching = true;

        setTimeout(() => {
            this.movieService.searchMovie(value)
                .then(movies => {
                    this._isSearching = false;
                    this._movies = movies.results
                })
                .catch(console.error)

        }, this.searchbar.debounce);

    }

    /**
     * abre el modal del detalle
     * @param {string} movieId identificador de la pelicula
     */
    async openDetail(movieId) {
        try {
            const [movieDetail, movieCredits] = await Promise.all([
				this.movieService.getMovieDetail(movieId), 
				this.movieService.getMovieCredits(movieId),
			]);
			
			const modalController = Object.assign(document.createElement('ion-modal'), {
				backdropDismiss: false,
				component: 'movie-detail',
				htmlAttributes: { // pasamos los props al ion-modal
					movieDetail,
					movieCredits				
				}
			});
    
            this.appendChild(modalController);
    
            await modalController.present();
            await modalController.onDidDismiss();
    
            modalController.remove();

        } catch (error) {
            console.error(error);
        }
	}
}

window.customElements.define('page-search', PageSearch);