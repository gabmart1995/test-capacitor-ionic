import {enviroment} from '../environment/env.js';

/**
 * Transforma la url del poster
 * @param {string | undefined} image 
 * @param {string} size 
 * @returns {string}
 */
const imagePipe = (image, size = 'w500') => {
    if (!image) return ('assets/no-image-banner.jpg');
    
    const imageUrl = (`${enviroment.API_IMAGE_URL}/${size}${image}`);

    return imageUrl;
};

export {
    imagePipe
}