/**
 * transforma un array en array pares
 * @param {Array<any>} array
 * @returns {Array<Array<any>>} 
 */
const parsPipe = (array) => {
    const pares = array.reduce((previousValue, _, index, arr) => {
        if (index % 2 === 0) {
            previousValue.push(arr.slice(index, (index + 2)))
        }
    
        return previousValue;
    }, []);

    return pares;
}

export {
    parsPipe
}