/**
 * @typedef {{
*  page: number;
*  total_results: number;
*  total_pages: number;
*  results: Pelicula[];
* }} RespuestaMDB
*/
/**
* @typedef {{
*   vote_count: number;
*   id: number;
*   video: boolean;
*   vote_average: number;
*   title: string;
*   popularity: number;
*   poster_path: string;
*   original_language: string;
*   original_title: string;
*   genre_ids: number[];
*   backdrop_path?: string;
*   adult: boolean;
*   overview: string;
*   release_date: string;
* }} Pelicula
*/
/**
 * @typedef {{
 *  adult: boolean;
 *  backdrop_path: string;
 * belongs_to_collection?: any;
 * budget: number;
 * genres: Genre[];
 * homepage: string;
 * id: number;
 * imdb_id: string;
 * original_language: string;
 * original_title: string;
 * overview: string;
 * popularity: number;
 * poster_path: string;
 * production_companies: Productioncompany[];
 * production_countries: Productioncountry[];
 * release_date: string;
 * revenue: number;
 * runtime: number;
 * spoken_languages: Spokenlanguage[];
 * status: string;
 * tagline: string;
 * title: string;
 * video: boolean;
 * vote_average: number;
 * vote_count: number;
 * }} PeliculaDetalle
 */
/**
 * @typedef {{
 *  iso_639_1: string;
 *  name: string;
 * }} Spokenlanguage
 */
/**
 * @typedef {{
*  iso_3166_1: string;
*  name: string;
* }} Productioncountry
*/
/**
 * @typedef {{
*   id: number;
*   logo_path?: string;
*   name: string;
*   origin_country: string;
* }} Productioncompany
*/
/**
 * @typedef {{
*   id: number;
*   name: string;
* }} Genre
*/
/**
 * @typedef {{
 *   credit_id: string;
 *   department: string;
 *  gender: number;
 *   id: number;
 *   job: string;
 *   name: string;
  *  profile_path?: string;
 * }} Crew
 */
/**
 * @typedef {{
 *   cast_id: number;
 *   character: string;
 *   credit_id: string;
 *   gender: number;
 *   id: number;
 *   name: string;
 *   order: number;
 *   profile_path?: string;
 * }} Cast
 */
/**
 * @typedef {{
 *  id: number;
 *  cast: Cast[];
 *  crew: Crew[];
 * }} RespuestaCredits
 */

import {enviroment} from '../environment/env.js';

export class MoviesService {
    constructor() {
        this.params = Object.freeze({
            language: 'es',
            include_image_language: 'es'
        });

        this.options = {
            method: 'GET',
            headers: Object.freeze({
                accept: 'application/json',
                Authorization: 'Bearer ' + enviroment.BEARER_TOKEN
            })
        };

        this.popularsPage = 0;

        /** @type {Array<Genre>} */
        this.genres = []
    }
    
    /**
     * Obtiene las peliculas destacadas
     * @returns {Promise<RespuestaMDB>}
     */
    getFeature() {
        const today = new Date();
        const lastDayMonth = new Date(today.getFullYear(), (1 + today.getMonth()), 0).getDate();
        const mounth = (today.getMonth() + 1); 
        const mounthString = mounth < 10 ? ('0' + mounth.toString()) : mounth.toString();
        const init = (`${today.getFullYear()}-${mounthString}-01`);
        const end = (`${today.getFullYear()}-${mounthString}-${lastDayMonth}`);
        const query = new URLSearchParams({
            ...this.params,
            "primary_release_date.gte": init,
            "primary_release_date.lte": end
        }); 
        
        return fetch(`${enviroment.API_URL}discover/movie?${query.toString()}`, this.options)
            .then(response => {
                if (response.ok && response.status === 200)  return response.json();
                throw response;
            });
    }

    /**
     * Obtiene las peliculas populares
     * @returns {Promise<RespuestaMDB>}
     */
    getPopulars() {
        const query = new URLSearchParams({
            ...this.params,
            sort_by: 'popularity.desc',
            page: ++this.popularsPage,
        });

        return fetch(`${enviroment.API_URL}discover/movie?${query.toString()}`, this.options)
            .then(response => {
                if (response.ok && response.status === 200)  return response.json();
                throw response;
            });
    }
    
    /**
     * obtiene el detalle de una pelicula
     * @param {string} id identificador de la pelicula
     * @returns {Promise<PeliculaDetalle>}
     */
    getMovieDetail(id) {
        const query = new URLSearchParams(this.params);

        return fetch(`${enviroment.API_URL}movie/${id}?${query.toString()}`, this.options)
            .then(response => {
                if (response.ok && response.status === 200)  return response.json();
                throw response;
            });
    }

    /**
     * obtiene los creditos de la pelicula
     * @param {string} id identificador de la pelicula
     * @returns {Promise<RespuestaCredits>}
     */
    getMovieCredits(id) {
        const query = new URLSearchParams(this.params);

        return fetch(`${enviroment.API_URL}movie/${id}/credits?${query.toString()}`, this.options)
            .then(response => {
                if (response.ok && response.status === 200)  return response.json();
                throw response;
            });
    }

    /**
     * Busca una pelicula por el titulo de la misma
     * @param {string} search titulo de la pelicula
     * @returns {Promise<RespuestaMDB>}
     */
    searchMovie(search) {
        const query = new URLSearchParams({
            ...this.params, 
            query: search
        });

        return fetch(`${enviroment.API_URL}search/movie?${query.toString()}`, this.options)
            .then(response => {
                if (response.ok && response.status === 200)  return response.json();
                throw response;
            });
    }

    /**
     * Retorna los generos de las peliculas
     * @returns {Promise<Array<Genre>>}
     */
    getGenreMovies() {
        return new Promise((resolve, reject) => {
            const query = new URLSearchParams({language: this.params.language});

            fetch(`${enviroment.API_URL}genre/movie/list?${query.toString()}`, this.options)
                .then(response => {
                    if (response.ok && response.status === 200)  return response.json();
                    throw response;
                })
                .then(genreResponse => {
                    this.genres = genreResponse.genres;
                    resolve(this.genres);
                })
                .catch(error => {
                    reject(error);
                });
        });
    }
}