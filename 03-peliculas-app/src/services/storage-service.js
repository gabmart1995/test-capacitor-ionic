import {Storage} from '@ionic/storage';

export class StorageService {
    /** @type {StorageService} */
    static instance;
    
    constructor() {
        /** @type {Storage | null} */
        this.storage = null;    

        /** @type {Array<import('./movies-service').PeliculaDetalle>} */
        this.movies = [];

        this.callbackDispatch = null;

        this.init();
    }
    
    static getInstance() {
        if (!StorageService.instance) StorageService.instance = new StorageService();

        return StorageService.instance;
    }

    async init() {
        this.storage = await (new Storage()).create();
        this.loadFavorites();
    }

    /**
     * guarda la pelicula en indexDB
     * @param {import('./movies-service').PeliculaDetalle} movie 
     * @returns {Promise<string>}
     */
    async saveMovie(movie) {
        const exists = this.movies.some(movieItem => movieItem.id === movie.id);
        let message = '';

        if (exists) {
            this.movies = this.movies.filter(movieItem => movieItem.id !== movie.id);
            message = 'removido de favoritos';

        } else {
            this.movies.push(movie);
            message = 'Agregada a favoritos';
        
        }

        await this.storage.set('movies', this.movies);

        this.dispatch();

        return message
    }

    async loadFavorites() {
        const movies = await this.storage.get('movies');
        this.movies = movies ?? [];

        return this.movies;
    }

    /**
     * verifica si existe la pelicula
     * @param {number} id 
     */
    async existMovie(id) {
        await this.loadFavorites();
        
        return this.movies.some(movieItem => movieItem.id === id);
    }


    subscribe(callback) {
        this.callbackDispatch = callback;
    }

    dispatch() {
        if (this.callbackDispatch) {
            this.callbackDispatch();
        }
    }

    unsubscribe() {
        if (this.callbackDispatch) {
            this.callbackDispatch = null;
        }
    }
}