const path = require('path');

module.exports = {
    watch: false,
    mode: 'production', // 'development' | 'production'
    entry: ['./src/index.js'],
    output: {
        path: path.resolve(__dirname, 'www'),
        filename: 'index.js',
    },
    module: {
        rules: [
            {
                test: /\.(js|mjs|cjs)$/,
                use: 'babel-loader',
                exclude: /node_modules/
            }
        ]
    },
    devServer: {
        contentBase: path.resolve(__dirname, 'www'),
        port: 3000,
        compress: true,
        historyApiFallback: true,
    }
};