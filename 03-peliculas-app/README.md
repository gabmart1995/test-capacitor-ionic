# VANILLA JS + CAPACITOR + IONIC

Este proyecto es una aplicacion de peliculas integrado
con vanilla js.

## Instalacion de Webpack dev server y Capacitor
`yarn install`

### Transpilacion y servidor de desarrollo
Para este proceso se requiere de 2 terminales o procesos uno `yarn build` y `yarn dev` 
para activar el modo transpilacion y el servidor en modo hot reload de Webpack.

## Compilar para android
Para generar el proyecto en android usa el comando `yarn generate_android` creara los 
directorios escritos en JAVA para la compilacion (usar este comando si no tienes el directorio de 
andrioid).<br /><br />

Una vez completado debes correr la sincronizacion del proyecto con `yarn sync` y por ultimo
corres el comando de ejecucion `yarn run_android` para correrlo en emulador de Android Studio para mas info consultar [aqui](https://capacitorjs.com/docs/android)