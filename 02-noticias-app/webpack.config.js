const path = require('path');

module.exports = {
  mode: 'production',
  watch: false,
  entry: [
    './src/index.js'
  ],
  output: {
    path: path.resolve(__dirname, 'www', 'app'),
    filename: 'index.js',
   
    // establece el path de los assets del bundle 
    // usar en caso de que la transpilacion se halle en otra
    // carpeta
    publicPath: '/app/'  
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        use: 'babel-loader',
        exclude: /node_modules/
      },
      {
        test: /\.css$/,
        use: [
            'style-loader',
            'css-loader'
        ],
      },
      // images
      {
          test: /\.(png|jpg|svg|gif)$/,
          use: [
              'file-loader'
          ]
      },
    ]
  },
  devServer: {
    contentBase: path.resolve(__dirname, 'www'),
    port: 3000,
    historyApiFallback: true,
    compress: true
  },
};
	