/**
 * Respuesta de los encabezados
 * @typedef {{
 *  status: string,
 *  totalResults: number,
 *  articles: Array<Article>
 * }} ResponseTopHeadlines
 * 
*/

/** 
 * Instancia de la fuente
 * @typedef {{
 *  id?: string,
 *  name: string
 * }} Source
 */

/**  
 * Instancia del articulos
 * @typedef {{
 *  author?: string,
 *  content?: string, 
 *  title: string,
 *  description: string,
 *  url: string,
 *  urlToImage: string,
 *  publishedAt: string,
 *  source: Source
 * }} Article
 */
/**
 * Instancia del articulos y pagina
 * @typedef {{
 *  [key: string]: {
 *      page: number,
 *      articles: Array<Article>
 *  }
* }} ArticlesByCategoryAndPage
 */

import {enviroment} from '../environment/environment';

export class NewsService {
    /** @type {NewsService} */
    static instance;
 
    /**
     * funcion que verifica que la instancia esta creada
     * @returns {NewsService}
    */
    static getInstance() {
        if (NewsService.instance) return NewsService.instance;
        
        return new NewsService();
    }
    
    constructor() {
        this.categories = ['business', 'entertainment', 'general', 'health', 'science', 'sports', 'technology'];
        
        /** @type {ArticlesByCategoryAndPage} */
        this.articlesByCategoryAndPage = {} // caching de peticiones http
    }

    get _categories() {
        return this.categories
    }

    /**
     * Obtiene los encabezados
     * @returns {Promise<Array<Article>>}
     */
    getTopHeadLines() {
        return this.getArticlesByCategory('business');
    }

    /**
     * Obtiene los encabezados por categoria
     * @param {string} category nombre de la categoria
     * @param {boolean} loadMore flag que indica que si necesita ir al API a obtener nuevos resultados
     * @returns {Promise<Array<Article>>}
     */
    getTopHeadLinesByCategory(category, loadMore = false) {
        // si el usuario quiere cargar mas
        if (loadMore) {
            return this.getArticlesByCategory(category);
        }

        // si existe la peticion en el cache la retornamos
        if (Object.keys(this.articlesByCategoryAndPage).includes(category)) {
            // console.log('peticion desde cache');
            
            return Promise.resolve(this.articlesByCategoryAndPage[category].articles);
        }
        
        return this.getArticlesByCategory(category);
    }

    /**
     * metodo privado
     * 
     * Funcion que verifica si la informacion esta en cache y anade nuevos elementos
     * @param {string} category nombre de la categoria
     * @returns {Promise<Array<Article>>}
     */
    getArticlesByCategory(category) {
        // sino existe elemento en cache crea la direccion en memoria
        if (!(Object.keys(this.articlesByCategoryAndPage)).includes(category)) {
            this.articlesByCategoryAndPage[category] = {
                page: 0,
                articles: []
            }  
        } 

        const page = (this.articlesByCategoryAndPage[category].page + 1)
        const queryParams = (new URLSearchParams({
            apiKey: enviroment.API_KEY,
            country: 'us',
            category,
            page
        })).toString();

        return fetch(`https://newsapi.org/v2/top-headlines?${queryParams}`)
            .then(response => {
                if (response.ok && response.status === 200) {
                    return response.json();
                } 

                throw response;
            })
            .then((/** @type {ResponseTopHeadlines} */ {articles}) => {
                // console.log('peticion desde http');

                // si llega al final de la pagina devuelve un array vacio
                // se devuelve la pagina con la ultima posicion en 
                // memoria
                if (articles.length === 0) {
                    return Promise.resolve(this.articlesByCategoryAndPage[category].articles);
                }
                
                // actualizamos el cache con el numero de pagina
                // y los articulos
                const newArticles = this.articlesByCategoryAndPage[category].articles.concat(articles);
                this.articlesByCategoryAndPage[category] = {
                    page,
                    articles: newArticles
                };
                
                return Promise.resolve(newArticles);
            });        
    }
}
