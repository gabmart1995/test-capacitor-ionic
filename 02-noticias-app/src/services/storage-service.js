import {Storage} from '@ionic/storage';

export class StorageService {

    /** @type {StorageService} */
    static instance;
    
    /* patron singleton: se asegura que se tome una unica instancia  */
    static getStore() {
        if (!StorageService.instance) StorageService.instance = new StorageService();
        
        return StorageService.instance;
    }
    
    constructor() {
        /** @type {Array<import('./news-service').Article>} */
        this.localArticles = [];
        
        /** @type {React.Dispatch<React.SetStateAction<Article[]>> | null} */
        this.callbackDispatch = null; // funcion que actualiza la interfaz
        
        this.init();
    }
    
    async init() {
        this.storage = await (new Storage()).create();

        this.loadFavorites();
    }

    get _localArticles() {
        return [...this.localArticles];
    }

    set _localArticles(localArticles = []) {
        this.localArticles = localArticles;
        
        this.dispatch(); // ejecuta la accion de despachar al useState
    }

    /**
     * Salva en el indexDB los datos del articulo
     * @param {import('./news-service').Article} article
     */
    async saveRemoveArticle(article) {
        // transforma la expresion en un valor booleano
        const exists = !!(this._localArticles.find(localArticle => localArticle.title === article.title));

        // si existe remueve el articulo, sino la crea
        if (exists) {
            this._localArticles = this._localArticles.filter(localArticle => localArticle.title !== article.title);
        
        } else {
            this._localArticles = [article, ...this._localArticles];

        }

        await this.storage.set('articles', this._localArticles);
    }

    async loadFavorites() {
        try {
            const articles = await this.storage.get('articles');    
            this.localArticles = articles ?? [];

        } catch (error) {
            console.error(error);

            this.localArticles = [];
        }
    } 

    /**
     * determina si un articulo esta en favoritos
     * @param {import('./news-service').Article} article 
     * @returns {boolean}
     */
    articleInFavorites(article) {
        return this._localArticles.some(localArticle => localArticle.title === article.title);
    }


    /** patron observador: permite despachar un funcion cuando se notifique algun cambio */
    /** permite despachar un evento de suscripcion */
    dispatch() {
        if (this.callbackDispatch) {
            this.callbackDispatch(this._localArticles);
        }
    }

    /** 
     * permite suscribir a una funcion de actualiza la interfaz de usuario
     * @param {React.Dispatch<React.SetStateAction<Article[]>>} callback
     */
    subscribe(callback) {
        this.callbackDispatch = callback;
    }

    /** deja de subscribirse a los cambios del store. */
    unsubscribe() {
        this.callbackDispatch = null;
    }
}