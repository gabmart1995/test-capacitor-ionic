import React, { useMemo } from 'react';

import {IonButton, IonCard, IonCardContent, IonCardHeader, IonCardSubtitle, IonCardTitle, IonCol, IonIcon, IonImg, IonRow, useIonActionSheet, useIonToast} from '@ionic/react';
import {ellipsisVerticalOutline, heartOutline, shareOutline, closeOutline, heart} from 'ionicons/icons';

import {Browser} from '@capacitor/browser';
import {Share} from '@capacitor/share';

import {detectMobile} from '../utilities/utilities';

import {StorageService} from '../services/storage-service';

const Article = ({index, article}) => {
    const [present] = useIonActionSheet(); // hook action sheet
    const [presentToast] = useIonToast();  // hook toast

    /** @type {StorageService} */
    const storageService = useMemo(() => StorageService.getStore(), []);

    /**
     * Abre el navegador incluido en la aplicacion
     */
    const openArticle = async () => {
        if (detectMobile()) {
            await Browser.open({url: article.url});
            return; 
        }

        window.open(article.url, '_blank');
    };
    
    /**
     * Abre el menu action sheet
     * @param {Event} event 
     */
    const openMenu = event => {
        event.stopPropagation(); // evita la propagacion del click del openArticle
        
        const articleInFavorite = storageService.articleInFavorites(article);

        /** @type {import('@ionic/react/dist/types/components/IonActionSheet').ActionSheetOptions} */
        const options = {
            buttons: [
                {
                    text: articleInFavorite ? 'Remover favorito' : 'Favorito',
                    icon: articleInFavorite ? heart : heartOutline,
                    handler: () => onToogleFavorite(articleInFavorite)
                },
                {
                    text: 'Cancelar',
                    role: 'cancel',
                    icon: closeOutline
                }
            ],
            header: 'Opciones'
        }

        if (detectMobile()) {
            options.buttons.unshift({
                text: 'Compartir',
                icon: shareOutline,
                handler: onShareArticle
            });
        }

        present(options);
    };

    /**
     * Abre el toast del usuario
     * @param {string} message mensaje del toast
     * @param {'top'|'middle'|'bottom'} position position del elemento
     */
    const openToast = (message, position) => presentToast({
        position,
        message,
        duration: 1500
    });
    

    /** Comparte el articulo solo funciona en plataforma nativa */
    const onShareArticle = async () => {
        try {
            const {value: result} = await Share.canShare(); // preguntamos primero si puede compartir  
            
            // compartimos la noticia
            if (result) {
                await Share.share({
                    title: article.title,
                    url: article.url,
                    text: article.source.name,
                    dialogTitle: 'Compartir en:'
                });    
            }

        } catch (error) {
            console.error(error);
        
        }
    };

    /**
     * Marca la noticia como favorita
     * @param {boolean} exists flag del mensaje al toast
     */
    const onToogleFavorite = (exists) => {
        storageService.saveRemoveArticle(article);
        openToast(
            exists ? 'El articulo se retiro de favoritos' : 'El articulo se anadio a favoritos',
            'bottom'
        );
    };
    
    return (
        <>
            <IonCard>
                <IonCardHeader>
                    <IonCardSubtitle class="ion-margin">
                        <IonRow onClick={openArticle}>
                            <IonCol size={10}>
                                <span className="text-primary" style={{marginRight: 5}}>{index}</span>
                                <span className="article-source-name">{article.source.name}</span>
                            </IonCol>
                            <IonCol size={2}>
                                <IonButton fill="clear" onClick={openMenu}>
                                    <IonIcon slot="icon-only" icon={ellipsisVerticalOutline}></IonIcon>
                                </IonButton>
                            </IonCol>
                        </IonRow>
                    </IonCardSubtitle>
                    <IonCardTitle className="ion-padding-horizontal ion-margin-bottom">
                        {article.title}
                    </IonCardTitle>
                </IonCardHeader>
                {article.urlToImage && (<IonImg src={article.urlToImage}></IonImg>)}
                {article.description && (<IonCardContent><p>{article.description}</p></IonCardContent>)}                                    
            </IonCard>
        </>
    );
}

export default Article;