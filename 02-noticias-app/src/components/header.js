import React from 'react';

import {IonHeader, IonToolbar, IonTitle} from '@ionic/react';

const Header = ({title}) => {
    return (
        <IonHeader translucent mode="ios" className="ion-no-border">
            <IonToolbar color="secondary">
                <IonTitle color="light">{title}</IonTitle>
            </IonToolbar>
        </IonHeader>
    )
}

export default Header;