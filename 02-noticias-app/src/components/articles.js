import React from 'react';
import {IonGrid, IonRow, IonCol} from '@ionic/react'; 

import {Article} from './index';

const Articles = ({articles = []}) => {
    return (
        <IonGrid fixed>
            <IonRow>
                {articles.map((article, index) => (
                    <IonCol size={12} sizeLg={3} sizeMd={6} sizeXs={12} key={index}>
                       <Article article={article} index={index + 1} /> 
                    </IonCol>
                ))}
            </IonRow>
        </IonGrid>
    );
}

export default Articles;