import React from 'react';
import {Redirect, Route} from 'react-router-dom';

import {globeOutline, personOutline, starOutline} from 'ionicons/icons';

import {IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';

import {HeadingsTabPage, NewsTabPage, FavoriteTabPage} from './pages';

const App = () => {
    return (
        <IonApp>
            <IonReactRouter>
                <IonTabs>
                    <IonRouterOutlet>
                        <Route path="/favorites" component={FavoriteTabPage} />
                        <Route path="/headings" component={HeadingsTabPage} />
                        <Route path="/news"  component={NewsTabPage} />
                        <Redirect exact from="/" to="/news" />
                    </IonRouterOutlet>
    
                    {/** tab bar */}
                    <IonTabBar slot="bottom">
                        <IonTabButton tab="/news" href="/news">
                            <IonIcon icon={personOutline}></IonIcon>
                            <IonLabel>Para Ti</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="/headings" href="/headings">
                            <IonIcon icon={globeOutline}></IonIcon>
                            <IonLabel>Encabezados</IonLabel>
                        </IonTabButton>
                        <IonTabButton tab="/favorites" href="/favorites">
                            <IonIcon icon={starOutline}></IonIcon>
                            <IonLabel>Favoritos</IonLabel>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonReactRouter>
        </IonApp>
    );
}

export default App;