import HeadingsTabPage from './HeadingsTabPage';
import NewsTabPage from './NewsTabPage';
import FavoriteTabPage from './FavoriteTabPage';

export {
    HeadingsTabPage,
    NewsTabPage,
    FavoriteTabPage
}