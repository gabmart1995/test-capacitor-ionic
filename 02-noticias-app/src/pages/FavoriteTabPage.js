import React, {useMemo, useState, useEffect} from 'react';
import {IonContent, IonHeader, IonToolbar, IonTitle} from '@ionic/react';

import {Articles, Header} from '../components';
import {StorageService} from '../services/storage-service';

const FavoritesTab = () => {
    const storageService = useMemo(() => StorageService.getStore(), []);
    const [articles, setArticles] = useState(storageService._localArticles);

    useEffect(() => {
        // patron observer le pasamos la funcion que actualiza el estado en react
        // si existe algun cambio en el indexDB de los articulos actualiza el estado
        storageService.subscribe(setArticles);

        // cuando se desmonte el componente nos dejaremos de suscribir al servicio
        return () => storageService.unsubscribe();
    }, []);


   return (
        <>
            <Header title="Favoritos" />
            <IonContent className="ion-padding" fullscreen>
                <IonHeader collapse="condense" mode="ios" className="ion-no-border">
                    <IonToolbar>
                        <IonTitle size="large">Favoritos</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Articles articles={articles} />
            </IonContent>        
        </>
   )
}

export default FavoritesTab;