import React, {useMemo, useEffect, useState} from 'react';
import {IonContent, IonHeader, IonToolbar, IonTitle, IonSegment, IonLabel, IonSegmentButton, IonInfiniteScroll, IonInfiniteScrollContent} from '@ionic/react';
import {NewsService} from '../services/news-service';
import {Articles, Header} from '../components';

const Tab2Page = () => {
    const newsService = useMemo(() => NewsService.getInstance(), []);
    
    const [categorySelected, setCategorySelected] = useState(newsService._categories[0]);
    const [articles, setArticles] = useState([]);
    
    /** 
     * Funcion que maneja el cambio de categoria
     * @param {string} category
     */
    const segmentChanged = ({detail: {value}}) => setCategorySelected(value);

    /**
     * funcion que maneja el infinite scroll
     * @param {CustomEvent} event 
     */
    const loadData = ({target}) => {
        newsService.getTopHeadLinesByCategory(categorySelected, true)
            .then(data => {
                // verifica si es el ultimo elemento del array en cache
                // es el mismo de la peticion desactiva el infinite
                const isInfiniteFinished = data[data.length - 1].title === articles[articles.length - 1].title; 
                if (isInfiniteFinished) {
                    target.disabled = true;
                    return;
                }

                // espera un segundo y medio para cargar la data
                setTimeout(() => {
                    target.complete();
                    setArticles(data);
                }, 1500);
            })
            .catch(console.error);
    };

    // colocamos un efecto sobre categorySelected para que cuando cambie el 
    // segemento dispara la peticion
    useEffect(() => {
        newsService.getTopHeadLinesByCategory(categorySelected)
            .then(data => setArticles(data))
            .catch(console.error);

    }, [categorySelected]);

   return (
        <>
            <Header title="Encabezados" />
            <IonContent className="ion-padding" fullscreen>
                <IonHeader collapse="condense" mode="ios" className="ion-no-border">
                    <IonToolbar>
                        <IonTitle size="large">Encabezados</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <IonSegment 
                    mode="md" 
                    scrollable 
                    onIonChange={segmentChanged}
                    value={categorySelected}
                    className="no-bottom-bar"
                >
                    {newsService._categories.map((category, index) => (
                        <IonSegmentButton value={category} key={index + 1}>
                            <IonLabel className="text-capitalize">{category}</IonLabel>
                        </IonSegmentButton>
                    ))}
                </IonSegment>
                <Articles articles={articles} />
                <IonInfiniteScroll threshold="25%" onIonInfinite={loadData}>
                    <IonInfiniteScrollContent 
                        loadingSpinner="crescent" 
                        loadingText="Cargando ..."
                    ></IonInfiniteScrollContent>
                </IonInfiniteScroll>
            </IonContent>
        </>
   )
}

export default Tab2Page;