import React, {useEffect, useMemo, useState} from 'react';
import {IonContent, IonHeader, IonToolbar, IonTitle, IonInfiniteScroll, IonInfiniteScrollContent} from '@ionic/react';

import {NewsService} from '../services/news-service';

import {Articles, Header} from '../components';

const NewsTabPage = () => {
    const [articles, setArticles] = useState([]);

    const newsService = useMemo(() => NewsService.getInstance(), []);

    /**
     * funcion que maneja el infinite scroll
     * @param {CustomEvent} event 
     */
    const loadData = ({target}) => {
        newsService.getTopHeadLinesByCategory('business', true)
            .then(data => {
                
                // verifica si es el ultimo elemento del array en cache
                // es el mismo de la peticion desactiva el infinite
                const isInfiniteFinished = data[data.length - 1].title === articles[articles.length - 1].title; 
                if (isInfiniteFinished) {
                    target.disabled = true;
                    return;
                }

                // espera un segundo y medio para cargar la data
                setTimeout(() => {
                    target.complete();
                    setArticles(data);
                }, 1500);
            })
            .catch(console.error);
    };

    useEffect(() => {
        newsService.getTopHeadLines()
            .then(articles => setArticles(articles))
            .catch(console.error);
    }, []);

   return (
        <>
            <Header title="Gabriel's News" />
            <IonContent className="ion-padding" fullscreen>
                <IonHeader collapse="condense" mode="ios" className="ion-no-border">
                    <IonToolbar>
                        <IonTitle size="large">Gabriel's News</IonTitle>
                    </IonToolbar>
                </IonHeader>
                <Articles articles={articles} />
                <IonInfiniteScroll threshold="25%" onIonInfinite={loadData}>
                    <IonInfiniteScrollContent 
                        loadingSpinner="crescent" 
                        loadingText="Cargando ..."
                    ></IonInfiniteScrollContent>
                </IonInfiniteScroll>
            </IonContent>
        </>
   )
}

export default NewsTabPage;