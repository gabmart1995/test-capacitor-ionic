import React from 'react';
import ReactDom from 'react-dom/client';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';
import './style.css';

import AppIonic from './App';
import {StorageService} from './services/storage-service';

StorageService.getStore(); //inicializamos el store de la app

const root = ReactDom.createRoot(document.getElementById('root'));

root.render(<AppIonic />);
	