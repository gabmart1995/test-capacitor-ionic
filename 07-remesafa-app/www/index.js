"use strict";(()=>{var Xt=Object.create;var je=Object.defineProperty;var Jt=Object.getOwnPropertyDescriptor;var Zt=Object.getOwnPropertyNames;var en=Object.getPrototypeOf,tn=Object.prototype.hasOwnProperty;var re=(x=>typeof require<"u"?require:typeof Proxy<"u"?new Proxy(x,{get:(f,h)=>(typeof require<"u"?require:f)[h]}):x)(function(x){if(typeof require<"u")return require.apply(this,arguments);throw Error('Dynamic require of "'+x+'" is not supported')});var nn=(x,f)=>()=>(f||x((f={exports:{}}).exports,f),f.exports);var rn=(x,f,h,p)=>{if(f&&typeof f=="object"||typeof f=="function")for(let g of Zt(f))!tn.call(x,g)&&g!==h&&je(x,g,{get:()=>f[g],enumerable:!(p=Jt(f,g))||p.enumerable});return x};var on=(x,f,h)=>(h=x!=null?Xt(en(x)):{},rn(f||!x||!x.__esModule?je(h,"default",{value:x,enumerable:!0}):h,x));var Ge=nn((Qe,Se)=>{"use strict";(function(x){if(typeof Qe=="object"&&typeof Se<"u")Se.exports=x();else if(typeof define=="function"&&define.amd)define([],x);else{var f;typeof window<"u"?f=window:typeof global<"u"?f=global:typeof self<"u"?f=self:f=this,f.localforage=x()}})(function(){var x,f,h;return function p(g,A,T){function O(L,F){if(!A[L]){if(!g[L]){var m=typeof re=="function"&&re;if(!F&&m)return m(L,!0);if(E)return E(L,!0);var b=new Error("Cannot find module '"+L+"'");throw b.code="MODULE_NOT_FOUND",b}var N=A[L]={exports:{}};g[L][0].call(N.exports,function(P){var q=g[L][1][P];return O(q||P)},N,N.exports,p,g,A,T)}return A[L].exports}for(var E=typeof re=="function"&&re,C=0;C<T.length;C++)O(T[C]);return O}({1:[function(p,g,A){(function(T){"use strict";var O=T.MutationObserver||T.WebKitMutationObserver,E;if(O){var C=0,L=new O(P),F=T.document.createTextNode("");L.observe(F,{characterData:!0}),E=function(){F.data=C=++C%2}}else if(!T.setImmediate&&typeof T.MessageChannel<"u"){var m=new T.MessageChannel;m.port1.onmessage=P,E=function(){m.port2.postMessage(0)}}else"document"in T&&"onreadystatechange"in T.document.createElement("script")?E=function(){var B=T.document.createElement("script");B.onreadystatechange=function(){P(),B.onreadystatechange=null,B.parentNode.removeChild(B),B=null},T.document.documentElement.appendChild(B)}:E=function(){setTimeout(P,0)};var b,N=[];function P(){b=!0;for(var B,W,M=N.length;M;){for(W=N,N=[],B=-1;++B<M;)W[B]();M=N.length}b=!1}g.exports=q;function q(B){N.push(B)===1&&!b&&E()}}).call(this,typeof global<"u"?global:typeof self<"u"?self:typeof window<"u"?window:{})},{}],2:[function(p,g,A){"use strict";var T=p(1);function O(){}var E={},C=["REJECTED"],L=["FULFILLED"],F=["PENDING"];g.exports=m;function m(d){if(typeof d!="function")throw new TypeError("resolver must be a function");this.state=F,this.queue=[],this.outcome=void 0,d!==O&&q(this,d)}m.prototype.catch=function(d){return this.then(null,d)},m.prototype.then=function(d,S){if(typeof d!="function"&&this.state===L||typeof S!="function"&&this.state===C)return this;var _=new this.constructor(O);if(this.state!==F){var D=this.state===L?d:S;N(_,D,this.outcome)}else this.queue.push(new b(_,d,S));return _};function b(d,S,_){this.promise=d,typeof S=="function"&&(this.onFulfilled=S,this.callFulfilled=this.otherCallFulfilled),typeof _=="function"&&(this.onRejected=_,this.callRejected=this.otherCallRejected)}b.prototype.callFulfilled=function(d){E.resolve(this.promise,d)},b.prototype.otherCallFulfilled=function(d){N(this.promise,this.onFulfilled,d)},b.prototype.callRejected=function(d){E.reject(this.promise,d)},b.prototype.otherCallRejected=function(d){N(this.promise,this.onRejected,d)};function N(d,S,_){T(function(){var D;try{D=S(_)}catch(H){return E.reject(d,H)}D===d?E.reject(d,new TypeError("Cannot resolve promise with itself")):E.resolve(d,D)})}E.resolve=function(d,S){var _=B(P,S);if(_.status==="error")return E.reject(d,_.value);var D=_.value;if(D)q(d,D);else{d.state=L,d.outcome=S;for(var H=-1,k=d.queue.length;++H<k;)d.queue[H].callFulfilled(S)}return d},E.reject=function(d,S){d.state=C,d.outcome=S;for(var _=-1,D=d.queue.length;++_<D;)d.queue[_].callRejected(S);return d};function P(d){var S=d&&d.then;if(d&&(typeof d=="object"||typeof d=="function")&&typeof S=="function")return function(){S.apply(d,arguments)}}function q(d,S){var _=!1;function D(U){_||(_=!0,E.reject(d,U))}function H(U){_||(_=!0,E.resolve(d,U))}function k(){S(H,D)}var $=B(k);$.status==="error"&&D($.value)}function B(d,S){var _={};try{_.value=d(S),_.status="success"}catch(D){_.status="error",_.value=D}return _}m.resolve=W;function W(d){return d instanceof this?d:E.resolve(new this(O),d)}m.reject=M;function M(d){var S=new this(O);return E.reject(S,d)}m.all=ue;function ue(d){var S=this;if(Object.prototype.toString.call(d)!=="[object Array]")return this.reject(new TypeError("must be an array"));var _=d.length,D=!1;if(!_)return this.resolve([]);for(var H=new Array(_),k=0,$=-1,U=new this(O);++$<_;)V(d[$],$);return U;function V(ee,ie){S.resolve(ee).then(fe,function(J){D||(D=!0,E.reject(U,J))});function fe(J){H[ie]=J,++k===_&&!D&&(D=!0,E.resolve(U,H))}}}m.race=X;function X(d){var S=this;if(Object.prototype.toString.call(d)!=="[object Array]")return this.reject(new TypeError("must be an array"));var _=d.length,D=!1;if(!_)return this.resolve([]);for(var H=-1,k=new this(O);++H<_;)$(d[H]);return k;function $(U){S.resolve(U).then(function(V){D||(D=!0,E.resolve(k,V))},function(V){D||(D=!0,E.reject(k,V))})}}},{1:1}],3:[function(p,g,A){(function(T){"use strict";typeof T.Promise!="function"&&(T.Promise=p(2))}).call(this,typeof global<"u"?global:typeof self<"u"?self:typeof window<"u"?window:{})},{2:2}],4:[function(p,g,A){"use strict";var T=typeof Symbol=="function"&&typeof Symbol.iterator=="symbol"?function(e){return typeof e}:function(e){return e&&typeof Symbol=="function"&&e.constructor===Symbol&&e!==Symbol.prototype?"symbol":typeof e};function O(e,n){if(!(e instanceof n))throw new TypeError("Cannot call a class as a function")}function E(){try{if(typeof indexedDB<"u")return indexedDB;if(typeof webkitIndexedDB<"u")return webkitIndexedDB;if(typeof mozIndexedDB<"u")return mozIndexedDB;if(typeof OIndexedDB<"u")return OIndexedDB;if(typeof msIndexedDB<"u")return msIndexedDB}catch{return}}var C=E();function L(){try{if(!C||!C.open)return!1;var e=typeof openDatabase<"u"&&/(Safari|iPhone|iPad|iPod)/.test(navigator.userAgent)&&!/Chrome/.test(navigator.userAgent)&&!/BlackBerry/.test(navigator.platform),n=typeof fetch=="function"&&fetch.toString().indexOf("[native code")!==-1;return(!e||n)&&typeof indexedDB<"u"&&typeof IDBKeyRange<"u"}catch{return!1}}function F(e,n){e=e||[],n=n||{};try{return new Blob(e,n)}catch(r){if(r.name!=="TypeError")throw r;for(var t=typeof BlobBuilder<"u"?BlobBuilder:typeof MSBlobBuilder<"u"?MSBlobBuilder:typeof MozBlobBuilder<"u"?MozBlobBuilder:WebKitBlobBuilder,o=new t,i=0;i<e.length;i+=1)o.append(e[i]);return o.getBlob(n.type)}}typeof Promise>"u"&&p(3);var m=Promise;function b(e,n){n&&e.then(function(t){n(null,t)},function(t){n(t)})}function N(e,n,t){typeof n=="function"&&e.then(n),typeof t=="function"&&e.catch(t)}function P(e){return typeof e!="string"&&(console.warn(e+" used as a key, but it is not a string."),e=String(e)),e}function q(){if(arguments.length&&typeof arguments[arguments.length-1]=="function")return arguments[arguments.length-1]}var B="local-forage-detect-blob-support",W=void 0,M={},ue=Object.prototype.toString,X="readonly",d="readwrite";function S(e){for(var n=e.length,t=new ArrayBuffer(n),o=new Uint8Array(t),i=0;i<n;i++)o[i]=e.charCodeAt(i);return t}function _(e){return new m(function(n){var t=e.transaction(B,d),o=F([""]);t.objectStore(B).put(o,"key"),t.onabort=function(i){i.preventDefault(),i.stopPropagation(),n(!1)},t.oncomplete=function(){var i=navigator.userAgent.match(/Chrome\/(\d+)/),r=navigator.userAgent.match(/Edge\//);n(r||!i||parseInt(i[1],10)>=43)}}).catch(function(){return!1})}function D(e){return typeof W=="boolean"?m.resolve(W):_(e).then(function(n){return W=n,W})}function H(e){var n=M[e.name],t={};t.promise=new m(function(o,i){t.resolve=o,t.reject=i}),n.deferredOperations.push(t),n.dbReady?n.dbReady=n.dbReady.then(function(){return t.promise}):n.dbReady=t.promise}function k(e){var n=M[e.name],t=n.deferredOperations.pop();if(t)return t.resolve(),t.promise}function $(e,n){var t=M[e.name],o=t.deferredOperations.pop();if(o)return o.reject(n),o.promise}function U(e,n){return new m(function(t,o){if(M[e.name]=M[e.name]||Ae(),e.db)if(n)H(e),e.db.close();else return t(e.db);var i=[e.name];n&&i.push(e.version);var r=C.open.apply(C,i);n&&(r.onupgradeneeded=function(a){var s=r.result;try{s.createObjectStore(e.storeName),a.oldVersion<=1&&s.createObjectStore(B)}catch(c){if(c.name==="ConstraintError")console.warn('The database "'+e.name+'" has been upgraded from version '+a.oldVersion+" to version "+a.newVersion+', but the storage "'+e.storeName+'" already exists.');else throw c}}),r.onerror=function(a){a.preventDefault(),o(r.error)},r.onsuccess=function(){var a=r.result;a.onversionchange=function(s){s.target.close()},t(a),k(e)}})}function V(e){return U(e,!1)}function ee(e){return U(e,!0)}function ie(e,n){if(!e.db)return!0;var t=!e.db.objectStoreNames.contains(e.storeName),o=e.version<e.db.version,i=e.version>e.db.version;if(o&&(e.version!==n&&console.warn('The database "'+e.name+`" can't be downgraded from version `+e.db.version+" to version "+e.version+"."),e.version=e.db.version),i||t){if(t){var r=e.db.version+1;r>e.version&&(e.version=r)}return!0}return!1}function fe(e){return new m(function(n,t){var o=new FileReader;o.onerror=t,o.onloadend=function(i){var r=btoa(i.target.result||"");n({__local_forage_encoded_blob:!0,data:r,type:e.type})},o.readAsBinaryString(e)})}function J(e){var n=S(atob(e.data));return F([n],{type:e.type})}function De(e){return e&&e.__local_forage_encoded_blob}function Je(e){var n=this,t=n._initReady().then(function(){var o=M[n._dbInfo.name];if(o&&o.dbReady)return o.dbReady});return N(t,e,e),t}function Ze(e){H(e);for(var n=M[e.name],t=n.forages,o=0;o<t.length;o++){var i=t[o];i._dbInfo.db&&(i._dbInfo.db.close(),i._dbInfo.db=null)}return e.db=null,V(e).then(function(r){return e.db=r,ie(e)?ee(e):r}).then(function(r){e.db=n.db=r;for(var a=0;a<t.length;a++)t[a]._dbInfo.db=r}).catch(function(r){throw $(e,r),r})}function K(e,n,t,o){o===void 0&&(o=1);try{var i=e.db.transaction(e.storeName,n);t(null,i)}catch(r){if(o>0&&(!e.db||r.name==="InvalidStateError"||r.name==="NotFoundError"))return m.resolve().then(function(){if(!e.db||r.name==="NotFoundError"&&!e.db.objectStoreNames.contains(e.storeName)&&e.version<=e.db.version)return e.db&&(e.version=e.db.version+1),ee(e)}).then(function(){return Ze(e).then(function(){K(e,n,t,o-1)})}).catch(t);t(r)}}function Ae(){return{forages:[],db:null,dbReady:null,deferredOperations:[]}}function et(e){var n=this,t={db:null};if(e)for(var o in e)t[o]=e[o];var i=M[t.name];i||(i=Ae(),M[t.name]=i),i.forages.push(n),n._initReady||(n._initReady=n.ready,n.ready=Je);var r=[];function a(){return m.resolve()}for(var s=0;s<i.forages.length;s++){var c=i.forages[s];c!==n&&r.push(c._initReady().catch(a))}var l=i.forages.slice(0);return m.all(r).then(function(){return t.db=i.db,V(t)}).then(function(u){return t.db=u,ie(t,n._defaultConfig.version)?ee(t):u}).then(function(u){t.db=i.db=u,n._dbInfo=t;for(var v=0;v<l.length;v++){var y=l[v];y!==n&&(y._dbInfo.db=t.db,y._dbInfo.version=t.version)}})}function tt(e,n){var t=this;e=P(e);var o=new m(function(i,r){t.ready().then(function(){K(t._dbInfo,X,function(a,s){if(a)return r(a);try{var c=s.objectStore(t._dbInfo.storeName),l=c.get(e);l.onsuccess=function(){var u=l.result;u===void 0&&(u=null),De(u)&&(u=J(u)),i(u)},l.onerror=function(){r(l.error)}}catch(u){r(u)}})}).catch(r)});return b(o,n),o}function nt(e,n){var t=this,o=new m(function(i,r){t.ready().then(function(){K(t._dbInfo,X,function(a,s){if(a)return r(a);try{var c=s.objectStore(t._dbInfo.storeName),l=c.openCursor(),u=1;l.onsuccess=function(){var v=l.result;if(v){var y=v.value;De(y)&&(y=J(y));var w=e(y,v.key,u++);w!==void 0?i(w):v.continue()}else i()},l.onerror=function(){r(l.error)}}catch(v){r(v)}})}).catch(r)});return b(o,n),o}function rt(e,n,t){var o=this;e=P(e);var i=new m(function(r,a){var s;o.ready().then(function(){return s=o._dbInfo,ue.call(n)==="[object Blob]"?D(s.db).then(function(c){return c?n:fe(n)}):n}).then(function(c){K(o._dbInfo,d,function(l,u){if(l)return a(l);try{var v=u.objectStore(o._dbInfo.storeName);c===null&&(c=void 0);var y=v.put(c,e);u.oncomplete=function(){c===void 0&&(c=null),r(c)},u.onabort=u.onerror=function(){var w=y.error?y.error:y.transaction.error;a(w)}}catch(w){a(w)}})}).catch(a)});return b(i,t),i}function ot(e,n){var t=this;e=P(e);var o=new m(function(i,r){t.ready().then(function(){K(t._dbInfo,d,function(a,s){if(a)return r(a);try{var c=s.objectStore(t._dbInfo.storeName),l=c.delete(e);s.oncomplete=function(){i()},s.onerror=function(){r(l.error)},s.onabort=function(){var u=l.error?l.error:l.transaction.error;r(u)}}catch(u){r(u)}})}).catch(r)});return b(o,n),o}function it(e){var n=this,t=new m(function(o,i){n.ready().then(function(){K(n._dbInfo,d,function(r,a){if(r)return i(r);try{var s=a.objectStore(n._dbInfo.storeName),c=s.clear();a.oncomplete=function(){o()},a.onabort=a.onerror=function(){var l=c.error?c.error:c.transaction.error;i(l)}}catch(l){i(l)}})}).catch(i)});return b(t,e),t}function at(e){var n=this,t=new m(function(o,i){n.ready().then(function(){K(n._dbInfo,X,function(r,a){if(r)return i(r);try{var s=a.objectStore(n._dbInfo.storeName),c=s.count();c.onsuccess=function(){o(c.result)},c.onerror=function(){i(c.error)}}catch(l){i(l)}})}).catch(i)});return b(t,e),t}function st(e,n){var t=this,o=new m(function(i,r){if(e<0){i(null);return}t.ready().then(function(){K(t._dbInfo,X,function(a,s){if(a)return r(a);try{var c=s.objectStore(t._dbInfo.storeName),l=!1,u=c.openKeyCursor();u.onsuccess=function(){var v=u.result;if(!v){i(null);return}e===0||l?i(v.key):(l=!0,v.advance(e))},u.onerror=function(){r(u.error)}}catch(v){r(v)}})}).catch(r)});return b(o,n),o}function ct(e){var n=this,t=new m(function(o,i){n.ready().then(function(){K(n._dbInfo,X,function(r,a){if(r)return i(r);try{var s=a.objectStore(n._dbInfo.storeName),c=s.openKeyCursor(),l=[];c.onsuccess=function(){var u=c.result;if(!u){o(l);return}l.push(u.key),u.continue()},c.onerror=function(){i(c.error)}}catch(u){i(u)}})}).catch(i)});return b(t,e),t}function lt(e,n){n=q.apply(this,arguments);var t=this.config();e=typeof e!="function"&&e||{},e.name||(e.name=e.name||t.name,e.storeName=e.storeName||t.storeName);var o=this,i;if(!e.name)i=m.reject("Invalid arguments");else{var r=e.name===t.name&&o._dbInfo.db,a=r?m.resolve(o._dbInfo.db):V(e).then(function(s){var c=M[e.name],l=c.forages;c.db=s;for(var u=0;u<l.length;u++)l[u]._dbInfo.db=s;return s});e.storeName?i=a.then(function(s){if(s.objectStoreNames.contains(e.storeName)){var c=s.version+1;H(e);var l=M[e.name],u=l.forages;s.close();for(var v=0;v<u.length;v++){var y=u[v];y._dbInfo.db=null,y._dbInfo.version=c}var w=new m(function(I,z){var R=C.open(e.name,c);R.onerror=function(Y){var ne=R.result;ne.close(),z(Y)},R.onupgradeneeded=function(){var Y=R.result;Y.deleteObjectStore(e.storeName)},R.onsuccess=function(){var Y=R.result;Y.close(),I(Y)}});return w.then(function(I){l.db=I;for(var z=0;z<u.length;z++){var R=u[z];R._dbInfo.db=I,k(R._dbInfo)}}).catch(function(I){throw($(e,I)||m.resolve()).catch(function(){}),I})}}):i=a.then(function(s){H(e);var c=M[e.name],l=c.forages;s.close();for(var u=0;u<l.length;u++){var v=l[u];v._dbInfo.db=null}var y=new m(function(w,I){var z=C.deleteDatabase(e.name);z.onerror=function(){var R=z.result;R&&R.close(),I(z.error)},z.onblocked=function(){console.warn('dropInstance blocked for database "'+e.name+'" until all open connections are closed')},z.onsuccess=function(){var R=z.result;R&&R.close(),w(R)}});return y.then(function(w){c.db=w;for(var I=0;I<l.length;I++){var z=l[I];k(z._dbInfo)}}).catch(function(w){throw($(e,w)||m.resolve()).catch(function(){}),w})})}return b(i,n),i}var ut={_driver:"asyncStorage",_initStorage:et,_support:L(),iterate:nt,getItem:tt,setItem:rt,removeItem:ot,clear:it,length:at,key:st,keys:ct,dropInstance:lt};function ft(){return typeof openDatabase=="function"}var j="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",dt="~~local_forage_type~",Le=/^~~local_forage_type~([^~]+)~/,ae="__lfsc__:",de=ae.length,he="arbf",me="blob",Ce="si08",Re="ui08",Ne="uic8",ze="si16",Pe="si32",Be="ur16",Oe="ui32",Me="fl32",He="fl64",Fe=de+he.length,ke=Object.prototype.toString;function $e(e){var n=e.length*.75,t=e.length,o,i=0,r,a,s,c;e[e.length-1]==="="&&(n--,e[e.length-2]==="="&&n--);var l=new ArrayBuffer(n),u=new Uint8Array(l);for(o=0;o<t;o+=4)r=j.indexOf(e[o]),a=j.indexOf(e[o+1]),s=j.indexOf(e[o+2]),c=j.indexOf(e[o+3]),u[i++]=r<<2|a>>4,u[i++]=(a&15)<<4|s>>2,u[i++]=(s&3)<<6|c&63;return l}function ve(e){var n=new Uint8Array(e),t="",o;for(o=0;o<n.length;o+=3)t+=j[n[o]>>2],t+=j[(n[o]&3)<<4|n[o+1]>>4],t+=j[(n[o+1]&15)<<2|n[o+2]>>6],t+=j[n[o+2]&63];return n.length%3===2?t=t.substring(0,t.length-1)+"=":n.length%3===1&&(t=t.substring(0,t.length-2)+"=="),t}function ht(e,n){var t="";if(e&&(t=ke.call(e)),e&&(t==="[object ArrayBuffer]"||e.buffer&&ke.call(e.buffer)==="[object ArrayBuffer]")){var o,i=ae;e instanceof ArrayBuffer?(o=e,i+=he):(o=e.buffer,t==="[object Int8Array]"?i+=Ce:t==="[object Uint8Array]"?i+=Re:t==="[object Uint8ClampedArray]"?i+=Ne:t==="[object Int16Array]"?i+=ze:t==="[object Uint16Array]"?i+=Be:t==="[object Int32Array]"?i+=Pe:t==="[object Uint32Array]"?i+=Oe:t==="[object Float32Array]"?i+=Me:t==="[object Float64Array]"?i+=He:n(new Error("Failed to get type for BinaryArray"))),n(i+ve(o))}else if(t==="[object Blob]"){var r=new FileReader;r.onload=function(){var a=dt+e.type+"~"+ve(this.result);n(ae+me+a)},r.readAsArrayBuffer(e)}else try{n(JSON.stringify(e))}catch(a){console.error("Couldn't convert value into a JSON string: ",e),n(null,a)}}function mt(e){if(e.substring(0,de)!==ae)return JSON.parse(e);var n=e.substring(Fe),t=e.substring(de,Fe),o;if(t===me&&Le.test(n)){var i=n.match(Le);o=i[1],n=n.substring(i[0].length)}var r=$e(n);switch(t){case he:return r;case me:return F([r],{type:o});case Ce:return new Int8Array(r);case Re:return new Uint8Array(r);case Ne:return new Uint8ClampedArray(r);case ze:return new Int16Array(r);case Be:return new Uint16Array(r);case Pe:return new Int32Array(r);case Oe:return new Uint32Array(r);case Me:return new Float32Array(r);case He:return new Float64Array(r);default:throw new Error("Unkown type: "+t)}}var pe={serialize:ht,deserialize:mt,stringToBuffer:$e,bufferToString:ve};function Ue(e,n,t,o){e.executeSql("CREATE TABLE IF NOT EXISTS "+n.storeName+" (id INTEGER PRIMARY KEY, key unique, value)",[],t,o)}function vt(e){var n=this,t={db:null};if(e)for(var o in e)t[o]=typeof e[o]!="string"?e[o].toString():e[o];var i=new m(function(r,a){try{t.db=openDatabase(t.name,String(t.version),t.description,t.size)}catch(s){return a(s)}t.db.transaction(function(s){Ue(s,t,function(){n._dbInfo=t,r()},function(c,l){a(l)})},a)});return t.serializer=pe,i}function Q(e,n,t,o,i,r){e.executeSql(t,o,i,function(a,s){s.code===s.SYNTAX_ERR?a.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name = ?",[n.storeName],function(c,l){l.rows.length?r(c,s):Ue(c,n,function(){c.executeSql(t,o,i,r)},r)},r):r(a,s)},r)}function pt(e,n){var t=this;e=P(e);var o=new m(function(i,r){t.ready().then(function(){var a=t._dbInfo;a.db.transaction(function(s){Q(s,a,"SELECT * FROM "+a.storeName+" WHERE key = ? LIMIT 1",[e],function(c,l){var u=l.rows.length?l.rows.item(0).value:null;u&&(u=a.serializer.deserialize(u)),i(u)},function(c,l){r(l)})})}).catch(r)});return b(o,n),o}function gt(e,n){var t=this,o=new m(function(i,r){t.ready().then(function(){var a=t._dbInfo;a.db.transaction(function(s){Q(s,a,"SELECT * FROM "+a.storeName,[],function(c,l){for(var u=l.rows,v=u.length,y=0;y<v;y++){var w=u.item(y),I=w.value;if(I&&(I=a.serializer.deserialize(I)),I=e(I,w.key,y+1),I!==void 0){i(I);return}}i()},function(c,l){r(l)})})}).catch(r)});return b(o,n),o}function Ye(e,n,t,o){var i=this;e=P(e);var r=new m(function(a,s){i.ready().then(function(){n===void 0&&(n=null);var c=n,l=i._dbInfo;l.serializer.serialize(n,function(u,v){v?s(v):l.db.transaction(function(y){Q(y,l,"INSERT OR REPLACE INTO "+l.storeName+" (key, value) VALUES (?, ?)",[e,u],function(){a(c)},function(w,I){s(I)})},function(y){if(y.code===y.QUOTA_ERR){if(o>0){a(Ye.apply(i,[e,c,t,o-1]));return}s(y)}})})}).catch(s)});return b(r,t),r}function bt(e,n,t){return Ye.apply(this,[e,n,t,1])}function yt(e,n){var t=this;e=P(e);var o=new m(function(i,r){t.ready().then(function(){var a=t._dbInfo;a.db.transaction(function(s){Q(s,a,"DELETE FROM "+a.storeName+" WHERE key = ?",[e],function(){i()},function(c,l){r(l)})})}).catch(r)});return b(o,n),o}function _t(e){var n=this,t=new m(function(o,i){n.ready().then(function(){var r=n._dbInfo;r.db.transaction(function(a){Q(a,r,"DELETE FROM "+r.storeName,[],function(){o()},function(s,c){i(c)})})}).catch(i)});return b(t,e),t}function xt(e){var n=this,t=new m(function(o,i){n.ready().then(function(){var r=n._dbInfo;r.db.transaction(function(a){Q(a,r,"SELECT COUNT(key) as c FROM "+r.storeName,[],function(s,c){var l=c.rows.item(0).c;o(l)},function(s,c){i(c)})})}).catch(i)});return b(t,e),t}function Et(e,n){var t=this,o=new m(function(i,r){t.ready().then(function(){var a=t._dbInfo;a.db.transaction(function(s){Q(s,a,"SELECT key FROM "+a.storeName+" WHERE id = ? LIMIT 1",[e+1],function(c,l){var u=l.rows.length?l.rows.item(0).key:null;i(u)},function(c,l){r(l)})})}).catch(r)});return b(o,n),o}function St(e){var n=this,t=new m(function(o,i){n.ready().then(function(){var r=n._dbInfo;r.db.transaction(function(a){Q(a,r,"SELECT key FROM "+r.storeName,[],function(s,c){for(var l=[],u=0;u<c.rows.length;u++)l.push(c.rows.item(u).key);o(l)},function(s,c){i(c)})})}).catch(i)});return b(t,e),t}function wt(e){return new m(function(n,t){e.transaction(function(o){o.executeSql("SELECT name FROM sqlite_master WHERE type='table' AND name <> '__WebKitDatabaseInfoTable__'",[],function(i,r){for(var a=[],s=0;s<r.rows.length;s++)a.push(r.rows.item(s).name);n({db:e,storeNames:a})},function(i,r){t(r)})},function(o){t(o)})})}function It(e,n){n=q.apply(this,arguments);var t=this.config();e=typeof e!="function"&&e||{},e.name||(e.name=e.name||t.name,e.storeName=e.storeName||t.storeName);var o=this,i;return e.name?i=new m(function(r){var a;e.name===t.name?a=o._dbInfo.db:a=openDatabase(e.name,"","",0),e.storeName?r({db:a,storeNames:[e.storeName]}):r(wt(a))}).then(function(r){return new m(function(a,s){r.db.transaction(function(c){function l(w){return new m(function(I,z){c.executeSql("DROP TABLE IF EXISTS "+w,[],function(){I()},function(R,Y){z(Y)})})}for(var u=[],v=0,y=r.storeNames.length;v<y;v++)u.push(l(r.storeNames[v]));m.all(u).then(function(){a()}).catch(function(w){s(w)})},function(c){s(c)})})}):i=m.reject("Invalid arguments"),b(i,n),i}var Tt={_driver:"webSQLStorage",_initStorage:vt,_support:ft(),iterate:gt,getItem:pt,setItem:bt,removeItem:yt,clear:_t,length:xt,key:Et,keys:St,dropInstance:It};function Dt(){try{return typeof localStorage<"u"&&"setItem"in localStorage&&!!localStorage.setItem}catch{return!1}}function qe(e,n){var t=e.name+"/";return e.storeName!==n.storeName&&(t+=e.storeName+"/"),t}function At(){var e="_localforage_support_test";try{return localStorage.setItem(e,!0),localStorage.removeItem(e),!1}catch{return!0}}function Lt(){return!At()||localStorage.length>0}function Ct(e){var n=this,t={};if(e)for(var o in e)t[o]=e[o];return t.keyPrefix=qe(e,n._defaultConfig),Lt()?(n._dbInfo=t,t.serializer=pe,m.resolve()):m.reject()}function Rt(e){var n=this,t=n.ready().then(function(){for(var o=n._dbInfo.keyPrefix,i=localStorage.length-1;i>=0;i--){var r=localStorage.key(i);r.indexOf(o)===0&&localStorage.removeItem(r)}});return b(t,e),t}function Nt(e,n){var t=this;e=P(e);var o=t.ready().then(function(){var i=t._dbInfo,r=localStorage.getItem(i.keyPrefix+e);return r&&(r=i.serializer.deserialize(r)),r});return b(o,n),o}function zt(e,n){var t=this,o=t.ready().then(function(){for(var i=t._dbInfo,r=i.keyPrefix,a=r.length,s=localStorage.length,c=1,l=0;l<s;l++){var u=localStorage.key(l);if(u.indexOf(r)===0){var v=localStorage.getItem(u);if(v&&(v=i.serializer.deserialize(v)),v=e(v,u.substring(a),c++),v!==void 0)return v}}});return b(o,n),o}function Pt(e,n){var t=this,o=t.ready().then(function(){var i=t._dbInfo,r;try{r=localStorage.key(e)}catch{r=null}return r&&(r=r.substring(i.keyPrefix.length)),r});return b(o,n),o}function Bt(e){var n=this,t=n.ready().then(function(){for(var o=n._dbInfo,i=localStorage.length,r=[],a=0;a<i;a++){var s=localStorage.key(a);s.indexOf(o.keyPrefix)===0&&r.push(s.substring(o.keyPrefix.length))}return r});return b(t,e),t}function Ot(e){var n=this,t=n.keys().then(function(o){return o.length});return b(t,e),t}function Mt(e,n){var t=this;e=P(e);var o=t.ready().then(function(){var i=t._dbInfo;localStorage.removeItem(i.keyPrefix+e)});return b(o,n),o}function Ht(e,n,t){var o=this;e=P(e);var i=o.ready().then(function(){n===void 0&&(n=null);var r=n;return new m(function(a,s){var c=o._dbInfo;c.serializer.serialize(n,function(l,u){if(u)s(u);else try{localStorage.setItem(c.keyPrefix+e,l),a(r)}catch(v){(v.name==="QuotaExceededError"||v.name==="NS_ERROR_DOM_QUOTA_REACHED")&&s(v),s(v)}})})});return b(i,t),i}function Ft(e,n){if(n=q.apply(this,arguments),e=typeof e!="function"&&e||{},!e.name){var t=this.config();e.name=e.name||t.name,e.storeName=e.storeName||t.storeName}var o=this,i;return e.name?i=new m(function(r){e.storeName?r(qe(e,o._defaultConfig)):r(e.name+"/")}).then(function(r){for(var a=localStorage.length-1;a>=0;a--){var s=localStorage.key(a);s.indexOf(r)===0&&localStorage.removeItem(s)}}):i=m.reject("Invalid arguments"),b(i,n),i}var kt={_driver:"localStorageWrapper",_initStorage:Ct,_support:Dt(),iterate:zt,getItem:Nt,setItem:Ht,removeItem:Mt,clear:Rt,length:Ot,key:Pt,keys:Bt,dropInstance:Ft},$t=function(n,t){return n===t||typeof n=="number"&&typeof t=="number"&&isNaN(n)&&isNaN(t)},Ut=function(n,t){for(var o=n.length,i=0;i<o;){if($t(n[i],t))return!0;i++}return!1},We=Array.isArray||function(e){return Object.prototype.toString.call(e)==="[object Array]"},te={},Ve={},Z={INDEXEDDB:ut,WEBSQL:Tt,LOCALSTORAGE:kt},Yt=[Z.INDEXEDDB._driver,Z.WEBSQL._driver,Z.LOCALSTORAGE._driver],se=["dropInstance"],ge=["clear","getItem","iterate","key","keys","length","removeItem","setItem"].concat(se),qt={description:"",driver:Yt.slice(),name:"localforage",size:4980736,storeName:"keyvaluepairs",version:1};function Wt(e,n){e[n]=function(){var t=arguments;return e.ready().then(function(){return e[n].apply(e,t)})}}function be(){for(var e=1;e<arguments.length;e++){var n=arguments[e];if(n)for(var t in n)n.hasOwnProperty(t)&&(We(n[t])?arguments[0][t]=n[t].slice():arguments[0][t]=n[t])}return arguments[0]}var Vt=function(){function e(n){O(this,e);for(var t in Z)if(Z.hasOwnProperty(t)){var o=Z[t],i=o._driver;this[t]=i,te[i]||this.defineDriver(o)}this._defaultConfig=be({},qt),this._config=be({},this._defaultConfig,n),this._driverSet=null,this._initDriver=null,this._ready=!1,this._dbInfo=null,this._wrapLibraryMethodsWithReady(),this.setDriver(this._config.driver).catch(function(){})}return e.prototype.config=function(t){if((typeof t>"u"?"undefined":T(t))==="object"){if(this._ready)return new Error("Can't call config() after localforage has been used.");for(var o in t){if(o==="storeName"&&(t[o]=t[o].replace(/\W/g,"_")),o==="version"&&typeof t[o]!="number")return new Error("Database version must be a number.");this._config[o]=t[o]}return"driver"in t&&t.driver?this.setDriver(this._config.driver):!0}else return typeof t=="string"?this._config[t]:this._config},e.prototype.defineDriver=function(t,o,i){var r=new m(function(a,s){try{var c=t._driver,l=new Error("Custom driver not compliant; see https://mozilla.github.io/localForage/#definedriver");if(!t._driver){s(l);return}for(var u=ge.concat("_initStorage"),v=0,y=u.length;v<y;v++){var w=u[v],I=!Ut(se,w);if((I||t[w])&&typeof t[w]!="function"){s(l);return}}var z=function(){for(var ne=function(Qt){return function(){var Gt=new Error("Method "+Qt+" is not implemented by the current driver"),Ke=m.reject(Gt);return b(Ke,arguments[arguments.length-1]),Ke}},ye=0,jt=se.length;ye<jt;ye++){var _e=se[ye];t[_e]||(t[_e]=ne(_e))}};z();var R=function(ne){te[c]&&console.info("Redefining LocalForage driver: "+c),te[c]=t,Ve[c]=ne,a()};"_support"in t?t._support&&typeof t._support=="function"?t._support().then(R,s):R(!!t._support):R(!0)}catch(Y){s(Y)}});return N(r,o,i),r},e.prototype.driver=function(){return this._driver||null},e.prototype.getDriver=function(t,o,i){var r=te[t]?m.resolve(te[t]):m.reject(new Error("Driver not found."));return N(r,o,i),r},e.prototype.getSerializer=function(t){var o=m.resolve(pe);return N(o,t),o},e.prototype.ready=function(t){var o=this,i=o._driverSet.then(function(){return o._ready===null&&(o._ready=o._initDriver()),o._ready});return N(i,t,t),i},e.prototype.setDriver=function(t,o,i){var r=this;We(t)||(t=[t]);var a=this._getSupportedDrivers(t);function s(){r._config.driver=r.driver()}function c(v){return r._extend(v),s(),r._ready=r._initStorage(r._config),r._ready}function l(v){return function(){var y=0;function w(){for(;y<v.length;){var I=v[y];return y++,r._dbInfo=null,r._ready=null,r.getDriver(I).then(c).catch(w)}s();var z=new Error("No available storage method found.");return r._driverSet=m.reject(z),r._driverSet}return w()}}var u=this._driverSet!==null?this._driverSet.catch(function(){return m.resolve()}):m.resolve();return this._driverSet=u.then(function(){var v=a[0];return r._dbInfo=null,r._ready=null,r.getDriver(v).then(function(y){r._driver=y._driver,s(),r._wrapLibraryMethodsWithReady(),r._initDriver=l(a)})}).catch(function(){s();var v=new Error("No available storage method found.");return r._driverSet=m.reject(v),r._driverSet}),N(this._driverSet,o,i),this._driverSet},e.prototype.supports=function(t){return!!Ve[t]},e.prototype._extend=function(t){be(this,t)},e.prototype._getSupportedDrivers=function(t){for(var o=[],i=0,r=t.length;i<r;i++){var a=t[i];this.supports(a)&&o.push(a)}return o},e.prototype._wrapLibraryMethodsWithReady=function(){for(var t=0,o=ge.length;t<o;t++)Wt(this,ge[t])},e.prototype.createInstance=function(t){return new e(t)},e}(),Kt=new Vt;g.exports=Kt},{3:3}]},{},[4])(4)})});var xe=class extends HTMLElement{constructor(){super(),this.innerHTML=`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-title></ion-title>
                    <ion-buttons slot="end">
                        <ion-button id="menu-button">
                            <ion-icon name="menu-outline" slot="icon-only"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
        `,this.render()}render(){let f=this.querySelector("ion-button#menu-button");if(!f)return;let h=document.querySelector("ion-menu");h&&f.addEventListener("click",()=>{h.open()})}static get observedAttributes(){return["title","color"]}attributeChangedCallback(f="",h="",p=""){if(f==="title"&&h!==p){let g=this.querySelector("ion-title");if(!g)return;g.innerText=p}if(f==="color"&&h!==p){let g=this.querySelector("ion-icon");if(!g)return;g.color=p}}};window.customElements.define("remesafa-header",xe);var Ee=class extends HTMLElement{connectedCallback(){this.innerHTML=`
            <ion-menu content-id="main-content" menu-id="main-menu" side="start">
                <ion-header class="ion-no-border">
                    <ion-buttons slot="start">
                        <ion-button color="secondary">
                            <ion-icon slot="icon-only" name="close-outline"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-header>
                <ion-content class="ion-padding">
                    <div class="logo-container ion-padding-bottom">
                        <img alt="logo" src="assets/images/fa_transferCash.png" class="logo" />
                    </div>
                    <ion-list>
                        <ion-menu-toggle auto-hide="false"> <!-- este elemento cierra el menu -->
                            <ion-item href="/" detail button lines="full">
                                <ion-icon name="information-circle-outline" slot="start" color="primary"></ion-icon>
                                <ion-label class="ion-text-capitalize">tasas del dia</ion-label>
                            </ion-item>
                        </ion-menu-toggle>
                        <ion-menu-toggle auto-hide="false"> <!-- este elemento cierra el menu -->
                            <ion-item href="/calculator" detail button lines="full">
                                <ion-icon slot="start" color="primary" name="calculator-outline"></ion-icon>
                                <ion-label class="ion-text-capitalize">simulador</ion-label>
                            </ion-item>
                        </ion-menu-toggle>
                    </ion-list>
                </ion-content>
            </ion-menu>
        `,this.onMount()}onMount(){let f=this.querySelector("ion-menu");if(!f)return;let h=this.querySelector("ion-button");h&&h.addEventListener("click",()=>f.close())}};window.customElements.define("remesafa-menu",Ee);var oe=on(Ge()),ce={SecureStorage:"ionicSecureStorage",IndexedDB:oe.default.INDEXEDDB,LocalStorage:oe.default.LOCALSTORAGE},Xe={name:"_ionicstorage",storeName:"_ionickv",dbKey:"_ionickey",driverOrder:[ce.SecureStorage,ce.IndexedDB,ce.LocalStorage]},le=class{constructor(f=Xe){this._db=null,this._secureStorageDriver=null;let h=Object.assign({},Xe,f||{});this._config=h}async create(){let f=oe.default.createInstance(this._config);return this._db=f,await f.setDriver(this._config.driverOrder||[]),this}async defineDriver(f){return f._driver===ce.SecureStorage&&(this._secureStorageDriver=f),oe.default.defineDriver(f)}get driver(){var f;return((f=this._db)===null||f===void 0?void 0:f.driver())||null}assertDb(){if(!this._db)throw new Error("Database not created. Must call create() first");return this._db}get(f){return this.assertDb().getItem(f)}set(f,h){return this.assertDb().setItem(f,h)}remove(f){return this.assertDb().removeItem(f)}clear(){return this.assertDb().clear()}length(){return this.assertDb().length()}keys(){return this.assertDb().keys()}forEach(f){return this.assertDb().iterate(f)}setEncryptionKey(f){var h;if(this._secureStorageDriver)(h=this._secureStorageDriver)===null||h===void 0||h.setEncryptionKey(f);else throw new Error("@ionic-enterprise/secure-storage not installed. Encryption support not available")}};var G=class x{static getInstance(){return x.instance||(x.instance=new x),x.instance}constructor(){this.init()}async init(){this.storage=await new le().create()}};var we=class extends HTMLElement{constructor(){super();this.result=null;this.data={send_from:"",send_to:"",mount:0,special_tax:!1,mount_special:0},this.storageService=G.getInstance(),this.storageService.storage.get("info").then(h=>this.info=h)}connectedCallback(){this.innerHTML=`
            <remesafa-header title="Simulador de envio" color="primary"></remesafa-header>
            <ion-content color="light" class="ion-padding">
                <form>
                    <ion-list>
                        <ion-item>
                            <ion-select value="" label-placement="floating" name="send_from" label="Indique de donde realiza la remesa">     
                                <ion-select-option value="" selected>Seleccione ...</ion-select-option>
                                <ion-select-option value="CLP">Chile</ion-select-option>
                                <ion-select-option value="PEN">Peru</ion-select-option>
                            </ion-select>
                        </ion-item>
                        <ion-item>
                            <ion-select value="" label-placement="floating" name="send_to" label="Indique el destino de la remesa">     
                                <ion-select-option value="" selected>Seleccione ...</ion-select-option>
                                <ion-select-option value="VES">Venezuela</ion-select-option>
                                <ion-select-option value="CLP">Chile</ion-select-option>
                                <ion-select-option value="PEN">Peru</ion-select-option>
                            </ion-select>
                        </ion-item>
                        <ion-item>
                            <ion-checkbox justify="space-between" id="special-tax">
                                Tasa Especial
                            </ion-checkbox>
                        </ion-item>
                        <ion-item id="special-field" style="display: none;">
                            <ion-input 
                                name="mount_special"
                                type="number" 
                                label="Ingrese monto tasa especial" 
                                label-placement="floating"
                                step="0.00001"
                            ></ion-input>
                        </ion-item>
                        <ion-item>
                            <ion-input 
                                min="1"
                                name="mount"
                                type="number" 
                                label="Ingrese monto a enviar" 
                                label-placement="floating"
                            ></ion-input>
                        </ion-item>
                    </ion-list>
                    <div class="ion-text-center ion-margin-top">
                        <ion-button type="reset">Limpiar</ion-button>
                        <ion-button type="submit">Simular envio</ion-button>
                    </div>                
                </form>

                <!-- resultados de la simulacion -->
                <div class="result" id="result"></div>

            </ion-content>
            <ion-footer class="ion-padding-bottom ion-padding-top ion-no-border ion-text-center">    
                <ion-router-link href="/">
                    <ion-button color="secondary">
                        <ion-icon name="cash-outline" class="ion-margin-end"></ion-icon>
                        Consultar las tasas
                    </ion-button>
                </ion-router-link>
            </ion-footer>
        `,this.onMount()}onMount(){if(this.result=this.querySelector("#result"),!this.result)return;let h=this.querySelector("form");if(!h)return;h.addEventListener("submit",this.handleSubmit.bind(this));let p=Array.from(this.querySelectorAll("ion-select"));if(p.length===0)return;p.forEach(E=>{E.addEventListener("ionChange",C=>{let{value:L,name:F}=C.target;this.data[F]=L})});let g=this.querySelector('ion-input[name="mount"]');if(!g)return;g.addEventListener("ionInput",E=>{let{name:C,value:L}=E.target;this.data[C]=Number(L)});let A=this.querySelector('ion-input[name="mount_special"]');if(!A)return;A.addEventListener("ionInput",E=>{let{name:C,value:L}=E.target;this.data[C]=Number(L)});let T=this.querySelector('ion-button[type="reset"]');if(!T)return;T.addEventListener("click",()=>this.handleReset(p,g));let O=this.querySelector("ion-checkbox#special-tax");O&&O.addEventListener("ionChange",this.handleCheck.bind(this))}handleReset(h,p){this.data={mount:0,send_from:"",send_to:"",mount_special:0,special_tax:!1},h.forEach(T=>T.value=""),p.value="",this.result&&(this.result.innerHTML="");let g=this.querySelector("ion-checkbox");if(!g)return;g.checked=this.data.special_tax;let A=this.querySelector("#special-field");A&&(A.style.display="none")}handleCheck(h){let p=this.querySelector("#special-field");if(p)if(this.data.special_tax=h.target.checked,this.data.special_tax)p.style.display="";else{this.data.mount_special=0,p.style.display="none";let g=p.querySelector("ion-input");if(!g)return;g.value=""}}async handleSubmit(h){h.preventDefault();let p=0,g=0,A="";if(this.data.send_to===""||this.data.send_from===""){await this.showAlert("Completa los datos para simular el envio");return}if(this.data.send_from===this.data.send_to){await this.showAlert("No se puede enviar remesas dentro del mismo pais");return}if(this.data.send_from==="CLP"&&this.data.send_to==="VES")if(this.info?.taxes.tax_venezuela_chile)g=Number(this.info.taxes.tax_venezuela_chile),A="Bs.";else{await this.showAlert("La tasa de Chile Hacia Venezuela no esta establecida, por favor actualiza la tasa");return}else if(this.data.send_from==="PEN"&&this.data.send_to==="VES")if(this.info?.taxes.tax_venezuela_peru)g=Number(this.info.taxes.tax_venezuela_peru),A="Bs";else{await this.showAlert("La tasa de Peru Hacia Venezuela no esta establecida, por favor actualiza la tasa");return}else if(this.data.send_from==="CLP"&&this.data.send_to==="PEN")if(this.info?.taxes.tax_chile_peru)g=Number(this.info.taxes.tax_chile_peru),A="/s";else{await this.showAlert("La tasa de Chile Hacia Peru no esta establecida, por favor actualiza la tasa");return}else if(this.info?.taxes.tax_peru_chile)g=Number(this.info.taxes.tax_peru_chile),A="$";else{await this.showAlert("La tasa de Peru Hacia Chile no esta establecida, por favor actualiza la tasa");return}if(this.data.special_tax&&!this.data.mount_special){await this.showAlert("debe proporcionar un monto para calcular tasa especial");return}p=this.data.special_tax?Number(this.data.mount)*Number(this.data.mount_special):Number(this.data.mount)*g,this.renderResult(p,A)}renderResult(h,p){let g=this.querySelector("#result");g&&(g.innerHTML=`
            <ion-card class="ion-padding" color="success">
                <ion-card-content>
                    <h1>Total a recibir: <span class="ion-margin-start"><b>${h.toFixed(2)}</b></span> ${p}</h1>
                </ion-card-content>
            </ion-card>
        `)}async showAlert(h){let p={backdropDismiss:!1,header:"Atencion",message:h,buttons:[{text:"Entendido",role:"confirm"}]},g=Object.assign(document.createElement("ion-alert"),p);this.appendChild(g),await g.present(),await g.onDidDismiss(),g.remove()}};window.customElements.define("page-calculator",we);var Ie=class extends HTMLElement{constructor(){super(),this.taxes={tax_chile_peru:"",tax_peru_chile:"",tax_venezuela_chile:"",tax_venezuela_peru:""},this.storageService=G.getInstance(),this.storageService.storage?.get("info").then(f=>{f&&(this.taxes=f.taxes,this.connectedCallback())})}connectedCallback(){this.innerHTML=`
            <remesafa-header title="Tasas del dia" color="primary"></remesafa-header>
            <ion-content color="light" class="ion-padding-top">
                <div class="logo-container ion-padding-bottom">
                    <img alt="logo" src="assets/images/fa_transferCash.png" class="logo" />
                </div>
                <ion-grid>
                    <ion-row>
                        <ion-col size="12">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="calendar-outline"></ion-icon>
                                <span id="date" class="ion-margin-start ion-text-capitalize"></span>
                            </ion-text>
                        </ion-col>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa hacia Venezuela
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/chile.png" alt="chile" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 pesos (CLP)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_venezuela_chile" color="dark">
                                                        ${this.taxes.tax_venezuela_chile.length>0?this.taxes.tax_venezuela_chile:"-"}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Bolivares (Bs)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/venezuela.png" />
                                    </ion-avatar>
                                </ion-item>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 sol (PEN)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_venezuela_peru" color="dark">
                                                        ${this.taxes.tax_venezuela_peru.length>0?this.taxes.tax_venezuela_peru:"-"}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Bolivares (Bs)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/venezuela.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button color="primary" id="tax-to-ven">Actualizar tasas</ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>

                <ion-grid>
                    <ion-row>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa Peru hacia Chile
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 sol (PEN)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_peru_chile" color="dark">
                                                        ${this.taxes.tax_peru_chile.length>0?this.taxes.tax_peru_chile:"-"}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Pesos (CLP)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/chile.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>                
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button id="tax-to-chile" color="primary">Actualizar tasa</ion-button>
                        </ion-col>
                    </ion-row>

                    <!-- chile a peru -->
                    <ion-row>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa Chile hacia Peru
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/chile.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 Peso (CLP)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_chile_peru" color="dark">
                                                        ${this.taxes.tax_chile_peru.length>0?this.taxes.tax_chile_peru:"-"}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Soles (PEN)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>                
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button color="primary" id="tax-to-peru">Actualizar tasa</ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>    
            </ion-content>
            <ion-footer class="ion-padding-bottom ion-padding-top ion-no-border ion-text-center">    
                <ion-router-link href="/calculator">
                    <ion-button color="secondary">
                        <ion-icon slot="start" name="calculator-outline"></ion-icon>
                        <ion-label>Iniciar simulacion de envio<ion-label>
                    </ion-button> 
                </ion-router-link>
            </ion-footer>
        `,this.onMount()}onMount(){this.setDate();let f=this.querySelector("ion-button#tax-to-ven");if(!f)return;let h=this.querySelector("ion-button#tax-to-chile");if(!h)return;let p=this.querySelector("ion-button#tax-to-peru");p&&(h.addEventListener("click",()=>this.openPrompt("tax-chile")),p.addEventListener("click",()=>this.openPrompt("tax-peru")),f.addEventListener("click",()=>this.openPrompt("tax-ven")))}setDate(){let f=this.querySelector("span#date");if(!f)return;let h=new Date,p=h.toLocaleDateString("es-VE",{weekday:"long"}),g=h.getDate()>9?h.getDate().toString():"0"+h.getDate().toString(),A=h.toLocaleDateString("es-VE",{month:"long"}),T=h.getFullYear();f.innerText=`${p}, ${g} / ${A} / ${T}`}async openPrompt(f){let h=[];switch(f){case"tax-chile":h=[{placeholder:"Tasa Peru hacia Chile",type:"number",min:1,max:999999,name:"tax_peru_chile"}];break;case"tax-peru":h=[{placeholder:"Tasa Chile hacia Peru",type:"number",min:1,max:999999,name:"tax_chile_peru"}];break;default:h=[{placeholder:"Tasa Chile hacia Venezuela",type:"number",min:1,max:999999,name:"tax_venezuela_chile"},{placeholder:"Tasa Per\xFA hacia Venezuela",type:"number",min:1,max:999999,name:"tax_venezuela_peru"}];break}let p=Object.assign(document.createElement("ion-alert"),{backdropDismiss:!1,header:"Actualizar tasa(s)",buttons:[{text:"Cancelar",role:"cancel",cssClass:"danger"},{text:"Confirmar",role:"confirm",cssClass:"success"}],inputs:h});this.appendChild(p),await p.present();let g=await p.onDidDismiss();if(g&&g.role==="confirm"){let{values:A}=g.data;await this.handleResponsePrompt(A)}p.remove()}async handleResponsePrompt(f){Object.keys(f).forEach(p=>{p==="tax_chile_peru"&&(this.taxes.tax_chile_peru=f[p]),p==="tax_peru_chile"&&(this.taxes.tax_peru_chile=f[p]),p==="tax_venezuela_chile"&&(this.taxes.tax_venezuela_chile=f[p]),p==="tax_venezuela_peru"&&(this.taxes.tax_venezuela_peru=f[p])}),await this.storageService.storage?.set("info",{taxes:this.taxes}),this.renderTax();let h=Object.assign(document.createElement("ion-toast"),{duration:1500,position:"top",message:"Tasa(s) actualizadas con exito"});this.appendChild(h),await h.present(),await h.onDidDismiss(),h.remove()}renderTax(){Object.keys(this.taxes).forEach(f=>{let h=this.querySelector("#"+f);h&&(f==="tax_chile_peru"||f==="tax_peru_chile"||f==="tax_venezuela_chile"||f==="tax_venezuela_peru")&&(h.innerHTML=this.taxes[f]??"-")})}};window.customElements.define("page-info",Ie);var Te=class extends HTMLElement{connectedCallback(){this.innerHTML=`
            <ion-app>
                <remesafa-menu></remesafa-menu>
                <ion-router>
                    <ion-route url="/calculator" component="page-calculator"></ion-route>
                    <ion-route url="/" component="page-info"></ion-route>
                </ion-router>
                <ion-router-outlet id="main-content"></ion-router-outlet>
            </ion-app>
        `}};window.customElements.define("remesafa-app",Te);G.getInstance();new EventSource("/esbuild").addEventListener("change",()=>location.reload());})();
/*! Bundled license information:

localforage/dist/localforage.js:
  (*!
      localForage -- Offline Storage, Improved
      Version 1.10.0
      https://localforage.github.io/localForage
      (c) 2013-2017 Mozilla, Apache License 2.0
  *)
*/
