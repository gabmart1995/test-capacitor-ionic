export interface Tax {
    tax_chile_peru?: string,
    tax_peru_chile?: string,
    tax_venezuela_chile?: string,
    tax_venezuela_peru?: string
}