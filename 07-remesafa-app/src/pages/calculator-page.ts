import {AlertOptions, CheckboxChangeEventDetail, IonCheckboxCustomEvent} from "@ionic/core";
import {StorageService} from "../services/storage-service";
import {Tax} from "../interfaces/tax-interface";

class CalculatorPage extends HTMLElement {
    data: {
        send_from: string,
        send_to: string,
        mount: number,
        special_tax: boolean,
        mount_special: number,
    }
    info?: {taxes: Tax}
    result: HTMLDivElement | null = null;
    storageService: StorageService;
    
    constructor() {
        super();
        this.data = {
            send_from: '',
            send_to: '',
            mount: 0,
            special_tax: false,
            mount_special: 0
        }

        // cargamos los datos del store
        this.storageService = StorageService.getInstance();
        this.storageService.storage!.get('info')
            .then(info => this.info = info);
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <remesafa-header title="Simulador de envio" color="primary"></remesafa-header>
            <ion-content color="light" class="ion-padding">
                <form>
                    <ion-list>
                        <ion-item>
                            <ion-select value="" label-placement="floating" name="send_from" label="Indique de donde realiza la remesa">     
                                <ion-select-option value="" selected>Seleccione ...</ion-select-option>
                                <ion-select-option value="CLP">Chile</ion-select-option>
                                <ion-select-option value="PEN">Peru</ion-select-option>
                            </ion-select>
                        </ion-item>
                        <ion-item>
                            <ion-select value="" label-placement="floating" name="send_to" label="Indique el destino de la remesa">     
                                <ion-select-option value="" selected>Seleccione ...</ion-select-option>
                                <ion-select-option value="VES">Venezuela</ion-select-option>
                                <ion-select-option value="CLP">Chile</ion-select-option>
                                <ion-select-option value="PEN">Peru</ion-select-option>
                            </ion-select>
                        </ion-item>
                        <ion-item>
                            <ion-checkbox justify="space-between" id="special-tax">
                                Tasa Especial
                            </ion-checkbox>
                        </ion-item>
                        <ion-item id="special-field" style="display: none;">
                            <ion-input 
                                name="mount_special"
                                type="number" 
                                label="Ingrese monto tasa especial" 
                                label-placement="floating"
                                step="0.00001"
                            ></ion-input>
                        </ion-item>
                        <ion-item>
                            <ion-input 
                                min="1"
                                name="mount"
                                type="number" 
                                label="Ingrese monto a enviar" 
                                label-placement="floating"
                            ></ion-input>
                        </ion-item>
                    </ion-list>
                    <div class="ion-text-center ion-margin-top">
                        <ion-button type="reset">Limpiar</ion-button>
                        <ion-button type="submit">Simular envio</ion-button>
                    </div>                
                </form>

                <!-- resultados de la simulacion -->
                <div class="result" id="result"></div>

            </ion-content>
            <ion-footer class="ion-padding-bottom ion-padding-top ion-no-border ion-text-center">    
                <ion-router-link href="/">
                    <ion-button color="secondary">
                        <ion-icon name="cash-outline" class="ion-margin-end"></ion-icon>
                        Consultar las tasas
                    </ion-button>
                </ion-router-link>
            </ion-footer>
        `);

        this.onMount();
    }

    onMount() {
        this.result = this.querySelector('#result');
        if (!this.result) return;
        
        const form = this.querySelector<HTMLFormElement>('form');
        if (!form) return;
        
        form.addEventListener('submit', this.handleSubmit.bind(this));

        const selects: HTMLIonSelectElement[] = Array.from(this.querySelectorAll('ion-select'));
        if (selects.length === 0) return;

        selects.forEach(select => {
            select.addEventListener('ionChange', event => {
                const {value, name} = event.target;
                
                // @ts-ignore
                this.data[name] = value
            });
        });

        const inputMount = this.querySelector<HTMLIonInputElement>('ion-input[name="mount"]');
        if (!inputMount) return;

        inputMount.addEventListener('ionInput', event => {
            // @ts-ignore
            const {name, value} = event.target;

            // @ts-ignore
            this.data[name] = Number(value);
        });

        const inputSpecial = this.querySelector<HTMLIonInputElement>('ion-input[name="mount_special"]');
        if (!inputSpecial) return;

        inputSpecial.addEventListener('ionInput', event => {
            // @ts-ignore
            const {name, value} = event.target;

            // @ts-ignore
            this.data[name] = Number(value);
        })

        const buttonReset = this.querySelector<HTMLIonButtonElement>('ion-button[type="reset"]');
        if (!buttonReset) return;

        // limpia el formulario
        buttonReset.addEventListener('click', () => this.handleReset(selects, inputMount));

        // evento check
        const taxCheck = this.querySelector<HTMLIonCheckboxElement>('ion-checkbox#special-tax');
        if (!taxCheck) return;

        taxCheck.addEventListener('ionChange', this.handleCheck.bind(this));
    }

    handleReset(selects: HTMLIonSelectElement[], inputMount: HTMLIonInputElement) {
        this.data = {
            mount: 0,
            send_from: '',
            send_to: '',
            mount_special: 0,
            special_tax: false,
        };
        
        selects.forEach(select => select.value = '');
        inputMount.value = '';

        if (this.result) {
            this.result.innerHTML = '';
        }

        // limpia el check y oculta el campo
        const check = this.querySelector('ion-checkbox');
        if (!check) return;

        check.checked = this.data.special_tax;

        const specialField = this.querySelector<HTMLIonItemElement>('#special-field');
        if (!specialField) return;

        specialField.style.display = 'none';
    }

    handleCheck(event: IonCheckboxCustomEvent<CheckboxChangeEventDetail<any>>) {
        const specialField = this.querySelector<HTMLIonItemElement>('#special-field');
        if (!specialField) return;

        this.data.special_tax = event.target.checked;

        // ocultamos el elemento segun la opcion seleccionada
        if (this.data.special_tax) {
            specialField.style.display = '';

        } else {
            this.data.mount_special = 0;
            
            // ocultamos el campo
            specialField.style.display = 'none';
            
            // limpiamos el input
            const input = specialField.querySelector('ion-input');
            if (!input) return;

            input.value = '';
        }
    }
    
    async handleSubmit(event: SubmitEvent) {
        event.preventDefault();
        
        let total = 0;
        let tax = 0;
        let prefix = '';

        // si ambos campos de envio esten vacios
        if (this.data.send_to === '' || this.data.send_from === '') {
            await this.showAlert('Completa los datos para simular el envio');
            return;
        }

        // validaciones
        if (this.data.send_from === this.data.send_to) {
            await this.showAlert('No se puede enviar remesas dentro del mismo pais');
            return;
        }

        // procedemos a determinar las combinaciones
        // chile hacia venezuela
        if (this.data.send_from === 'CLP' && this.data.send_to === 'VES') {
            if (this.info?.taxes.tax_venezuela_chile) {
                tax = Number(this.info.taxes.tax_venezuela_chile);
                prefix = 'Bs.';

            } else {
                await this.showAlert('La tasa de Chile Hacia Venezuela no esta establecida, por favor actualiza la tasa');
                return;

            }
        
        // peru hacia venezuela
        } else if (this.data.send_from === 'PEN' && this.data.send_to === 'VES') {
            if (this.info?.taxes.tax_venezuela_peru) {
                tax = Number(this.info.taxes.tax_venezuela_peru);
                prefix = 'Bs';

            } else {
                await this.showAlert('La tasa de Peru Hacia Venezuela no esta establecida, por favor actualiza la tasa');
                return;

            }
        
        // chile hacia peru
        } else if (this.data.send_from === 'CLP' && this.data.send_to === 'PEN') {
            if (this.info?.taxes.tax_chile_peru) {
                tax = Number(this.info.taxes.tax_chile_peru);
                prefix = '/s';

            } else {
                await this.showAlert('La tasa de Chile Hacia Peru no esta establecida, por favor actualiza la tasa');
                return;

            }

        } else {  // peru hacia chile
            
            if (this.info?.taxes.tax_peru_chile) {
                tax = Number(this.info.taxes.tax_peru_chile);
                prefix = '$';

            } else {
                await this.showAlert('La tasa de Peru Hacia Chile no esta establecida, por favor actualiza la tasa');
                return;

            }
        } 

        // validamos tasa especial
        if (this.data.special_tax) {
            if (!this.data.mount_special) {
                await this.showAlert('debe proporcionar un monto para calcular tasa especial');
                return;
            }
        }

        // calculo
        total = this.data.special_tax ? (Number(this.data.mount) * Number(this.data.mount_special)) :
            (Number(this.data.mount) * tax);

        // realizamos la operacion
        this.renderResult(total, prefix);
    }

    renderResult(total: number, prefix: string) {
        const result = this.querySelector<HTMLDivElement>('#result');
        if (!result) return;

        result.innerHTML = (`
            <ion-card class="ion-padding" color="success">
                <ion-card-content>
                    <h1>Total a recibir: <span class="ion-margin-start"><b>${total.toFixed(2)}</b></span> ${prefix}</h1>
                </ion-card-content>
            </ion-card>
        `)
    }

    async showAlert(message: string) {
        const options: AlertOptions = {
            backdropDismiss: false,
            header: 'Atencion',
            message,
            buttons: [
                {
                    text: 'Entendido',
                    role: 'confirm'
                }
            ]
        };

        const alertController: HTMLIonAlertElement & AlertOptions = Object.assign(
            document.createElement('ion-alert'), 
            options
        );

        this.appendChild(alertController);

        await alertController.present();
        await alertController.onDidDismiss();

        alertController.remove();
    }
}

window.customElements.define('page-calculator', CalculatorPage);