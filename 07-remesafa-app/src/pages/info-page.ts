import { AlertInput, AlertOptions, OverlayEventDetail, ToastOptions } from "@ionic/core";
import { Tax } from "../interfaces/tax-interface";
import { StorageService } from "../services/storage-service";

class InfoPage extends HTMLElement {
    taxes: Tax
    storageService: StorageService;

    constructor() {
        super();
        this.taxes = {
            tax_chile_peru: '',
            tax_peru_chile: '',
            tax_venezuela_chile: '',
            tax_venezuela_peru: ''
        };
        this.storageService = StorageService.getInstance();
        this.storageService.storage?.get('info')
            .then(data => {
                if (data) {
                    this.taxes = data['taxes'];
                    this.connectedCallback();
                }
            });
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <remesafa-header title="Tasas del dia" color="primary"></remesafa-header>
            <ion-content color="light" class="ion-padding-top">
                <div class="logo-container ion-padding-bottom">
                    <img alt="logo" src="assets/images/fa_transferCash.png" class="logo" />
                </div>
                <ion-grid>
                    <ion-row>
                        <ion-col size="12">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="calendar-outline"></ion-icon>
                                <span id="date" class="ion-margin-start ion-text-capitalize"></span>
                            </ion-text>
                        </ion-col>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa hacia Venezuela
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/chile.png" alt="chile" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 pesos (CLP)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_venezuela_chile" color="dark">
                                                        ${this.taxes.tax_venezuela_chile!.length > 0 ? this.taxes.tax_venezuela_chile : '-'}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Bolivares (Bs)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/venezuela.png" />
                                    </ion-avatar>
                                </ion-item>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 sol (PEN)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_venezuela_peru" color="dark">
                                                        ${this.taxes.tax_venezuela_peru!.length > 0 ? this.taxes.tax_venezuela_peru : '-'}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Bolivares (Bs)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/venezuela.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button color="primary" id="tax-to-ven">Actualizar tasas</ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>

                <ion-grid>
                    <ion-row>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa Peru hacia Chile
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 sol (PEN)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_peru_chile" color="dark">
                                                        ${this.taxes.tax_peru_chile!.length > 0 ? this.taxes.tax_peru_chile : '-'}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Pesos (CLP)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/chile.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>                
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button id="tax-to-chile" color="primary">Actualizar tasa</ion-button>
                        </ion-col>
                    </ion-row>

                    <!-- chile a peru -->
                    <ion-row>
                        <ion-col size="12" class="ion-margin-top">
                            <ion-text class="ion-text-center title-info">
                                <ion-icon color="dark" name="cash-outline"></ion-icon>
                                <span class="ion-margin-start ion-text-capitalize">
                                    Tasa Chile hacia Peru
                                </span>
                            </ion-text>
                        </ion-col>
                    </ion-row>
                    <ion-row>
                        <ion-col size="12">
                            <ion-list>
                                <ion-item lines="full">
                                    <ion-avatar slot="start">
                                        <img src="assets/images/chile.png" />
                                    </ion-avatar>
                                    <ion-label>
                                        <ion-grid>
                                            <ion-row class="info-tasa">
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">1 Peso (CLP)</ion-text>                                                        
                                                </ion-col>
                                                <ion-col size="6">
                                                    <ion-text class="ion-text-center" id="tax_chile_peru" color="dark">
                                                        ${this.taxes.tax_chile_peru!.length > 0 ? this.taxes.tax_chile_peru : '-'}
                                                    </ion-text>
                                                </ion-col>
                                                <ion-col size="3">
                                                    <ion-text class="ion-text-center" color="medium">Soles (PEN)</ion-text> 
                                                </ion-col>
                                            </ion-row>    
                                        </ion-grid>
                                    </ion-label>
                                    <ion-avatar slot="end">
                                        <img src="assets/images/peru.png" />
                                    </ion-avatar>
                                </ion-item>
                            </ion-list>                
                        </ion-col>
                        <ion-col size="12" class="ion-text-center">
                            <ion-button color="primary" id="tax-to-peru">Actualizar tasa</ion-button>
                        </ion-col>
                    </ion-row>
                </ion-grid>    
            </ion-content>
            <ion-footer class="ion-padding-bottom ion-padding-top ion-no-border ion-text-center">    
                <ion-router-link href="/calculator">
                    <ion-button color="secondary">
                        <ion-icon slot="start" name="calculator-outline"></ion-icon>
                        <ion-label>Iniciar simulacion de envio<ion-label>
                    </ion-button> 
                </ion-router-link>
            </ion-footer>
        `);

        this.onMount();
    }

    onMount() {
        this.setDate();

        const buttonTaxVen = this.querySelector<HTMLIonButtonElement>('ion-button#tax-to-ven')
        if (!buttonTaxVen) return;

        const buttonTaxChi = this.querySelector<HTMLIonButtonElement>('ion-button#tax-to-chile');
        if (!buttonTaxChi) return;

        const buttonTaxPer = this.querySelector<HTMLIonButtonElement>('ion-button#tax-to-peru');
        if (!buttonTaxPer) return;

        buttonTaxChi.addEventListener('click', () => this.openPrompt('tax-chile'));
        buttonTaxPer.addEventListener('click', () => this.openPrompt('tax-peru'));
        buttonTaxVen.addEventListener('click', () => this.openPrompt('tax-ven'));
    }
    
    setDate() {
        const span: HTMLSpanElement | null = this.querySelector('span#date');
        if (!span) return;
    
        const date = new Date();
    
        const weekDay = date.toLocaleDateString('es-VE', {weekday: 'long'});
        const day = date.getDate() > 9 ? date.getDate().toString() : '0' + date.getDate().toString();
        const month = date.toLocaleDateString('es-VE', {month: 'long'});
        const year = date.getFullYear();
        
        span.innerText = (`${weekDay}, ${day} / ${month} / ${year}`);
    }

    async openPrompt(tax: 'tax-ven' | 'tax-peru' | 'tax-chile') {
        let inputs: AlertInput[] = [];

        switch (tax) {
            case 'tax-chile':
                inputs = [
                    {
                        placeholder: 'Tasa Peru hacia Chile',
                        type: 'number',
                        min: 1,
                        max: 999999,
                        name: 'tax_peru_chile'
                    }
                ];
            break;

            case 'tax-peru':
                inputs = [
                    {
                        placeholder: 'Tasa Chile hacia Peru',
                        type: 'number',
                        min: 1,
                        max: 999999,
                        name: 'tax_chile_peru'
                    }
                ];
            break;

            default:
                inputs = [
                    {
                        placeholder: 'Tasa Chile hacia Venezuela',
                        type: 'number',
                        min: 1,
                        max: 999999,
                        name: 'tax_venezuela_chile'
                    },
                    {
                        placeholder: 'Tasa Perú hacia Venezuela',
                        type: 'number',
                        min: 1,
                        max: 999999,
                        name: 'tax_venezuela_peru'
                    }
                ];
            break;
        }
        
        // creamos la instancia del prompt
        const alertController: HTMLIonAlertElement & AlertOptions = Object.assign(
            document.createElement('ion-alert'), 
            {
                backdropDismiss: false,
                header: 'Actualizar tasa(s)',
                buttons: [
                    {
                        text: 'Cancelar',
                        role: 'cancel',
                        cssClass: 'danger'
                    },
                    {
                        text: 'Confirmar',
                        role: 'confirm',
                        cssClass: 'success'
                    }
                ],
                inputs
            });

        // insertamos la alerta en el DOM
        this.appendChild(alertController);

        await alertController.present();

        const response = await alertController.onDidDismiss();

        // console.log(response);

        // comprobamos la informacion
        if (response && response.role === 'confirm') {
            const {values} = response.data;
            await this.handleResponsePrompt(values);
        }

        // removemos la alerta del DOM
        alertController.remove();
    }

    /** promesa que maneja la recepcion de los datos del prompt y alamcena en el store */
    async handleResponsePrompt(values: Tax) {
        
        // preparamos el objeto al store
        Object.keys(values).forEach((key) => {
            if (key === 'tax_chile_peru') {
                this.taxes.tax_chile_peru = values[key];
            }

            if (key === 'tax_peru_chile') {
                this.taxes.tax_peru_chile = values[key];
            }

            if (key === 'tax_venezuela_chile') {
                this.taxes.tax_venezuela_chile = values[key];
            }

            if (key === 'tax_venezuela_peru') {
                this.taxes.tax_venezuela_peru = values[key];
            }
        });

        // mandar al store en este punto.
        await this.storageService.storage?.set('info', {taxes: this.taxes}); 

        // volvemos a redibujar la interfaz
        this.renderTax();        

        // mandamos un toast para confirmar el envio
        const toastController: HTMLIonToastElement & ToastOptions = Object.assign(
            document.createElement('ion-toast'), 
            {
                duration: 1500,
                position: 'top',
                message: 'Tasa(s) actualizadas con exito',
            });

        this.appendChild(toastController);

        await toastController.present();
        await toastController.onDidDismiss();

        toastController.remove();
    }

    renderTax() {
        Object.keys(this.taxes).forEach(key => {
            const element = this.querySelector('#'+ key);
            if (!element) return;
     
            if (
                key === 'tax_chile_peru' || 
                key === 'tax_peru_chile' || 
                key === 'tax_venezuela_chile' || 
                key === 'tax_venezuela_peru'
            ) {
                element.innerHTML = this.taxes[key] ?? '-';
            }
        });
    }
}

window.customElements.define('page-info', InfoPage);