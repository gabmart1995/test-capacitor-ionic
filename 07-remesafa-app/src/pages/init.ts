class InitPage extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-app>
                <remesafa-menu></remesafa-menu>
                <ion-router>
                    <ion-route url="/calculator" component="page-calculator"></ion-route>
                    <ion-route url="/" component="page-info"></ion-route>
                </ion-router>
                <ion-router-outlet id="main-content"></ion-router-outlet>
            </ion-app>
        `);
    }
}

window.customElements.define('remesafa-app', InitPage);