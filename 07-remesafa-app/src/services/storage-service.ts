import {Storage} from "@ionic/storage";

export class StorageService {
    static instance: StorageService
    storage?: Storage;

    static getInstance() {
        if (!StorageService.instance) StorageService.instance = new StorageService();

        return StorageService.instance;
    }

    constructor() {
        this.init();
    }

    async init() {
        this.storage = await (new Storage()).create();
    }
}