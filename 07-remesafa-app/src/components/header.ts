class Header extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-title></ion-title>
                    <ion-buttons slot="end">
                        <ion-button id="menu-button">
                            <ion-icon name="menu-outline" slot="icon-only"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
        `)

        this.render();
    }

    render() {
        const button = this.querySelector('ion-button#menu-button');
        if (!button) return;

        const menu: any = document.querySelector('ion-menu');
        if (!menu) return;

        button.addEventListener('click', () => {
            menu.open();
        });
    }   


    static get observedAttributes() {
        return ['title', 'color'];
    }

    attributeChangedCallback(name = '', oldValue = '', newValue = '') {
        if (name === 'title' && oldValue !== newValue) {
            
            const title: any = this.querySelector('ion-title');
            if (!title) return;
            
            title.innerText = newValue;
        }


        if (name === 'color' && oldValue !== newValue) {
            const icon: any = this.querySelector('ion-icon');            
            if (!icon) return;

            icon.color = newValue;
        }
    }
}

window.customElements.define('remesafa-header', Header);