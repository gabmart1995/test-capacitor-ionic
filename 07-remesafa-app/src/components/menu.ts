class Menu extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-menu content-id="main-content" menu-id="main-menu" side="start">
                <ion-header class="ion-no-border">
                    <ion-buttons slot="start">
                        <ion-button color="secondary">
                            <ion-icon slot="icon-only" name="close-outline"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-header>
                <ion-content class="ion-padding">
                    <div class="logo-container ion-padding-bottom">
                        <img alt="logo" src="assets/images/fa_transferCash.png" class="logo" />
                    </div>
                    <ion-list>
                        <ion-menu-toggle auto-hide="false"> <!-- este elemento cierra el menu -->
                            <ion-item href="/" detail button lines="full">
                                <ion-icon name="information-circle-outline" slot="start" color="primary"></ion-icon>
                                <ion-label class="ion-text-capitalize">tasas del dia</ion-label>
                            </ion-item>
                        </ion-menu-toggle>
                        <ion-menu-toggle auto-hide="false"> <!-- este elemento cierra el menu -->
                            <ion-item href="/calculator" detail button lines="full">
                                <ion-icon slot="start" color="primary" name="calculator-outline"></ion-icon>
                                <ion-label class="ion-text-capitalize">simulador</ion-label>
                            </ion-item>
                        </ion-menu-toggle>
                    </ion-list>
                </ion-content>
            </ion-menu>
        `);

        this.onMount();
    }

    onMount() {
        const menu = this.querySelector<HTMLIonMenuElement>('ion-menu');
        if (!menu) return;

        const buttonClose = this.querySelector<HTMLIonButtonElement>('ion-button');
        if (!buttonClose) return;

        buttonClose.addEventListener('click', () => menu.close());
    }
}

window.customElements.define('remesafa-menu', Menu);