import './components';
import './pages';
import { StorageService } from './services/storage-service';

import './style.css';

StorageService.getInstance() // inicializa el store

new EventSource('/esbuild').addEventListener('change', () => location.reload());
