# VANILLA JS + CAPACITOR + IONIC

Este proyecto es una introduccion al desarrollo nativo con capacitor. 
Usando los WebComponents de Ionic Framework, Zod y
Swiper para generar las interfaces y la logica de validacion. <br /><br />
Es un repositorio de practica y consulta sobre el uso y funcionamiento de las
aplicaciones hibridas.

## Instalacion de Webpack dev server y Capacitor
`yarn install`

### servidor de desarrollo
puedes correrlo desde cualquier servidor de desarrollo.

## Compilar para android
Para generar el proyecto en android usa el comando `yarn generate_android` creara los 
directorios escritos en JAVA para la compilacion (usar este comando si no tienes el directorio de 
andrioid).<br /><br />

Una vez completado debes correr la sincronizacion del proyecto con `yarn sync` y por ultimo
corres el comando de ejecucion `yarn run_android` para correrlo en emulador de Android Studio para mas info consultar [aqui](https://capacitorjs.com/docs/android)