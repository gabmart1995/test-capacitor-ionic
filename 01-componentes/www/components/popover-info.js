class PopoverInfo extends HTMLElement {
    constructor() {
        super();
        this.items = new Array(40).fill(null);
        this.popoverElement = null;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-content class="ion-padding">
                <ion-list>
                    ${this.items.map((_, index) => (`
                        <ion-item>
                            <ion-label>Item ${index + 1}</ion-label>
                        </ion-item>
                    `)).join('')}
                </ion-list>
            </ion-content>
        `)

        this.onMount();
    }

    onMount() {
        this.popoverElement = this.parentNode;

        const itemsElement = Array.from(this.querySelectorAll('ion-item'));
        if (itemsElement.length === 0) return;

        itemsElement.forEach((itemElement, index) => {
            itemElement.addEventListener('click', () => {
                if (this.popoverElement) {
                    this.popoverElement.dismiss({
                        item: index
                    });
                }
            });
        });
    }
}

window.customElements.define('app-popover-info', PopoverInfo);