class Header extends HTMLElement {
    constructor() {
        super();
        this.innerHTML = (`
            <ion-header class="ion-no-border" translucent>
                <ion-toolbar>
                    <ion-buttons slot="start"> 
                        <ion-button href="/">
                            <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                        </ion-button>    
                    </ion-buttons>
                    <ion-title class="ion-text-capitalize"></ion-title>
                </ion-toolbar>
            </ion-header>
        `);
    }

    static get observedAttributes() {
        return ['title', 'color'];
    }

    attributeChangedCallback(name = '', oldValue = '', newValue = '') {
        if (name === 'title' && oldValue !== newValue) {
            const title = this.querySelector('ion-title');
            if (!title) return;

            title.innerText = newValue;
        }

        if (name === 'color' && oldValue !== newValue) {
            const icon = this.querySelector('ion-icon');
            if (!icon) return;

            icon.color = newValue;
        }
    }
}

window.customElements.define('app-header', Header);