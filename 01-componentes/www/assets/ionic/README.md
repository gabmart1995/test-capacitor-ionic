# Libreria Ionic para la Web

Es una copia del paquete NPM de @ionic/core version 7 para trabajar en local
usando para trabajar sin conexión, se retira el contenido se deja unicamente
los archivos y la carpeta "ionic" que contiene la logica de componentes y el 
bundle css del proyecto.

### Como importarlos??

importalos a tu web de la siguiente manera: <br> <br>

`
<script type="module" src="/assets/ionic-web/core/ionic.esm.js"></script>
<script nomodule src="/assets/ionic-web/core/ionic.js"></script>
<link rel="stylesheet" href="/assets/ionic-web/ionic.bundle.css" />
`

montalo en el index.html de tu proyecto o bundle de produccion en caso de usar
un servicio como webpack.