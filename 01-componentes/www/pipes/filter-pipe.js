/**
 * Filtra la busqueda del arreglo
 * @param {Array<any>} array Array de elementos
 * @param {string} text texto a buscar
 * @param {string} [column] columna a buscar
 * @returns {Array<any>}
 */
const filterPipeTransform = (array, text, column = 'title') => {
    if (text.length === 0) return array;
    
    text = text.toLowerCase();
    
    return array.filter(item => item[column].toLowerCase().includes(text));
};

export {
    filterPipeTransform
}