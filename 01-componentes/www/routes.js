/**  @type {Array<{url: string, icon: string, name: string, component: string}>} */
const ROUTES = [
    {
        url: '/',
        icon: '',
        name: '',
        component: 'page-home',
    },
    {   
        url: '/action-sheet',
        icon: 'american-football-outline',
        name: 'Action Sheet',
        component: 'page-action-sheet',
    },
    {   
        url: '/alert',
        icon: 'logo-apple-appstore',
        name: 'Alert',
        component: 'page-alert',
    },
    {
        url: '/avatar',
        icon: 'beaker-outline',
        name: 'Avatar',
        component: 'page-avatar',
    },
    {
        url: '/buttons',
        icon: 'radio-button-off-outline',
        name: 'Buttons',
        component: 'page-buttons',
    },
    {
        url: '/card',
        icon: 'card-outline',
        name: 'Cards',
        component: 'page-card'
    },
    {
        url: '/checks',
        icon: 'checkmark-circle-outline',
        name: 'Checkbox',
        component: 'page-check'
    },
    {
        url: '/datetime',
        icon: 'calendar-outline',
        name: 'Datetime',
        component: 'page-datetime'
    },
    {
        url: '/fabs',
        icon: 'car-outline',
        name: 'Fabs',
        component: 'page-fabs'
    },
    {
        url: '/grid',
        icon: 'grid-outline',
        name: 'Grid',
        component: 'page-grid'
    },
    {
        url: '/infinite-scroll',
        icon: 'infinite-outline',
        name: 'Infinite Scroll',
        component: 'page-infinite-scroll'
    },
    {
        url: '/input',
        icon: 'hammer-outline',
        name: 'Input Forms',
        component: 'page-input'
    },
    {
        url: '/list',
        icon: 'list-outline',
        name: 'List - Sliding',
        component: 'page-list'
    },
    {
        url: '/list-reorder',
        icon: 'reorder-three-outline',
        name: 'List - Reorder',
        component: 'page-list-reorder'
    },
    {
        url: '/loading',
        icon: 'refresh-circle-outline',
        name: 'Loading',
        component: 'page-loading'
    },
    {
        url: '/modal',
        icon: 'phone-portrait-outline',
        name: 'Modal',
        component: 'page-modal'
    },
    {
        url: '/popover',
        icon: 'tablet-portrait-outline',
        name: 'Popover',
        component: 'page-popover'
    },
    {
        url: '/progress',
        icon: 'code-working-outline',
        name: 'ProgressBar',
        component: 'page-progress'
    },
    {
        url: '/refresher',
        icon: 'arrow-down-circle-outline',
        name: 'Refresh',
        component: 'page-refresher'
    },
    {
        url: '/search',
        icon: 'search-outline',
        name: 'Search',
        component: 'page-searchbar'
    },
    {
        url: '/segment',
        icon: 'copy-outline',
        name: 'Segment',
        component: 'page-segment'
    },
    {
        url: '/slides',
        icon: 'albums-outline',
        name: 'Slides',
        component: 'page-slides'
    },
    {
        url: '/tabs',
        icon: 'cog-outline',
        name: 'Tabs',
        component: 'page-tabs'
    },
    {
        url: '/toast',
        icon: 'pricetag-outline',
        name: 'Toast',
        component: 'page-toast'
    }
];

const ItemsList = ROUTES.filter((_, index) => index > 0);

export {
    ROUTES,
    ItemsList
};