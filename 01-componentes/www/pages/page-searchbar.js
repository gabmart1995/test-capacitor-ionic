import DataService from '../services/data-service.js';

import { filterPipeTransform } from '../pipes/filter-pipe.js';

class PageSearchBar extends HTMLElement {
    constructor() {
        super();
        this.albums = [];
        this.listElement = null;
        this.dataService = new DataService();
    }

    set _albums(albums = []) {
        this.albums = albums;
        this.listElement.innerHTML = this._albums.map(album => (`
            <ion-item>
                <ion-label>${album.title}</ion-label>
            </ion-item>
        `)).join('');
    }

    get _albums() {
        return this.albums;
    }

    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-router-link routerDirection="root" href="/">
                            <ion-button>
                                <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                            </ion-button>
                        </ion-router-link>
                    </ion-buttons>
                    <ion-title>Search Bar</ion-title>
                </ion-toolbar>
                <ion-searchbar debounce="500" placeholder="buscar album" animated inputmode="text"></ion-searchbar>
            </ion-header>
            <ion-content class="ion-padding">
                <ion-list></ion-list>
            </ion-content>
        `)

        this.onMount();
    }

    onMount() {
        this.listElement = this.querySelector('ion-list');
        if (!this.listElement) return;

        const searchBarElement = this.querySelector('ion-searchbar');
        if (!searchBarElement) return;

        searchBarElement.addEventListener('ionInput', this.handleSearch.bind(this));

        this.dataService.getAlbumes()
            .then(albumes => this._albums = albumes)
            .catch(console.error);
        }
    
    handleSearch(event) {
        const {value} = event.target;
        
        this.listElement.innerHTML = filterPipeTransform(this._albums, value, 'title')
            .map(album => (`
                <ion-item>
                    <ion-label>${album.title}</ion-label>
                </ion-item>
            `)).join('');            
    }
}

window.customElements.define('page-searchbar', PageSearchBar);