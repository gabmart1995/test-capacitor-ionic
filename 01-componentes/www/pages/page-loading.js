class PageLoading extends HTMLElement {
    constructor() {
        super();
        this.ionLoadingElement = null;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="Loading" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-button id="open-loading" expand="block">Mostrar Loading</ion-button>
                <ion-loading></ion-loading>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.ionLoadingElement = this.querySelector('ion-loading');
        if (!this.ionLoadingElement) return;

        const button = this.querySelector('#open-loading');
        if (!button) return;

        button.addEventListener('click', async () => {
            this.ionLoadingElement.message = "Hola Loading ...";
            this.ionLoadingElement.duration = 2000;
            
            await this.ionLoadingElement.present();
        })
    }
}

window.customElements.define('page-loading', PageLoading)