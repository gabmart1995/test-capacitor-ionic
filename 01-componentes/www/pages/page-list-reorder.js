class PageListReorder extends HTMLElement {
    constructor() {
        super();
        this.personajes = ['Aquaman', 'Superman', 'Batman', 'Womder Woman', 'Flash'];
        this.isToogleDisabled = true;
        this.ionReorderGroupElement = null;
    }

    set _isToogleDisabled(isToogleDisabled = false) {
        this.isToogleDisabled = isToogleDisabled;
        this.ionReorderGroupElement.disabled = this.isToogleDisabled;
    }

    get _isToogleDisabled() {
        return this.isToogleDisabled;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-router-link routerDirection="root" href="/">
                            <ion-button>
                                <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                            </ion-button>
                        </ion-router-link>
                    </ion-buttons>
                    <ion-title>List Reorder</ion-title>
                    <ion-buttons slot="end">
                        <ion-button id="toggle-button" color="primary">TOGGLE</ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
            <ion-content class="ion-padding">
                <ion-list>
                    <ion-reorder-group disabled="${this._isToogleDisabled}">
                        ${this.personajes.map(personaje => (`
                            <ion-item>
                                <ion-label>${personaje}</ion-label>
                                <ion-reorder slot="end">
                                    <ion-icon name="pizza"></ion-icon>
                                </ion-reorder>
                            </ion-item>
                        `)).join('')}
                    </ion-reorder-group>
                </ion-list>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.ionReorderGroupElement = this.querySelector('ion-reorder-group');
        if (!this.ionReorderGroupElement) return;

        const toggleButton = this.querySelector('#toggle-button');
        if (!toggleButton) return; 

        this.ionReorderGroupElement.addEventListener('ionItemReorder', this.doReorder.bind(this));
        toggleButton.addEventListener('click', () => this._isToogleDisabled = !this._isToogleDisabled);
    }
    
    doReorder({detail}) {
        detail.complete();

        const itemMove = this.personajes.splice(detail.from, 1)[0];
        this.personajes.splice(detail.to, 0, itemMove);

        console.log(this.personajes);
    }
}

window.customElements.define('page-list-reorder', PageListReorder)