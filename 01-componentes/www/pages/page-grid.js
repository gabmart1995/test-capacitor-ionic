class PageGrid extends HTMLElement {
    constructor() {
        super();
        this.data = Array(11).fill(null);
    }

    connectedCallback() {
        this.innerHTML = (`
            <app-header title="Grid" color="primary"></app-header>
            <ion-content fullscreen class="ion-padding">
                <ion-grid fixed>
                    <ion-row>
                        ${this.data.map(() => (`
                            <ion-col size="12" size-lg="3" size-md="4" size-sm="6">
                                <ion-card>
                                    <ion-img src="/assets/stan-lee.jpg"></ion-img>
                                    <ion-card-header>
                                        <ion-card-subtitle>Card Subtitle</ion-card-subtitle>
                                        <ion-card-title>Card Title</ion-card-title>
                                    </ion-card-header>
                                    <ion-card-content>
                                        Lorem ipsum dolor sit amet consectetur adipisicing elit. Molestiae, praesentium consequatur ea necessitatibus ullam at officia natus, esse delectus pariatur enim dignissimos nulla qui voluptas doloribus repudiandae. Distinctio, dignissimos necessitatibus?
                                    </ion-card-content>
                                </ion-card>
                            </ion-col>
                        `)).join('')}
                    </ion-row>
                </ion-grid>    
            </ion-content>
        `);
    }
}

window.customElements.define('page-grid', PageGrid);