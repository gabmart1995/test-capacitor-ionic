import DataService from '../services/data-service.js'; 

class PageList extends HTMLElement {
    constructor() {
        super();
        this.dataService = new DataService();
        this.users = [];
        this.listElement = null;
    }

    set _users(users = []) {
        this.users = users;

        this.listElement = this.querySelector('ion-list');
        if (!this.listElement) return;

        this.listElement.innerHTML = this._users.map(user => (`
            <ion-item-sliding>
                <ion-item>
                    <ion-label>
                        <h3>${user.name}</h3>
                        <p>${user.email}</p>
                    </ion-label>
                    <ion-label slot="end" class="ion-text-end text-small">
                        ${user.phone}
                    </ion-label>    
                </ion-item>
                <!--<ion-item-options side="start">
                    <ion-item-option class="favorite-button">
                        <ion-icon slot="icon-only" name="heart-outline"></ion-icon>
                    </ion-item-option>
                    <ion-item-option color="success" class="share-button">
                        <ion-icon slot="icon-only" name="share-outline"></ion-icon>
                    </ion-item-option>
                </ion-item-options>
                <ion-item-options side="end">
                    <ion-item-option color="danger" class="delete-button">
                        <ion-icon slot="icon-only" name="trash-outline"></ion-icon>
                    </ion-item-option>
                </ion-item-options>-->
            </ion-item-sliding>
        `)).join('');

        // añadimos los eventos
        const shareButtons = Array.from(this.querySelectorAll('.share-button'));
        const favoriteButtons = Array.from(this.querySelectorAll('.favorite-button'));
        const deleteButtons = Array.from(this.querySelectorAll('.delete-button'));

        if (shareButtons.length === 0 || favoriteButtons.length === 0 || deleteButtons.length === 0) return;

        shareButtons.forEach((shareButton, index) => {
            shareButton.addEventListener('click', this.share.bind(this, this._users[index]));
        });

        favoriteButtons.forEach((favoriteButton, index) => {
            favoriteButton.addEventListener('click', this.favorite.bind(this, this._users[index]));
        });
        
        deleteButtons.forEach((deleteButton, index) => {
            deleteButton.addEventListener('click', this.delete.bind(this, this._users[index]));
        });
    }

    get _users() {
        return this.users;
    }

    connectedCallback() {
        this.innerHTML = (`
            <app-header title="list" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-list></ion-list>
            </ion-content>
        `);

        this.render();
    }
    
    render() {
        this.dataService.getUsers()
            .then(users => this._users = users)
            .catch(console.error);
    }

    favorite(user) {
        console.log('favorite', user);
        this.listElement.closeSlidingItems();
    }

    share(user) {
        console.log('share', user);
        this.listElement.closeSlidingItems();
    }

    delete(user) {
        console.log('delete', user);
        this.listElement.closeSlidingItems();
    }
}

window.customElements.define('page-list', PageList);