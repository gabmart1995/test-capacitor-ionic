class PageInfiniteScroll extends HTMLElement {
    constructor() {
      super();
      this.listElement = null;
      this.infiniteScroll = null;
    }
  
    connectedCallback() {
        this.innerHTML = (`
          <app-header color="primary" title="infinite scroll"></app-header>
          <ion-content class="ion-padding">
            <ion-list></ion-list>
            <ion-infinite-scroll threshold="100px">
              <ion-infinite-scroll-content 
                loading-spinner="crescent"
                loading-text="loading more data ..."  
              ></ion-infinite-scroll-content>
            </ion-infinite-scroll>
          </ion-content>  
        `);

        this.onMount();
    }

    onMount() {
      this.listElement = this.querySelector('ion-list');
      if (!this.listElement) return;

      this.infiniteScroll = this.querySelector('ion-infinite-scroll');
      if (!this.infiniteScroll) return;

      this.infiniteScroll.addEventListener('ionInfinite', () => {
        setTimeout(() => this.generateItems(), 1500);
      });
      
      this.generateItems();
    }
    
    generateItems() {
      const count = this.listElement.childElementCount + 1;
      const total = count + 20; // suma 20 elementos
      
      if (count > 50) {
        this.infiniteScroll.complete();
        this.infiniteScroll.disabled = true;

        return
      }
      
      // insertamos en el dom
      for (let i = count; i < total; i++) {
        const itemElement = document.createElement('ion-item');
        const labelElement = document.createElement('ion-label');
        
        labelElement.innerText = ('item ' + i.toString());
        
        itemElement.appendChild(labelElement);
        this.listElement.appendChild(itemElement);
      }
      
      this.infiniteScroll.complete(); // termina la animacion del infinite
    }
  }

window.customElements.define('page-infinite-scroll', PageInfiniteScroll);