class PagePopover extends HTMLElement {
    constructor() {
        super();
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-router-link routerDirection="root" href="/">
                            <ion-button>
                                <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                            </ion-button>
                        </ion-router-link>
                    </ion-buttons>
                    <ion-title>Popover</ion-title>
                    <ion-buttons slot="end">
                        <ion-button id="header-button">
                            <ion-icon name="person-outline" slot="icon-only" color="primary"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
            <ion-content class="ion-padding">
                <ion-button id="test-popover" expand="block">Mostrar Popover</ion-button>
            </ion-content>
            <ion-footer>
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-button id="footer-button">
                            <ion-icon name="person-outline" slot="icon-only" color="primary"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                    <ion-title>Footer</ion-title>
                </ion-toolbar>
            </ion-footer>
        `);

        this.onMount();
    }

    onMount() {
        const buttonPopover = this.querySelector('#test-popover');
        if (!buttonPopover) return;

        const footerButtonPopover = this.querySelector('#footer-button');
        if (!footerButtonPopover) return;

        const headerButtonPopover = this.querySelector('#header-button');
        if (!headerButtonPopover) return;

        footerButtonPopover.addEventListener('click', this.openPopover.bind(this));
        buttonPopover.addEventListener('click', this.openPopover.bind(this));
        headerButtonPopover.addEventListener('click', this.openPopover.bind(this));
    }

    async openPopover(event) {
        const popoverController = Object.assign(
            document.createElement('ion-popover'), 
            {
                component: 'app-popover-info', // componente hijo a renderizar
                backdropDismiss: false,
                event,
            }
        );
        
        this.appendChild(popoverController);
        
        // abre el modal
        await popoverController.present();
        
        // cuando se cierra el popover podemos captura la informacion desde
        // aqui 
        const {data} = await popoverController.onDidDismiss();
        console.log(data);

        // se remueve el elemento popover del dom una vez que cierra la animacion
        popoverController.remove();
    }
}

window.customElements.define('page-popover', PagePopover);