class PageCard extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="cards" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-card mode="ios">
                    <ion-card-header>
                        <ion-card-subtitle>Card Subtitle</ion-card-subtitle>
                        <ion-card-title>Card Title</ion-card-title>
                    </ion-card-header>
                    <ion-card-content>
                        Here's a small text description for the card content. Nothing more, nothing less.
                    </ion-card-content>
                </ion-card>
                <ion-card>
                    <ion-item color="primary">
                        <ion-icon slot="start" name="pin-outline"></ion-icon>
                        <ion-label>Marcador</ion-label>
                    </ion-item>
                    <ion-card-content>
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries
                    </ion-card-content>
                </ion-card>
                <ion-card>
                    <ion-img alt="solid snake" src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRxFFin05kZjOWHxDaf_rhF0MgVb1esNIcJLbLj4WE4yA&s"></ion-img>
                    <ion-card-content>
                        Here's a small text description for the card content. Nothing more, nothing less.
                    </ion-card-content>
                </ion-card>
            </ion-content>
        `);
    }
}

window.customElements.define('page-card', PageCard);