class PageButtons extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-router-link routerDirection="root" href="/">
                            <ion-button>
                                <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                            </ion-button>
                        </ion-router-link>
                    </ion-buttons>
                    <ion-title>Buttons</ion-title>
                    <!-- end toolbar -->
                    <ion-buttons slot="end">
                        <ion-button id="favorites" color="danger">
                            <ion-icon slot="icon-only" name="heart-outline"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
            <ion-content class="ion-padding">
                <h3>Default</h3>
                <ion-button>Default</ion-button>
                
                <h3>Anchor</h3>
                <ion-button href="#">Anchor</ion-button>
                
                <h3>Colors</h3>
                <ion-button color="primary">Primary</ion-button>
                <ion-button color="secondary">Secondary</ion-button>
                <ion-button color="tertiary">Tertiary</ion-button>
                <ion-button color="success">Success</ion-button>
                <ion-button color="warning">Warning</ion-button>
                <ion-button color="danger">Danger</ion-button>
                <ion-button color="light">Light</ion-button>
                <ion-button color="medium">Medium</ion-button>
                <ion-button color="dark">Dark</ion-button>

                <h3>Expand</h3>
                <ion-button expand="full">Full button</ion-button>
                <ion-button expand="block">Block button</ion-button>
                
                <h3>Round</h3>
                <ion-button shape="round">Round button</ion-button>

                <h3>Fill</h3>
                <ion-button fill="outline" expand="full">Full + Fill button</ion-button>
                <ion-button expand="block" fill="outline">Block + Fill button</ion-button>
                <ion-button shape="round" fill="outline">Round + Fill button</ion-button>

                <h3>Icons</h3>
                <ion-button>
                    <ion-icon slot="start" name="star"></ion-icon>
                    Left Icon
                </ion-button>
                <ion-button>
                    Right Icon
                    <ion-icon slot="end" name="star"></ion-icon>
                </ion-button>
                <ion-button>
                    <ion-icon slot="icon-only" name="star"></ion-icon>
                </ion-button>

                <h3>Sizes</h3>
                <ion-button size="large">Large</ion-button>
                <ion-button size="default">Default</ion-button>
                <ion-button size="small">Small</ion-button>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.isFavorite = false;

        const buttonFavorite = this.querySelector('#favorites');
        if (!buttonFavorite) return;

        buttonFavorite.addEventListener('click', event => {
            this.isFavorite = !this.isFavorite;

            const [icon] = event.target.children;
            icon.name = this.isFavorite ? 'heart' : 'heart-outline';
        });
    }
}

window.customElements.define('page-buttons', PageButtons)