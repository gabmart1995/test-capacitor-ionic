import { ROUTES, ItemsList } from "../routes.js";

class InitApp extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-app>
                <ion-split-pane when="md" content-id="main-content">
                    <ion-menu content-id="main-content" menu-id="main-menu" side="start">
                        <ion-header class="ion-no-border">
                            <ion-toolbar>
                                <ion-title>Componentes</ion-title>
                            </ion-toolbar>
                        </ion-header>
                        <ion-content class="ion-padding">
                            <ion-list>
                                ${ItemsList.map(item => (`
                                    <ion-menu-toggle auto-hide="false"> <!-- este elemento cierra el menu -->
                                        <ion-item href="${item.url}" detail button>
                                            <ion-icon color="primary" slot="start" name="${item.icon}"></ion-icon>
                                            <ion-label class="ion-text-capitalize">${item.name}</ion-label>
                                        </ion-item>
                                    </ion-menu-toggle>
                                `)).join('')}
                            </ion-list>
                        </ion-content>
                    </ion-menu>
                    <!-- fin del menu app -->
                    <ion-router>
                        ${ROUTES.map(route => (`
                            <ion-route url="${route.url}" component="${route.component}"></ion-route>
                        `)).join('')}
                    </ion-router>
                    <ion-router-outlet id="main-content"></ion-router-outlet>
                </ion-split-pane>
            </ion-app>
        `);
    }   
}

window.customElements.define('app-ionic', InitApp);