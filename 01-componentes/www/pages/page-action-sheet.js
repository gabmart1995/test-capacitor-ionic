class PageActionSheet extends HTMLElement {
    connectedCallback() {
      this.innerHTML = (`
        <app-header color="primary" title="action sheet"></app-header>
        <ion-content class="ion-padding">
          <ion-button expand="block" id="open-action-sheet">Mostrar action sheet</ion-button>
          <ion-action-sheet trigger="open-action-sheet" header="Albumes"></ion-action-sheet>
        </ion-content>
      `);

      this.onMount();
    }
    
    onMount() {
      const actionSheet = this.querySelector('ion-action-sheet');
      if (!actionSheet) return;

      actionSheet.backdropDismiss = false; // forza al usuario a tomar una opcion
      actionSheet.buttons = [
        {
          text: 'Delete',
          icon: 'trash-outline',
          role: 'destructive',
          cssClass: 'red-element',
          data: {
            action: 'delete',
          },
          handler: () => {
            console.log('Delete clicked');
          }
        },
        {
          text: 'Favorite',
          icon: 'heart-outline',
          data: {
            action: 'share',
          },
          handler: () => {
            console.log('share clicked');
          }
        },
        {
          text: 'Cancel',
          icon: 'close-outline',
          role: 'cancel',
          data: {
            action: 'cancel',
          },
          handler: () => {
            console.log('cancel clicked');
          }
        },
      ];

      // evento que captura el cierre
      actionSheet.addEventListener('ionActionSheetDidDismiss', event => {
        const {data} = event.detail;
        console.log(data);
      });
    }
  }


  window.customElements.define('page-action-sheet', PageActionSheet);