class PageFabs extends HTMLElement {
    constructor() {
        super();
        this.data = Array(100).fill(null);
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="fabs" color="primary"></app-header>
            <ion-content class="ion-padding">
                <!-- ion-fab -->
                <ion-fab vertical="bottom" horizontal="end" slot="fixed">
                    <ion-fab-button>
                        <ion-icon name="add-outline"></ion-icon>
                    </ion-fab-button>
                    <ion-fab-list side="top">
                        <ion-fab-button color="facebook"><ion-icon name="logo-facebook"></ion-icon></ion-fab-button>
                        <ion-fab-button color="twitter"><ion-icon name="logo-twitter"></ion-icon></ion-fab-button>
                        <ion-fab-button color="youtube"><ion-icon name="logo-youtube"></ion-icon></ion-fab-button>
                    </ion-fab-list>
                    <ion-fab-list side="start">
                        <ion-fab-button color="vimeo"><ion-icon name="logo-vimeo"></ion-icon></ion-fab-button>
                        <ion-fab-button color="google"><ion-icon name="logo-google"></ion-icon></ion-fab-button>
                        <ion-fab-button color="github"><ion-icon name="logo-github"></ion-icon></ion-fab-button>
                    </ion-fab-list>
                </ion-fab>    
                <!-- end ion-fab -->
                <ion-list>
                    ${this.data.map((_, index) => (`
                        <ion-item>
                            <ion-label>item ${index + 1}</ion-label>
                        </ion-item>
                    `)).join('')}
                </ion-list>
            </ion-content>
            <ion-footer>
                <ion-toolbar>
                    <ion-title>Footer</ion-title>
                </ion-toolbar>
            </ion-footer>
        `)
    }
}

window.customElements.define('page-fabs', PageFabs);