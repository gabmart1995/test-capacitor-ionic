class PageDateTime extends HTMLElement {
    constructor() {
        super();
        this.dateExample = (new Date()).toISOString();
        this.yearValues = [2020, 2016, 2008, 2004, 2000, 1996];
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header color="primary" title="Datetime"></app-header>
            <ion-content class="ion-padding">
                
                <h3>Datos de nacimiento</h3>
                <ion-datetime id="date-input" presentation="date" value="${this.dateExample}">
                    <span slot="title">Selecciona la fecha:</span>
                </ion-datetime>

                <h3>Restricciones</h3>
                <ion-datetime 
                    presentation="date" 
                    min="2015-01-01" 
                    max="2025-12-31"
                >
                    <span slot="title">Selecciona la fecha:</span>
                </ion-datetime>

                <h3>Años</h3>
                <ion-datetime  
                    presentation="year" 
                    min="2015" 
                    max="2025"
                >
                    <span slot="title">Selecciona la fecha:</span>
                </ion-datetime>

                <h3>Opciones personalizadas</h3>
                <ion-datetime id="custom-datetime" year-values="${this.yearValues}">
                    <ion-buttons slot="buttons">
                        <ion-button color="danger">Limpiar</ion-button>
                        <ion-button color="primary">Confirmar</ion-button>
                    </ion-buttons>
                </ion-datetime>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        const dateTimeDate = this.querySelector('#date-input');
        if (!dateTimeDate) return;

        dateTimeDate.addEventListener('ionChange', event => {
            console.log(new Date(event.detail.value));
        });

        const customDateTime = this.querySelector('#custom-datetime');
        if (!customDateTime) return;

        const buttons = customDateTime.querySelectorAll('ion-button');
        if (buttons.length === 0) return;

        const [buttonCancel, buttonConfirm] = buttons;

        buttonCancel.addEventListener('click', async () => await customDateTime.reset());
        buttonConfirm.addEventListener('click', async () => {
            await customDateTime.confirm();
            console.log(new Date(customDateTime.value));
        });
    }
}

window.customElements.define('page-datetime', PageDateTime)