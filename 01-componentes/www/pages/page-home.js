import { ItemsList } from "../routes.js";

class Home extends HTMLElement {
    constructor() {
        super();
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-menu-button menu="main-menu" color="primary"></ion-menu-button>
                    </ion-buttons>
                    <ion-title>Menú Principal</ion-title>
                </ion-toolbar>
            </ion-header>
            <ion-content class="ion-padding">    
                <ion-list>
                    ${ItemsList.map(item => (`
                        <ion-item href="${item.url}" detail button>
                            <ion-icon color="primary" slot="start" name="${item.icon}"></ion-icon>
                            <ion-label class="ion-text-capitalize">${item.name}</ion-label>
                        </ion-item>
                    `)).join('')}    
                </ion-list>
            </ion-content>
        `);
    }
}

window.customElements.define('page-home', Home);