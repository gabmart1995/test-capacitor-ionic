class PageAlert extends HTMLElement {
    connectedCallback() {
      this.innerHTML = (`
        <app-header title="alert" color="primary"></app-header>
        <ion-content class="ion-padding">
          <ion-button id="first-alert" expand="block">Mostrar alerta</ion-button>
          <ion-button id="second-alert" expand="block">Mostrar opciones alerta</ion-button>
          <ion-button id="third-alert" expand="block">Mostrar opciones alerta 2</ion-button>
          <ion-button id="prompt-alert" expand="block">Mostrar alert prompt</ion-button>
          <ion-alert
            trigger="first-alert"
            header="Alert"
            sub-header="Subtitle"
            message="This is an alert message"
          ></ion-alert>
          <ion-alert
            trigger="second-alert"
            header="Alert two"
            sub-header="Subtitle"
            message="This is an alert message"
          ></ion-alert>
          <ion-alert
            trigger="third-alert"
            header="Alert three"
            sub-header="Subtitle"
            message="This is an alert message"
          ></ion-alert>
          <ion-alert
            trigger="prompt-alert"
            header="Alert four"
            message="Completa el formulario"
          ></ion-alert>
        </ion-content>
      `);

      this.onMount();
    }

    onMount() {
      const alert = this.querySelector('ion-alert[header="Alert"]');
      if (!alert) return;

      alert.buttons = ['OK'];
      alert.backdropDismiss = false;

      const alertTwo = this.querySelector('ion-alert[header="Alert two"]');
      if (!alertTwo) return;

      alertTwo.buttons = ['Cancel', 'Open modal', 'Delete'];
      alertTwo.backdropDismiss = false;

      const alertThree = this.querySelector('ion-alert[header="Alert three"]');
      if (!alertThree) return;

      alertThree.buttons = [
        {
          text: 'OK',
          handler: () => {
            console.log('click en ok');
          }
        },
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'red-element'
        }
      ];
      alertThree.backdropDismiss = false;

      const alertfour = this.querySelector('ion-alert[header="Alert four"]');
      if (!alertfour) return;

      alertfour.backdropDismiss = false;
      alertfour.buttons = [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary'
        },
        {
          text: 'OK',
          handler: data => {
            // capturamos los datos del formulario
            console.log(data);
          }
        }
      ];

      // campos de formularios
      alertfour.inputs = [
        {
          name: 'name',
          placeholder: 'nombre',
          type: 'text',
          attributes: {
            maxLength: 20,
          }
        },
        {
          name: 'age',
          type: 'number',
          placeholder: 'edad',
          min: 18,
          max: 100
        },
        {
          name: 'message',
          placeholder: 'my message',
          type: 'textarea'
        },
        {
          name: 'birthday',
          type: 'date',
          min: '2015-03-01',
          max: '2025-01-12'
        }
      ];
    }
  }

window.customElements.define('page-alert', PageAlert);