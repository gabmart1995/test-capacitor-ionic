import User from '../models/user.js';

class PageInput extends HTMLElement {
    constructor() {
        super();
        this.name = '';
        this.inputName = null;
        this.appHeader = null;
        this.form = null;
        this.fields = null;
        this.userInstance = new User();
        this.isFormValid = false;
    }

    set _name(name = '') {
        this.name = name;
        this.appHeader.title = this._name;
    }

    get _name() {
        return this.name.length === 0 ? 'Input Forms' : this.name;
    }

    set _isFormValid(isValid = false) {
        this.isFormValid = isValid;

        const formState = this.querySelector('#form-state');
        if (!formState) return;

        formState.innerText = this._isFormValid;
    }

    get _isFormValid() {
        return this.isFormValid ? 'Válido' : 'Inválido';
    }

    connectedCallback() {
        this.innerHTML = (`
            <app-header title="${this._name}" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-list>
                    <ion-list-header>
                        <ion-label>Input normal</ion-label>
                    </ion-list-header>
                    <ion-item>
                        <ion-input 
                            label="Nombre:" 
                            id="input-name" 
                            value="${this.name}" 
                            placeholder="My name" 
                            type="text"
                        ></ion-input>
                    </ion-item>
                    <ion-item>
                        <ion-input 
                            label="Label flotante" 
                            label-placement="floating" 
                            placeholder="Enter text"
                        ></ion-input>
                    </ion-item>
                </ion-list>

                <form id="formulario">
                    <ion-list>
                        <ion-list-header>
                            <ion-label>
                                Formulario valido: 
                                <span id="form-state">${this._isFormValid}</span>
                            </ion-label>
                        </ion-list-header>
                        <ion-item>
                            <ion-input 
                                type="email" 
                                placeholder="Email" 
                                name="email" 
                            ></ion-input>
                        </ion-item>
                        <ion-item>
                            <ion-input 
                                type="password" 
                                placeholder="Password" 
                                name="password" 
                            ></ion-input>
                        </ion-item>
                        <ion-button aria-reset color="medium" class="ion-margin-top" expand="block">Cancelar</ion-button>
                        <ion-button expand="block" type="submit">Enviar formulario</ion-button>
                    </ion-list>
                </form>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.inputName = this.querySelector('#input-name');
        if (!this.inputName) return;

        this.appHeader = this.querySelector('app-header');
        if (!this.appHeader) return;

        this.inputName.addEventListener('ionInput', event => {
            const {value} = event.detail;
            this._name = value.length === 0 ? 'Input Forms' : value;
        });

        this.form = this.querySelector('#formulario');
        if (!this.form) return;

        this.form.addEventListener('submit', this.handleSubmit.bind(this));

        this.fields = Array.from(this.form.querySelectorAll('ion-input'));
        if (this.fields.length === 0) return;

        this.fields.forEach(field => {
            field.addEventListener('ionBlur', ({target}) => target.classList.add('ion-touched'));
            field.addEventListener('ionInput', this.handleInput.bind(this));
        });

        const resetButton = this.querySelector('ion-button[aria-reset]');
        if (!resetButton) return;

        resetButton.addEventListener('click', this.resetForm.bind(this));
    }

    handleSubmit(event) {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = {
            email: '',
            password: ''
        };

        for (const [key, value] of formData.entries()) {
            data[key] = value;
        }

        // limpiamos los campos
        this.fields.forEach(field => {
            field.classList.remove('ion-valid');
            field.classList.remove('ion-invalid');

            // limpiamos los mensajes de error antes de validar
            field.errorText = undefined; 
        });

        // mandamos a validar
        const valid = this.userInstance.validateForm(data);

        if (!valid.isValid) {
            valid.errors.forEach(error => {
                // buscamos los campos marcados con los errores
                const field = this.form.querySelector('ion-input[name="' + error.field + '"');
                if (!field) return;

                // agregamos el campo del error
                field.errorText = error.message;

                // debe agregarse la clase ion-touched para que muestre los errores
                // del formulario si ya esta colocado no es necesario volverlo a agregar
                if (!field.classList.contains('ion-touched')) {
                    field.classList.add('ion-touched');
                } 
                    
                field.classList.add('ion-invalid');
            });
            
        } else {
            // send to backend ...
            console.log(data);
        }
        
        this._isFormValid = valid.isValid;
    }

    handleInput(event) {
        const {target, target: {name, value}} = event;

        target.classList.remove('ion-invalid');
        target.classList.remove('ion-valid');

        const schema = this.userInstance._userSchema[name];
        
        try {
            schema.parse(value);
            target.classList.add('ion-valid');

        } catch (error) {

            if (error instanceof Zod.ZodError) {
                let {issues} = error;
                const [issue] = issues.map(issue => ({message: issue.message}));
                
                target.errorText = issue.message;
                
                if (!target.classList.contains('ion-touched')) {
                    target.classList.add('ion-touched');
                } 
                
                target.classList.add('ion-invalid');
            }
        }
    }

    resetForm() {
        // limpiamos los campos
        this.fields.forEach(field => {
            field.classList.remove('ion-valid');
            field.classList.remove('ion-invalid');

            // limpiamos los mensajes de error antes de validar
            field.errorText = undefined; 
        });

        this.form.reset();
        this._isFormValid = false; // cambiamos el estado del formulario
    }
}

window.customElements.define('page-input', PageInput);