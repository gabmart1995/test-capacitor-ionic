class PageRefresher extends HTMLElement {
    constructor() {
        super();
        this.items = [];
    }

    set _items(items = []) {
        this.items = items;

        const listElement = this.querySelector('ion-list');
        if (!listElement) return;

        listElement.innerHTML = '';

        this._items.forEach((_, index) => listElement.innerHTML += (`
            <ion-item class="animate__animated animate__fadeIn animate__faster">
                <ion-label>Item ${index + 1}</ion-label>
            </ion-item>
        `));
    }

    get _items() {
        return this.items;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="refresher" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-refresher slot="fixed">
                    <ion-refresher-content></ion-refresher-content>
                </ion-refresher>
                <ion-list></ion-list>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        const refresher = this.querySelector('ion-refresher');
        if (!refresher) return;

        refresher.addEventListener('ionRefresh', this.doRefresh.bind(this));
    }

    doRefresh(event) {
        setTimeout(() => {
            this._items = this._items.concat(new Array(20).fill(null));
            event.target.complete();
        }, 2000);
    }
}

window.customElements.define('page-refresher', PageRefresher);