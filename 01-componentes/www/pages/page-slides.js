class PageSlides extends HTMLElement {
    constructor() {
        super();
        this.slides = [
            {
                img: 'assets/slides/photos.svg',
                titulo: 'Comparte Fotos',
                desc: 'Mira y comparte increíbles fotos de todo el mundo'
            },
            {
                img: 'assets/slides/music-player-2.svg',
                titulo: 'Escucha Música',
                desc: 'Toda tu música favorita está aquí'
            },
            {
                img: 'assets/slides/calendar.svg',
                titulo: 'Nunca olvides nada',
                desc: 'El mejor calendario del mundo a tu disposición'
            },
            {
                img: 'assets/slides/placeholder-1.svg',
                titulo: 'Tu ubicación',
                desc: 'Siempre sabremos donde estás!'
            }
        ];
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-content>
                <swiper-container pagination class="slide-full">
                    ${this.slides.map(slide => (`
                        <swiper-slide>
                            <div>
                                <img class="slide-image" src="${slide.img}"/>
                                <ion-card mode="ios">
                                    <ion-card-header>
                                        <ion-card-subtitle>${slide.titulo}</ion-card-subtitle>
                                    </ion-card-header>
                                    <ion-card-content>${slide.desc}</ion-card-content>
                                </ion-card>
                            </div>    
                        </swiper-slide>
                    `)).join('')}
                    <swiper-slide>
                        <div>
                            <ion-button href="/" fill="clear" expand="block">Comenzar</ion-button>    
                        </div>
                    </swiper-slide>
                </swiper-container>
            </ion-content>
        `)
    }
}

window.customElements.define('page-slides', PageSlides);