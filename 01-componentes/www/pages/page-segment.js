import DataService from '../services/data-service.js';

import { filterPipeTransform } from '../pipes/filter-pipe.js';

class PageSegment extends HTMLElement {
    constructor() {
        super();
        this.superHeroes = [];
        this.dataService = new DataService();
        this.listElement = null;
        this.skeletonElement = null;
        this.isLoading = true;
    }

    set _superHeroes(superHeroes = []) {
        this.superHeroes = superHeroes;
        this.listElement.innerHTML = this._superHeroes
            .map(heroe => (`
                <ion-item class="animate__animated animate__fadeIn animate__faster">
                    <ion-label>
                        <h3>${heroe.superhero} <small>${heroe.alter_ego}</small></h3>
                        <p>${heroe.first_appearance}</p>
                    </ion-label>
                    <ion-label class="ion-text-right" slot="end">${heroe.publisher}</ion-label>
                </ion-item>
            `))
            .join('');
    }

    get _superHeroes() {
        return this.superHeroes;
    }

    set _isLoading(isLoading = true) {
        this.isLoading = isLoading;

        if (this._isLoading) {
            this.skeletonElement.style.display = '';
            this.listElement.style.display = 'none';
        
        } else {
            this.skeletonElement.style.display = 'none';
            this.listElement.style.display = '';

        }
    }

    get _isLoading() {
        return this.isLoading;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header class="ion-no-border">
                <ion-toolbar>
                    <ion-buttons slot="start">
                        <ion-router-link routerDirection="root" href="/">
                            <ion-button>
                                <ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
                            </ion-button>
                        </ion-router-link>
                    </ion-buttons>
                    <ion-title>Segment</ion-title>
                </ion-toolbar>
                <ion-segment value="">
                    <ion-segment-button value="">
                        <ion-label>Todos</ion-label>
                    </ion-segment-button>
                    <ion-segment-button value="dc comics">
                        <ion-label>DC-comics</ion-label>
                    </ion-segment-button>
                    <ion-segment-button value="marvel comics">
                        <ion-label>Marvel-comics</ion-label>
                    </ion-segment-button>
                </ion-segment>
            </ion-header>
            <ion-content class="ion-padding">
                <ion-list id="content-list" style="display: none;"></ion-list>
                <!-- skeleton text -->
                <ion-list id="skeleton-list">
                    ${([1,1,1,1,1]).map(() => (`
                        <ion-item>
                            <ion-label>
                                <h3>
                                    <ion-skeleton-text animated style="width: 70%"></ion-skeleton-text>
                                </h3>
                                <p>
                                    <ion-skeleton-text animated style="width: 100%"></ion-skeleton-text>
                                </p>
                            </ion-label>
                            <ion-skeleton-text slot="end" style="width: 50px; height: 50px;"></ion-skeleton-text>
                        </ion-item>
                    `)).join('')}
                </ion-list>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.listElement = this.querySelector('#content-list');
        if (!this.listElement) return;

        this.skeletonElement = this.querySelector('#skeleton-list');
        if (!this.skeletonElement) return;

        const segmentElement = this.querySelector('ion-segment');
        if (!segmentElement) return;
        
        segmentElement.addEventListener('ionChange', this.onSegmentChanged.bind(this));

        this.dataService.getHeroes()
            .then(heroes => {
                setTimeout(() => {
                    this._superHeroes = heroes
                    this._isLoading = false;
                }, 1500);
            })
            .catch(console.error)
    }
    
    onSegmentChanged({detail: {value}}) {
        this._isLoading = true;

        setTimeout(() => {
            this.listElement.innerHTML = filterPipeTransform(this._superHeroes, value, 'publisher')
                .map(heroe => (`
                    <ion-item class="animate__animated animate__fadeIn animate__faster">
                        <ion-label>
                            <h3>${heroe.superhero} <small>${heroe.alter_ego}</small></h3>
                            <p>${heroe.first_appearance}</p>
                        </ion-label>
                        <ion-label class="ion-text-right" slot="end">${heroe.publisher}</ion-label>
                    </ion-item>
                `))
                .join('');

            this._isLoading = false;
        }, 1500);
    }
}

window.customElements.define('page-segment', PageSegment);