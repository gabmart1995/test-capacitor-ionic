class PageToast extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="toast" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-button expand="block">Mostrar toast</ion-button>
                <ion-button expand="block" id="options">Mostrar toast con opciones</ion-button>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        const button = this.querySelector('ion-button[expand="block"]');
        if (!button) return;

        const buttonOptions = this.querySelector('#options');
        if (!buttonOptions) return;

        button.addEventListener('click', this.showToast.bind(this));
        buttonOptions.addEventListener('click', this.showToastOptions.bind(this));
    }

    async showToast() {
        // existen 2 formas de recojer la informacion
        // mediante promesas usando el controller
        // o por elemento por event listeners.
        const toastController = Object.assign(
            document.createElement('ion-toast'), 
            {
                message: 'My first toast',
                duration: 3000,
            }
        );

        // inyectamos el child en el DOM
        this.appendChild(toastController);

        await toastController.present();
        await toastController.onDidDismiss();
        
        // asegurate de remover en el dom el elemento
        // ya que es un elemento descartable
        toastController.remove();
    }

    async showToastOptions() {
        const toastController = Object.assign(
            document.createElement('ion-toast'), 
            {
                position: 'top',
                header: 'Toast header',
                message: 'Click to close',
                buttons: [
                    {
                        text: 'More info',
                        role: 'info',
                        handler: () => {
                            console.log('more info clicked')
                        },
                    },
                    {
                        text: 'Dismiss',
                        role: 'cancel',
                        handler: () => {
                            console.log('dismiss clicked')
                        },
                    }
                ]
            }
        );

        // inyectamos el child en el DOM
        this.appendChild(toastController);
        
        // presentamos
        await toastController.present();
        const response = await toastController.onDidDismiss();
        
        console.log(response);

        // asegurate de remover en el dom el elemento
        // ya que es un elemento descartable
        toastController.remove();
    }
}

window.customElements.define('page-toast', PageToast);