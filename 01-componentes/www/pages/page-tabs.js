class PageTabs extends HTMLElement {
    connectedCallback() {
        this.innerHTML = (`
            <ion-tabs>
                <ion-tab tab="account" component="page-avatar"></ion-tab>
                <ion-tab tab="contact" component="page-list-reorder"></ion-tab>
                <ion-tab tab="seetings" component="page-infinite-scroll"></ion-tab>
                         
                <ion-tab-bar slot="bottom">
                    <ion-tab-button tab="account">
                        <ion-icon  name="person-outline"></ion-icon>
                        <ion-label>Account</ion-label>
                    </ion-tab-button>
                    <ion-tab-button tab="contact">
                        <ion-icon name="call-outline"></ion-icon>
                        <ion-label>Contacts</ion-label>
                    </ion-tab-button>
                    <ion-tab-button tab="seetings">
                        <ion-icon name="cog-outline"></ion-icon>
                        <ion-label>Seetings</ion-label>
                    </ion-tab-button>
                </ion-tab-bar>
            </ion-tabs>
        `);

        this.onMount();
    }

    onMount() {
        const tab = this.querySelector('ion-tab[tab="account"]');
        if (!tab) return;

        tab.setActive();
    }
}

window.customElements.define('page-tabs', PageTabs);