class PageProgress extends HTMLElement {
    constructor() {
        super();
        this.porcentaje = 0.01;
        this.progressBarElement = null;
        this.rangeElement = null;
    }

    get _porcentaje() {
        return this.porcentaje;
    }

    set _porcentaje(pocentaje = 0.01) {
        this.porcentaje = pocentaje;
        this.progressBarElement.value = this._porcentaje;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="progress bar" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-progress-bar 
                    color="primary"  
                    value="${this._porcentaje}" 
                    type="determinate"
                ></ion-progress-bar>   
                <ion-list>
                    <ion-item>
                        <ion-range min="0" max="100">
                            <ion-icon slot="start" name="snow-outline"></ion-icon>
                            <ion-icon slot="end" name="sunny-outline"></ion-icon>
                        </ion-range>
                    </ion-item>
                </ion-list>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.progressBarElement = this.querySelector('ion-progress-bar');
        if (!this.progressBarElement) return;

        this.rangeElement = this.querySelector('ion-range');
        if (!this.rangeElement) return;

        this.rangeElement.addEventListener('ionChange', event => {
            const {value} = event.detail;
            this._porcentaje = (value / 100);
        });
    }
}

window.customElements.define('page-progress', PageProgress);