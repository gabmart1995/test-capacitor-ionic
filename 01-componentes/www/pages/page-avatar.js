class PageAvatar extends HTMLElement {
	connectedCallback() {
		this.innerHTML = (`
			<ion-header class="ion-no-border">
				<ion-toolbar>
					<ion-buttons slot="start">
						<ion-router-link routerDirection="root" href="/">
							<ion-button>
								<ion-icon name="arrow-back-outline" slot="icon-only"></ion-icon>
							</ion-button>
						</ion-router-link>
					</ion-buttons>
					<ion-title>Avatar</ion-title>
					<ion-buttons slot="end">
						<ion-avatar>
							<ion-img alt="stan lee" src="/assets/stan-lee.jpg"></ion-img>
						</ion-avatar>
					</ion-buttons>
				</ion-toolbar>
			</ion-header>
			<ion-content class="ion-padding">		
				${[1,1,1,1,1].map(() => (`
					<ion-chip color="primary" outline="true">
						<ion-label>default</ion-label>
						<ion-avatar>
							<ion-img alt="stan lee" src="/assets/stan-lee.jpg"></ion-img>
						</ion-avatar>
					</ion-chip>
				`)).join('')}
				<ion-list>
				${[1,1,1,1,1,1,1,1,1,1].map(() => (`
					<ion-item>
						<ion-avatar slot="start">
							<ion-img alt="stan lee" src="/assets/stan-lee.jpg"></ion-img>
						</ion-avatar>
						<ion-label>Stan lee</ion-label>
					</ion-item>
				`)).join('')}
				</ion-list>
			</ion-content>
		`)
	}
}

window.customElements.define('page-avatar', PageAvatar);
	