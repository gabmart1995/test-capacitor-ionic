class PageModal extends HTMLElement {
    constructor() {
        super();
        this.ionModalElement = null;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header title="Modal" color="primary"></app-header>
            <ion-content class="ion-padding">
                <ion-button expand="block">Mostrar Modal</ion-button>
                <ion-modal>
                    <ion-header>
                        <ion-toolbar>
                            <ion-title>My Modal</ion-title>
                        </ion-toolbar>
                    </ion-header>
                    <ion-content class="ion-padding">
                        <ion-button expand="block">Salir sin argumentos</ion-button>
                        <ion-button expand="block">Salir con argumentos</ion-button>
                    </ion-content>
                </ion-modal>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.ionModalElement = this.querySelector('ion-modal');
        if (!this.ionModalElement) return;

        const buttonModal = this.querySelector('ion-button[expand="block"]');
        if (!buttonModal) return;

        buttonModal.addEventListener('click', this.showModal.bind(this));
        
        // establece el titulo del modal al abrir
        this.ionModalElement.addEventListener('ionModalWillPresent', () => {
            // recojemos los atributos
            const props = JSON.parse(this.ionModalElement.getAttribute('props'));
            const title = this.ionModalElement.querySelector('ion-title');
            
            if (!title) return

            title.innerText = props.name + ' - ' + props.country;
        });

        this.ionModalElement.addEventListener('ionModalDidDismiss', ({detail: {data: data}}) => {
            if (data) {
                console.log('continuar ejecucion ...', data);
            }
        });

        // =========================================
        // modal elements 
        // =========================================
        const modalButtons = Array.from(this.ionModalElement.querySelectorAll('ion-button'));
        if (modalButtons.length === 0) return;

        const [buttonCloseWithoutArgs, buttonCloseWithArgs] = modalButtons;

        buttonCloseWithoutArgs.addEventListener('click', this.closeModal.bind(this));
        buttonCloseWithArgs.addEventListener('click', this.closeModalArgs.bind(this));
    }

    async showModal() {
        const props = JSON.stringify({
            name: 'Gabriel', country: 'Venezuela'
        });
        // podemos anexar valores a los props dentro del modal
        // usando strings si quieres pasar objetos usa stringify
        this.ionModalElement.setAttribute('props', props);

        await this.ionModalElement.present();
    }

    async closeModal() {
        await this.ionModalElement.dismiss();
    }

    async closeModalArgs() {
        await this.ionModalElement.dismiss({
            name: 'Fernando',
            country: 'Costa Rica'
        });
    }
}

window.customElements.define('page-modal', PageModal);