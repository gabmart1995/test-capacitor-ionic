class PageCheck extends HTMLElement {
    constructor() {
        super();
        this.data = [
            {
                name: 'primary',
                selected: false,
            },
            {
                name: 'secondary',
                selected: true,
            },
            {
                name: 'tertiary',
                selected: false,
            },
            {
                name: 'success',
                selected: true,
            },
        ];
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <app-header color="primary" title="checkbox"></app-header>
            <ion-content class="ion-padding">
                <ion-list>
                    ${this.data.map(item => (`
                        <ion-item>
                            <ion-checkbox 
                                label-placement="start"
                                color="${item.name}" 
                                ${item.selected ? 'checked' : ''} 
                            >${item.name}</ion-checkbox>
                        </ion-item>
                    `)).join('')}
                </ion-list>
                <ion-button expand="block">Ver datos</ion-button>
            </ion-content>
        `);

        this.render();
    }

    render() {
        const checkbox = this.querySelectorAll('ion-checkbox');
        if (checkbox.length === 0) return;

        checkbox.forEach(check => {
            check.addEventListener('ionChange', this.handleChange.bind(this));
        });

        const button = this.querySelector('ion-button[expand="block"]');
        if (!button) return;

        button.addEventListener('click', () => console.log(this.data));
    }

    handleChange(event) {
        // extraemos las propiedades
        const {target: {innerText}, detail: {checked}} = event;
                
        const index = this.data.findIndex(item => item.name === innerText);
        
        if (index > -1) this.data[index].selected = checked;
    }
}

window.customElements.define('page-check', PageCheck);