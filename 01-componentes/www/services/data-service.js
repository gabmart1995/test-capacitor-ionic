export default class DataService {
    getUsers() {
        return fetch('https://jsonplaceholder.typicode.com/users')
            .then(response => {
                if (response.ok) return response.json();

                throw {
                    status: response.status,
                    message: 'no se pudo realizar la peticion',
                    ok: response.ok,
                }
            });
    }

    getAlbumes() {
        return fetch('https://jsonplaceholder.typicode.com/albums')
            .then(response => {
                if (response.ok) return response.json();

                throw {
                    status: response.status,
                    message: 'no se pudo realizar la peticion',
                    ok: response.ok,
                }
            });
    }

    getHeroes() {
        return fetch('/assets/superheroes.json')
            .then(response => {
                if (response.ok) return response.json();

                throw {
                    status: response.status,
                    message: 'no se pudo realizar la peticion',
                    ok: response.ok,
                }
            });
    }
}