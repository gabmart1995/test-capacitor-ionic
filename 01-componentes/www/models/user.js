export default class User {
    constructor () {
        // validation schema
        this.userShema = Zod.object({
            email: Zod.string()
                .email({message: 'Correo inválido'}),
            password: Zod.string()
                .regex(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!#%*?&])([A-Za-z\d$@$#!%*?&]|[^ ]){8,15}$/, 
                    {message: 'La contraseña es inválida'}
                ),
        });
    }

    /**
     * Valida los datos del formulario
     * @param {{email: string, password: string}} user instancia del usuario
     * @returns {{isValid: boolean, errors: Array<{message: string, field: string}> | null}}
     */
    validateForm(user) {
        const result = {isValid: false, errors: null};
        
        try {
            this.userShema.parse(user);

        } catch (error) {

            if (error instanceof Zod.ZodError) {
                const {issues} = error;
                
                result.errors = issues.map(issue => ({
                    message: issue.message,
                    field: issue.path[0]
                }));
            }

            return result;
        }

        result.isValid = true;
        
        return result;
    }

    get _userSchema() {
        return {
            email: Zod.string()
                .email({message: 'Correo inválido'}),
            password: Zod.string()
                .regex(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!#%*?&])([A-Za-z\d$@$#!%*?&]|[^ ]){8,15}$/, 
                    {message: 'La contraseña es inválida'}
                ),
        };
    }
}