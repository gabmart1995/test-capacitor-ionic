# ReactJS + ESBUILD + IONIC
Aplicacion de simulacion de una red social, ejercicio parecido a la vida real
posee sistema de autenticacion, rutas, validaciones, acceso a los recursos 
nativos como la camara, el sistema de archivos y la geolocalizacion.

## Instalacion de Capacitor
`yarn install`

## Transpilacion y servidor de desarrollo
Para ejecutar Esbuild debes tener GO instalado en el equipo y correr el comando 
`yarn dev` levanta la transpilacion y un servidor de desarrollo en tiempo real.

## Compilar para android
Para generar el proyecto en android usa el comando `yarn generate_android` creara los
directorios escritos en JAVA para la compilacion (usar este comando si no tienes el directorio de
andrioid). <br />

Una vez completado debes correr la sincronizacion del proyecto con `yarn sync` y por ultimo
corres el comando de ejecucion `yarn run_android` para correrlo en emulador de Android Studio 
para mas info consultar aqui <br />

Para depurar el proyecto en android studio usa el comando `yarn debug_android`

## Preparacion del enviroment
dentro del directorio 'src/enviroment' se encuentra el archivo de entrono en la
cual la realiza las solictiudes http al server debes asignar la ip o dominio del servidor y el puerto
para que el API pueda recibir las solicitudes.

```
export const ENVIRONMENT = Object.freeze({
    APP_URL: 'http://192.168.1.108:8080',
})
```

### Asignar permisos 
Al generar el proyecto de android debes asignar dentro del AndroidManifest.xml en Android 

```
    <!-- modo de desarrollo: anadir esta propiedad para
        que android pueda realizar solicitudes http. (quitar en produccion)
     -->
    <application ... android:usesCleartextTraffic="true"></application>
    
    <!-- permisos -->
    <uses-permission android:name="android.permission.ACCESS_COARSE_LOCATION" />
    <uses-permission android:name="android.permission.ACCESS_FINE_LOCATION" />
    <uses-feature android:name="android.hardware.location.gps" />

    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```