/**
 * @typedef {{
 *  id: number
 *  name: string
 *  avatar: string
 *  password: string
 *  email: string
 *  posts: Array<import('./post-service').Post>
 *  created_by: string
 *  updated_by: string
 *  deleted_by?: string
 * }} User
 * 
 */

import {ENVIRONMENT} from '../environment/environment';
import {StorageService} from './storage-service';

export class UserService {
    /** @type {UserService} */
    static instance 

    static  getInstance() {
        if (!UserService.instance) UserService.instance = new UserService();
        
        return UserService.instance;
    }
    
    constructor() {
        this.token = '';
        this.storageService = StorageService.getInstance();
        
        /** @type {User | null} */
        this.user = null;
    }

    /**
     * carga el token del usuario del storage
     * @returns {Promise<boolean>}
     */
    async loadToken() {
        this.token = await this.storageService.storage.get('token') ?? '';
        return this.token.length > 0;
    }

    /**
     * realiza la autenticacion al servidor
     * @param {Partial<User>} loginModel 
     * @returns {Promise<{boolean}>}
     */
    login(loginModel) {
        return fetch(`${ENVIRONMENT.APP_URL}/user/login`, {
            body: JSON.stringify(loginModel),
            method: 'POST',
            headers: {
                "Content-Type": "application/json",
            }
        })
            .then(response => {
                if (response.ok) return response.json();
                throw response;
             }) 
             .then(data => { 
                // guardamos los datos en el storage
                if (data.ok) {
                    this.token = data.token;
                    this.storageService.storage.set('token', data.token);
                    
                    return Promise.resolve(true);
                } 

                this.token = '';
                this.storageService.storage.clear();
            
                return Promise.resolve(false);
             });
    }

    /**
     * registro del usuario 
     * @param {Partial<User>} registerModel Modelo del usuario
     * @returns {Promise<{ok: boolean, message: string}>} 
    */
    register(registerModel) {
        return fetch(`${ENVIRONMENT.APP_URL}/user/create`, {
            body: JSON.stringify(registerModel),
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            }
        })
            .then(response => {
                if (response.ok) return response.json();

                throw response;
            })
    }
    
    /**
     * actualiza el usuario
     * @param {Partial<User>} updateModel model del usuario
     * @returns {Promise<{user: Partial<User>, ok: boolean, message: string}>}
     */
    update(updateModel) {
        return fetch(`${ENVIRONMENT.APP_URL}/user/update`, {
            body: JSON.stringify(updateModel),
            method: 'PUT',
            headers: {
                "Content-Type": "application/json",
                "x-token": this.token
            }
        })
            .then(response => {
                if (response.ok) return response.json();

                throw response;
            })
            .then(data => {
                if (data.ok) {
                    this.token = data.token; // token actualizado del usuario
                    this.storageService.storage.set('token', this.token);

                    //obtenemos la informacion del nuevo usuario
                    return fetch(`${ENVIRONMENT.APP_URL}/user`, {
                        method: 'GET',
                        headers: {
                            "Content-Type": "application/json",
                            "x-token": this.token
                        }
                    });
                }
                
                return Promise.reject({message: 'Error en el servidor', ok: false})
            })
            .then(response => {
                if (response.ok) return response.json();

                throw response;
            })
            .then(data => {
                if (data.ok) {
                    this.user = data.user;
                    
                    return Promise.resolve({
                        user: this.user, 
                        ok: data.ok, 
                        message: 'Usuario actualizado con exito'
                    });
                }

                return Promise.reject({message: 'Error en el servidor', ok: false})
            })
    }

    /** funcion que usan los guards para validar las rutas de la app */
    existsUser() {    
        return this.loadToken()
            .then(isLoad => {
                // si el token no esta cargado evitamos hacer la peticion
                if (!isLoad) throw {message: 'token no existe se redirecciona al usuario'};

                //2.- realizamos la peticion
                return fetch(`${ENVIRONMENT.APP_URL}/user`, {
                    method: 'GET',
                    headers: {
                        "Content-Type": "application/json",
                        "x-token": this.token
                    }
                })
            })
            .then(response => {
                if (response.ok) return response.json();
                
                throw response;
            })
            .then(data => {
                if (data.ok) {
                    this.user = data.user;

                    return Promise.resolve(true);
                }

                return Promise.resolve(false);
            })
            .catch(() => {
                // console.error(error);

                return Promise.resolve(false);
            });
    }

    async logOut() {
        await this.storageService.storage.remove('token');
    }
}