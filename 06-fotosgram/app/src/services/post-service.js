/**
 * @typedef {{
 *  id: number
 *  message: string
 *  coordinates?: string
 *  user_id: number
 *  images: Image[]
 *  created_at: string
 *  updated_at: string
 *  deleted_at?: string
 *  user: User
 * }} Post
 * 
 * 
 * @typedef {{
 *  id: number
 *  url: string
 *  post_id: number,
 *  created_at: string
 *  updated_at: string
 *  deleted_at?: string
 * }} Image
 * 
 * @typedef {{
 *  id: number
 *  name: string
 *  avatar: string
 *  email: string
 *  password?: string
 *  posts: Post[]
 *  created_at: string
 *  updated_at: string
 *  deleted_at?: string
 * }} User
 * 
 * @typedef {{
 *  ok: boolean
 *  posts: Post[]
 *  page: number
 * }} ResponsePosts
 */

import {ENVIRONMENT} from "../environment/environment";
import { UserService } from "./user-service";

export class PostService {
    constructor() {
        this.postPage = 0;
        this.userService = UserService.getInstance();
    }
        
    /**
     * Obtiene los posts de la aplicacion
     * @param {boolean} pull vuelve a cargar los registros desde el principio
     * @returns {Promise<ResponsePosts>}
     */
    getPosts(pull = false) {
        // si es un pull to refresh reseteamos el contador
        if (pull) this.postPage = 0;

        ++this.postPage;

        return fetch(`${ENVIRONMENT.APP_URL}/post?page=${this.postPage}`)
            .then(response => {
                if (response.ok) return response.json();
                throw response
            })
    }

    /**
     * Sube el post en la base de datos
     * @param {Partial<Post>} post post del usuario
     * @returns {Promise<{ok: boolean, post: Post}>}
     */
    createPost(post) {
        return fetch(`${ENVIRONMENT.APP_URL}/post`, {
            method: 'POST',
            body: JSON.stringify(post),
            headers: {
                "Content-Type": "application/json",
                "x-token": this.userService.token,
            }
        })
            .then(response => {
                if (response.ok) return response.json();

                throw response;
            })
    }

    /**
     * carga la imagen de forma temporal en el servidor
     * @param {string} imageSrc url de la imagen
     * @returns {Promise<{ok: boolean, message: string}>}
     */
    uploadImageTemp(imageSrc) {
        
        // leemos el archivo usando fetch y obtenemos el blob de los
        // datos en bruto de la imagen
        return fetch(imageSrc)
            .then(response => {
                if (response.ok) return response.blob();
                throw response;
            })
            .then(blob => {
                // extraemos el nombre del archivo
                const result = imageSrc.split('/');
                const fileName = result[result.length - 1];

                // creamos el form data e insertamos el binario
                const formData = new FormData();
                formData.append('image', blob, fileName);

                // lo mandamos al servidor
                return fetch(`${ENVIRONMENT.APP_URL}/post/upload`, {
                    method: 'POST',
                    body: formData,
                    headers: {
                        "x-token": this.userService.token
                    }
                })
            })
            .then(response => {
                if (response.ok) return response.json();
                throw response;
            })
    }
}

