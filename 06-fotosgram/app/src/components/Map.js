import React, {useEffect, useRef} from "react";

const Map = ({coordinates = ''}) => {
    /** @type {React.MutableRefObject<null | HTMLDivElement>} */
    const mapRef = useRef(null)

    useEffect(() => {
        if (mapRef.current) {
            const mapElement = mapRef.current;
            const newCoordinates = coordinates.split(',').map(coordinate => Number(coordinate));
            const tile = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });
        
             // esperamos que cargue el mapa en segundo y medio
            setTimeout(() => {
                const map = L.map(mapElement).setView(newCoordinates, 16)
                tile.addTo(map);
                L.marker(newCoordinates).addTo(map).openPopup(); 
            }, 1500);
        }
    }, []);

    return (
        <>
            <div ref={mapRef} style={{height: 300, width: '100%'}}></div>
        </>
    );
}

export default Map;