import Avatar from "./Avatar";
import Footer from "./Footer";
import Posts from "./Posts";
import Map from './Map';

export {
    Avatar,
    Footer,
    Posts,
    Map
}