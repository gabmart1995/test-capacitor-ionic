import React from "react";
import {IonToolbar, IonGrid, IonRow, IonCol, IonButton, useIonRouter} from '@ionic/react';

const Footer = () => {
    const {push} = useIonRouter();

    return (
        <footer className="footer-register">
            <IonToolbar>
                <IonGrid>
                    <IonRow>
                        <IonCol>
                            <IonButton shape="round"
                                expand="full"
                                size="small"
                                fill="outline"
                                color="tertiary"
                                onClick={() => push('/login')}    
                            >
                                Ingresar
                            </IonButton>
                        </IonCol>
                        <IonCol>
                            <IonButton shape="round"
                                expand="full"
                                size="small"
                                fill="outline"
                                color="tertiary"
                                onClick={() => push('/register')}
                            >
                                Registrarme
                            </IonButton>
                        </IonCol>
                    </IonRow>
                </IonGrid>
            </IonToolbar>
        </footer>
    )
}

export default Footer;