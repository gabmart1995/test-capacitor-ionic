import {IonAvatar, IonCol, IonIcon, IonItem, IonLabel} from "@ionic/react";
import {bookmark, heartOutline} from "ionicons/icons";
import React from "react";
import {imagePipe} from "../pipes/image-pipe";
import {Map} from '.';

const Post = ({post}) => {
    return (
        <IonCol size={12} sizeLg={3} sizeMd={4} sizeSm={6} sizeXs={12}>
            <div className="post">
                <IonItem lines="none">
                    <IonAvatar slot="start">
                        <img src={`assets/avatars/${post.user.avatar}`} />
                    </IonAvatar>
                    <IonLabel>
                        <h3>{post.user.name}</h3>
                        <h5>{post.user.email}</h5>
                    </IonLabel>
                </IonItem>
                {post.coordinates.length > 0 && (<Map coordinates={post.coordinates} />)}
                {post.images.length > 0 && (
                    <swiper-container pagination={true}>
                        {post.images.map((image, index) => (
                            <swiper-slide
                                key={index + 1} 
                                class="image-slide" 
                                style={{backgroundImage: (`url(${imagePipe(image.url, post.user_id)})`)}}
                            ></swiper-slide>
                        ))}
                    </swiper-container>
                )}

                {/** controles like */}
                <IonItem lines="none">
                    <IonIcon slot="start" icon={heartOutline}></IonIcon>
                    <IonIcon slot="end" icon={bookmark}></IonIcon>
                </IonItem>
                
                {/** messages */}
                <IonItem lines="none">
                    <IonLabel className="ion-text-wrap">{post.message}</IonLabel>
                </IonItem>
            </div>
        </IonCol>
    )
}

export default Post;