import React from 'react'

const Avatar = ({avatars, handleAvatar}) => {
    return (
        <>
            <h3 className="ion-text-center" style={{marginBottom: 20}}>Selecciona el avatar</h3>
            <swiper-container slides-per-view="3.5">
                {avatars.map((avatar, index) => {
                    return (
                        <swiper-slide key={index + 1}>
                            <img 
                                className={"pick-avatar " + (avatar.selected ? 'pick-avatar-seleccionado' : 'pick-avatar-no-seleccionado')} 
                                src={`assets/avatars/${avatar.img}`}
                                onClick={() => handleAvatar(index)} 
                            />
                        </swiper-slide>
                    )
                })}
            </swiper-container>
        </>
    )
}

export default Avatar;