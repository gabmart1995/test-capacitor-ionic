import React from "react";
import Post from "./Post";
import {IonGrid, IonRow} from "@ionic/react";

/**
 * componente post
 * @param {{ post: Array<import("../services/posts-services").Post> }} props 
 * @returns {JSX.Element}
 */
const Posts = ({posts}) => {
    return (
        <>
            <IonGrid fixed>
                <IonRow>
                    {posts.map((post, index) => (
                        <Post key={index} post={post} />
                    ))}
                </IonRow>
            </IonGrid>

        </>
    )
}

export default Posts;