import { ENVIRONMENT } from "../environment/environment"

/**
 * arma la url para la imagen
 * @param {string} name nombre de la imagen
 * @param {number} userId identifcador del usuario
 * @returns {string}
 */
const imagePipe = (name, userId) => {
    return (`${ENVIRONMENT.APP_URL}/image/${userId}/${name}`);
}

export {
    imagePipe
}