import React, {useEffect, useState, useMemo} from 'react';
import {Redirect} from 'react-router-dom';
import {UserService} from '../services/user-service';

const LoginGuard =  ({children, location}) => {
    const userService = useMemo(() => UserService.getInstance(), []);
    const [exists, setExists] = useState({result: false, loading: true});
   
    useEffect(() => {
        userService.existsUser()
            .then(existsUser => setExists({result: existsUser, loading: false}));
    }, []);

    // suspense compoent
    if (exists.loading) return null;

    return exists.result ? children : (<Redirect to={{pathname: "/login", state: {from: location}}} />);
}

export default LoginGuard;