import React, {useEffect, useState, useMemo} from 'react';
import {Redirect} from 'react-router-dom';
import {UserService} from '../services/user-service';

const NoLoginGuard =  ({children, location}) => {
    const userService = useMemo(() => UserService.getInstance(), []);
    const [exists, setExists] = useState({loading: true, result: false});
    
    useEffect(() => {
        userService.existsUser()
            .then(result => setExists({result: !result, loading: false}))
    }, []);
        
        // component suspense
    if (exists.loading) return null;

    return exists.result ? children : (<Redirect to={{ pathname: "/app/home", state: {from: location}}} />);
}

export default NoLoginGuard;