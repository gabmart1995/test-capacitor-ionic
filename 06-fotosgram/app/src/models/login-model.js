export class LoginModel {
    constructor() {
        this.loginSchema = Zod.object({
            password: Zod.string(),
                /*.regex(
                    /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[$@$!#%*?&])([A-Za-z\d$@$#!%*?&]|[^ ]){8,15}$/, 
                    {message: 'La contraseña es inválida'}
                ),*/
            email: Zod.string()
                .email({message: 'Correo inválido'}),
        });
    }

    /**
     * valida los datos del registro    
     * @param {{name: string, password: string}} login objeto de registro
     */
    validate(login) {
        const result = {isValid: false, errors: {}};

        try {
            this.loginSchema.parse(login);
            result.isValid = true;

            return result;

        } catch (error) {       
            if (error instanceof Zod.ZodError) {
                let {issues} = error;
                
                issues = issues.map(issue => ({
                    message: issue.message,
                    field: issue.path[0]
                }));

                for (const issue of issues) {
                    result.errors[issue.field] = issue.message; 
                }
                
                console.log(result);

                return result;
            }

            console.error(error);

            return result;
        }
    }
}