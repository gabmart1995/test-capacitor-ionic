import React from "react";
import ReactDom from "react-dom/client";

import App from './App';

// event reload (mode desarrollo)
new EventSource('/esbuild').addEventListener('change', () => location.reload());

import '@ionic/react/css/core.css';
import '@ionic/react/css/padding.css';
import '@ionic/react/css/text-alignment.css';
import './styles.css';

import { StorageService } from "./services/storage-service";

// inicializamos el store
StorageService.getInstance();


const root = ReactDom.createRoot(document.querySelector('#root'));
root.render(<App />);