import {IonApp, IonRouterOutlet} from "@ionic/react";
import {IonReactRouter} from "@ionic/react-router";
import {Redirect, Route} from "react-router-dom";
import React from "react";

import {Login, Main} from './pages';
import Register from "./pages/Register";
import {LoginGuard, NoLoginGuard} from './guards';


const App = () => {
    const ROUTES_APP = [
        {
            path: '/app',
            children: (
                <LoginGuard>
                    <Main />
                </LoginGuard>
            )
        },
        {
            path: '/login',
            children: (
                <NoLoginGuard>
                    <Login />
                </NoLoginGuard>
            )
        },
        {
            path: '/register',
            children: (
                <NoLoginGuard>
                    <Register />
                </NoLoginGuard>
            )
        }
    ];

    return (
        <>
            <IonApp>
                {/** router principal */}
                <IonReactRouter>
                    <IonRouterOutlet>
                        {ROUTES_APP.map((route, index) => (
                            <Route path={route.path} key={index}>{route.children}</Route>
                        ))}
                        <Redirect from="/" to="/login" exact />
                    </IonRouterOutlet>
                </IonReactRouter>
            </IonApp>
        </>
    );
}

export default App;