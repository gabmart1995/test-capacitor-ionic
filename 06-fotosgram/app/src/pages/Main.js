import React from "react";
import {person, home, addCircleOutline} from "ionicons/icons";
import {IonRouterOutlet, IonTabs, IonTabBar, IonTabButton, IonIcon} from "@ionic/react";
import {IonReactRouter} from "@ionic/react-router";
import {Redirect, Route} from "react-router-dom";

import {Home, Post, Profile} from '.'


const Main = () => {
    const ROUTES_APP = [
        {
            path: '/app/home',
            children: (<Home />)
        },
        {
            path: '/app/profile',
            children: (<Profile />)
        },
        {
            path: '/app/new-post',
            children: (<Post />)
        }
    ]

    return (
        <>
            <IonReactRouter>
                <IonTabs>
                    <IonRouterOutlet>
                        {ROUTES_APP.map((route, index) => (
                            <Route path={route.path} key={index}>{route.children}</Route>
                        ))}
                        <Redirect from="/app" to="/app/home" exact />
                    </IonRouterOutlet>
                    <IonTabBar slot="bottom">
                        <IonTabButton tab="/app/home" href="/app/home">
                            <IonIcon icon={home}></IonIcon>
                        </IonTabButton>
                        <IonTabButton tab="/app/new-post" href="/app/new-post">
                            <IonIcon icon={addCircleOutline}></IonIcon>
                        </IonTabButton>
                        <IonTabButton tab="/app/profile" href="/app/profile">
                            <IonIcon icon={person}></IonIcon>
                        </IonTabButton>
                    </IonTabBar>
                </IonTabs>
            </IonReactRouter>
        </>
    );
}

export default Main;