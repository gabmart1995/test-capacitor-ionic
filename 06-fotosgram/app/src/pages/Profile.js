import {IonButtons, IonContent, IonToolbar, IonButton, IonIcon, IonHeader, IonGrid, IonRow, IonCol, IonList, IonItem, IonLabel, IonInput, useIonToast} from "@ionic/react";
import {exit} from "ionicons/icons";
import React, {useMemo, useState} from "react";
import {UserService} from "../services/user-service";

import {Avatar} from '../components'
import {RegisterModel} from "../models/register-model";

const AVATARS = [
    {
      img: 'av-1.png',
      selected: true
    },
    {
      img: 'av-2.png',
      selected: false
    },
    {
      img: 'av-3.png',
      selected: false
    },
    {
      img: 'av-4.png',
      selected: false
    },
    {
      img: 'av-5.png',
      selected: false
    },
    {
      img: 'av-6.png',
      selected: false
    },
    {
      img: 'av-7.png',
      selected: false
    },
    {
      img: 'av-8.png',
      selected: false
    },
];

const Profile = () => {
    const userService = useMemo(() => UserService.getInstance(), []);    
    const registerModel = useMemo(() => new RegisterModel(), []);
    const [user, setUser] = useState({...userService.user});
    const [avatars, setAvatars] = useState(AVATARS.map(avatar => {
        if (avatar.img === user.avatar) {
            return {...avatar, selected: true};
        }

        return {...avatar, selected: false};
    }));
    const [errors, setErrors] = useState({})
    const [present] = useIonToast();

    const logOut = () => {
        // usamos el href porque si se utiliza el ion router
        // se obtiene la instancia del router mas interno a la ruta
        // no rederizando la ruta correcta
        userService.logOut()
            .then(() => window.location.href = '/login');
    }

    const handleAvatar = index => {
        const newAvatars = avatars.map((avatar, i) => {
            if (index === i) return {...avatar, selected: true}
            
            return {...avatar, selected: false};
        });

        setAvatars(newAvatars);
        setUser({...user, avatar: newAvatars[index].img})
    }

    const handleSubmit = event => {
        event.preventDefault();

        const formData = new FormData(event.target);
        const data = {
            avatar: (avatars.find(avatar => avatar.selected)).img,
            password: '',
        }

        for (const key of formData.keys()) {
            data[key] = formData.get(key);
        }

        // validamos la informacion
        const result = registerModel.validate(data)
        setErrors({});

        if (!result.isValid) {
            setErrors({...result.errors});
            return;
        }

        userService.update(data)
            .then(data => present({position: 'top', duration: 1500, message: data.message}))
            .catch(console.error);
    }

    const handleChange = event => {
        const {value, name} = event.target;
        
        setUser({...user, [name]: value});
    }

    return (
        <>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="end">
                        <IonButton onClick={logOut} color="danger">
                            <IonIcon slot="icon-only" icon={exit}></IonIcon>
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent color="light">
                <IonGrid>
                    <IonRow>
                        <IonCol className="img-container">
                            <img 
                                src={`assets/avatars/${user.avatar}`} 
                                className="img-avatar" 
                            />
                        </IonCol>
                    </IonRow>
                </IonGrid>
                <form onSubmit={handleSubmit} style={{marginLeft: 20, marginRight: 20}}>
                    <h3 className="ion-text-center">Actualizar perfil</h3>
                    <Avatar handleAvatar={handleAvatar} avatars={avatars} />
                    <IonList style={{marginTop: 20}}>
                        <IonItem>
                            <IonLabel position="floating">Nombre</IonLabel>
                            <IonInput onInput={handleChange} name="name" value={user.name} type="text"></IonInput>
                        </IonItem>
                        {!!errors.name && (
                            <div className="error-container">
                                <small className="danger">{errors.name}</small>
                            </div>
                        )}
                        <IonItem>
                            <IonLabel position="floating">Email</IonLabel>
                            <IonInput onInput={handleChange} name="email" value={user.email} type="email"></IonInput>
                        </IonItem>
                        {!!errors.email && (
                            <div className="error-container">
                                <small className="danger">{errors.email}</small>
                            </div>
                        )}
                    </IonList>
                    <div className="ion-text-center">
                        <IonButton type="submit" shape="round" style={{marginTop: 20}}>Actualizar</IonButton>
                    </div>
                </form>
            </IonContent>
        </>
    )
}

export default Profile;