import React, { useEffect, useMemo, useRef, useState } from "react";

import {PostService} from '../services/post-service';
import {IonButtons, IonContent, IonHeader, IonIcon, IonInfiniteScroll, IonInfiniteScrollContent, IonRefresher, IonRefresherContent, IonTitle, IonToolbar} from "@ionic/react";
import {Posts} from "../components";
import { bonfire } from "ionicons/icons";

const Home = () => {
    const [posts, setPosts] = useState([]);
    const [disabled, setDisabled] = useState(false);

    const postService = useMemo(() => new PostService(), []);

    const handleInfiniteScroll = ({target}) => { 
        setTimeout(() => {
            postService.getPosts()
                .then(data => {
                    setPosts(posts.concat(data.posts))
                    target.complete();
                })
                .catch(() => {
                    // desactivamos el infinite scroll
                    setDisabled(true);
                });  
        }, 1500)
    };

    // vaciamos los posts
    // activamos nuevamente el infinite scroll
    const handleRefresher = ({target}) => {
        setPosts([]);
        setDisabled(false);

        setTimeout(() => {
            postService.getPosts(true)
                .then(data => {
                    setPosts([...data.posts])
                    target.complete();
                })
                .catch(console.error);  
        }, 1500)
    }

    useEffect(() => {
        postService.getPosts()
            .then(({posts}) => setPosts(posts))
            .catch(console.error)  
    }, []);

    return (
        <>
            <IonHeader class="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonIcon slot="icon-only" icon={bonfire}></IonIcon>
                    </IonButtons>
                    <IonTitle>FotosGram</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent color="light" className="ion-padding">
                <IonRefresher slot="fixed" onIonRefresh={handleRefresher}>
                    <IonRefresherContent></IonRefresherContent>
                </IonRefresher>
                <Posts posts={posts} />
                <IonInfiniteScroll disabled={disabled} threshold="150px" onIonInfinite={handleInfiniteScroll}>
                    <IonInfiniteScrollContent loadingSpinner="crescent" loadingText="Cargando ..."></IonInfiniteScrollContent>
                </IonInfiniteScroll>
            </IonContent>
        </>
    )
}

export default Home;