import {IonContent, IonButton, IonInput, IonItem, IonLabel, IonList, IonImg, useIonRouter, useIonAlert} from "@ionic/react";
import React, {useMemo, useState} from "react";
import {Footer} from "../components";
import {LoginModel} from "../models/login-model";
import {UserService} from "../services/user-service";

const Login = () => {
    const loginModel = useMemo(() => new LoginModel(), []);
    const userService = useMemo(() => UserService.getInstance(), []);
    
    // state
    const [errors, setErrors] = useState({});
    const [submitting, setSubmitting] = useState(false);
    
    // hooks
    const {push} = useIonRouter();
    const [present] = useIonAlert();

    const handleSubmit = event => {
        event.preventDefault();
        setSubmitting(true);

        const formData = new FormData(event.target)
        const data = {
            password: '',
            email: ''
        };

        for (const key of formData.keys()) {
            data[key] = formData.get(key)
        }

        const result = loginModel.validate(data);
        setErrors({...result.errors})

        if (!result.isValid) {
            setSubmitting(false);
            return;
        }
        
        userService.login(data)
            .then(valid => {
                // si es valido redirecciona a la app
                if (valid) return push('/app');
            })
            .catch(response => {
                console.error(response)

                // mostramos una alerta solo si es 400 (credenciales invalidas)
                if (response.status === 400 || response.status === 500) {
                    present({
                        header: 'Informacion',
                        message: 'Credenciales de usuario invalidas',
                        buttons: ['OK']
                    });
                }
            })
            .finally(() => setSubmitting(false));
    }

    return (
        <>
            <IonContent>
                <form className="main-login" onSubmit={handleSubmit}>
                    <IonImg src="assets/avatars/av-1.png"></IonImg>
                    <IonList>
                        <IonItem>
                            <IonLabel position="floating">Correo:</IonLabel>
                            <IonInput name="email" type="email"></IonInput>
                        </IonItem>
                        {!!errors.email && (
                            <div className="error-container">
                                <small className="danger">{errors.email}</small>
                            </div>
                        )}
                        <IonItem>
                            <IonLabel position="floating">Contrasena:</IonLabel>
                            <IonInput name="password" type="password"></IonInput>
                        </IonItem>
                        {!!errors.password && (
                            <div className="error-container">
                                <small className="danger">{errors.password}</small>
                            </div>
                        )}
                    </IonList>
                    <IonButton disabled={submitting} type="submit" shape="round">Acceder</IonButton>
                </form>
            </IonContent>
            <Footer />
        </>
    )
}

export default Login;