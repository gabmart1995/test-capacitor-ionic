import {IonButton, IonButtons, IonCard, IonCol, IonContent, IonGrid, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonRow, IonSpinner, IonTextarea, IonTitle, IonToggle, IonToolbar, useIonRouter, useIonToast} from "@ionic/react";
import {camera, images} from "ionicons/icons";
import React, {useMemo, useState} from "react";

import {Geolocation} from '@capacitor/geolocation';
import {Camera, CameraResultType} from "@capacitor/camera";

import {UserService} from "../services/user-service";
import {PostService} from "../services/post-service";

const Post = () => {
    const userService = useMemo(() => UserService.getInstance(), []);
    const postService = useMemo(() =>  new PostService(), []);
    
    const [loadingSpinner, setLoadingSpinner] = useState(false);
    const [post, setPost] = useState({
        message: '',
        coordinates: '',
        user_id: userService.user.id,
        images: [],
    });

    // hooks
    const [present] = useIonToast();
    const {push} = useIonRouter();

    /** @type {React.FormEventHandler<HTMLIonTextareaElement>} */
    const handleInput = event => {
        const {name, value} = event.target;
        setPost({...post, [name]: value});
    }
    
    /** comparte el post en la base de datos */
    const share = () => {
        // vaciamos el array de imagenes temporales
        // para evitar reventar el servidor
        postService.createPost({...post, images: []})
            .then(data => {
                if (data.ok) {
                    present({
                        duration: 1500,
                        position: 'top',
                        message: 'Post created succesfully',
                        onDidDismiss: () => push('/app/home')
                    })
                }
            })
            .catch(console.error);
    }

    const handleCamara = () => {
        Camera.checkPermissions()
            .then(() => Camera.getPhoto({
                quality: 60,
                resultType: CameraResultType.Uri,
                saveToGallery: false,
                correctOrientation: true,
                allowEditing: true,
            }))
            .then(image => {
                const imageUrl = image.webPath;
                
                setPost({...post, images: [...post.images, imageUrl]});

                return postService.uploadImageTemp(imageUrl);
            })
            .then(result => {
                if (result.ok) {
                    present({
                        duration: 1500,
                        message: result.message,
                        position: 'top',
                    })
                }
            })
            .catch(console.error);
    }

    const handleGallery = () => {
        Camera.checkPermissions()
            .then(() => Camera.pickImages({
                quality: 60,
                correctOrientation: true,
            }))
            .then(({photos}) => {
                const newPhotos = photos.map(photo => photo.webPath);
                setPost({...post, images: [...post.images, ...newPhotos]})

                return Promise.all(newPhotos.map(newPhoto => postService.uploadImageTemp(newPhoto)))
            })
            .then(_ => {
                present({
                    duration: 1500,
                    message: 'files upload(s) succesfully'
                });
            })
            .catch(console.error);
    }

    const getGeolocation = event => {
        const {checked} = event.target;

        if (!checked) {
            setPost({...post, coordinates: null});
            setLoadingSpinner(false);
            return;
        }

        setLoadingSpinner(true);

        Geolocation.checkPermissions()
            .then(() => Geolocation.getCurrentPosition())
            .then(position => {
                const coordinates = (`${position.coords.latitude},${position.coords.longitude}`);
                setPost({...post, coordinates});
            })
            .catch(console.error)
            .finally(() => setLoadingSpinner(false))
    }

    return (
        <>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonTitle>Nuevo Post</IonTitle>
                    <IonButtons slot="end">
                        <IonButton 
                            disabled={(post.message.length === 0) || loadingSpinner} 
                            onClick={share} 
                            color="primary"
                        >
                            Compartir
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent color="light" className="ion-padding">
                <IonList>
                    <IonItem>
                        <IonLabel position="floating">Mensaje</IonLabel>
                        <IonTextarea name="message" onInput={handleInput}></IonTextarea>
                    </IonItem>
                    <IonItem className="ion-padding-top">
                        <IonLabel>Posición actual</IonLabel>
                        {loadingSpinner && (<IonSpinner name="lines-small"></IonSpinner>)}
                        <IonToggle slot="end" onIonChange={getGeolocation}></IonToggle>
                    </IonItem>
                </IonList>
                <IonGrid>
                    <IonRow>
                        <IonCol size={6}>
                            <IonButton onClick={handleCamara} expand="full" shape="round">
                                <IonIcon slot="start" icon={camera}></IonIcon>
                                Cámara
                            </IonButton>
                        </IonCol>
                        <IonCol size={6}>
                            <IonButton onClick={handleGallery} expand="full" shape="round">
                                <IonIcon slot="start" icon={images}></IonIcon>
                                Galería
                            </IonButton>
                        </IonCol>
                    </IonRow>
                    <IonRow>
                        {post.images.length > 0 && post.images.map((image, index) => (
                            <IonCol size={3} key={index}>
                                <IonCard>
                                    <img src={image} style={{width: '100%', height: '100%'}} />
                                </IonCard>
                            </IonCol>
                        ))} 
                    </IonRow>
                </IonGrid>
            </IonContent>
        </>
    )
}

export default Post;