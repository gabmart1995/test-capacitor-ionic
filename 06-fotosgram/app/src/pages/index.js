import Home from "./Home";
import Main from "./Main";
import Profile from "./Profile";
import Login from "./Login";
import Post from "./Post";

export {
    Post,
    Home,
    Profile,
    Login,
    Main
}