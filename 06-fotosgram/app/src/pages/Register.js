import {IonContent, IonList, IonItem, IonLabel, IonInput, IonButton, useIonToast, useIonRouter} from "@ionic/react";
import React, {useMemo, useState} from "react";

import {Avatar, Footer} from '../components'
import { RegisterModel } from "../models/register-model";
import { UserService } from "../services/user-service";

const AVATARS = [
    {
      img: 'av-1.png',
      selected: true
    },
    {
      img: 'av-2.png',
      selected: false
    },
    {
      img: 'av-3.png',
      selected: false
    },
    {
      img: 'av-4.png',
      selected: false
    },
    {
      img: 'av-5.png',
      selected: false
    },
    {
      img: 'av-6.png',
      selected: false
    },
    {
      img: 'av-7.png',
      selected: false
    },
    {
      img: 'av-8.png',
      selected: false
    },
];

const Register = () => {
    const registerModel = useMemo(() => new RegisterModel(), []);
    const userService = useMemo(() => UserService.getInstance(), []);
    
    // state
    const [errors, setErrors] = useState({});
    const [submitting, setSubmitting] = useState(false);
    const [avatars, setAvatars] = useState(AVATARS);

    // hooks
    const [present] = useIonToast();
    const {push} = useIonRouter();

    const handleSubmit = event => {
        event.preventDefault();
        setSubmitting(true);

        const formData = new FormData(event.target)
        const data = {
            avatar: (avatars.find(avatar => avatar.selected)).img,
            email: '',
            name: '',
            password: ''
        };

        for (const key of formData.keys()) {
            data[key] = formData.get(key)
        }

        // validamos la informacion y mostramos los errores en la interfaz
        const result = registerModel.validate(data) 
        setErrors({...result.errors});
        
        // liberamos el boton
        if (!result.isValid) {
            setSubmitting(false);
            return;    
        } 

        // mandamos al servicio
        userService.register(data)
            .then(data => present({
                duration: 1500,
                message: data.message,
                onDidDismiss: () => push('/login')
            }))
            .catch(console.error)
            .finally(() => setSubmitting(false))
    }

    const handleAvatar = index => {
        const newAvatars = avatars.map((avatar, i) => {
            if (index === i) return {...avatar, selected: true}
            
            return {...avatar, selected: false};
        });

        setAvatars(newAvatars);
    }

    return (
        <>
            <IonContent className="main-register">
                <form className="ion-text-center" onSubmit={handleSubmit}>
                    <Avatar handleAvatar={handleAvatar} avatars={avatars} />
                    <IonList>
                        <IonItem>
                            <IonLabel position="floating">Nombre:</IonLabel>
                            <IonInput name="name" type="text"></IonInput>
                        </IonItem>
                        {!!errors.name && (
                            <div className="error-container">
                                <small className="danger">{errors.name}</small>
                            </div>
                        )}
                        <IonItem>
                            <IonLabel position="floating">Correo:</IonLabel>
                            <IonInput name="email" type="email"></IonInput>
                        </IonItem>
                        {!!errors.email && (
                            <div className="error-container">
                                <small className="danger">{errors.email}</small>
                            </div>
                        )}
                        <IonItem>
                            <IonLabel position="floating">Contrasena:</IonLabel>
                            <IonInput name="password" type="password"></IonInput>
                        </IonItem>
                        {!!errors.password && (
                            <div className="error-container">
                                <small className="danger">{errors.password}</small>
                            </div>
                        )}
                    </IonList>
                    <IonButton disabled={submitting} type="submit" shape="round">
                        Registrarse
                    </IonButton>
                </form>
            </IonContent>
            <Footer />
        </>
    )
}

export default Register;