package middlewares

import (
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func CORSMiddleware(c *fiber.Ctx) error {
	// establecemos las cabeceras
	c.Set("Access-Control-Allow-Origin", "*")
	c.Set("Access-Control-Allow-Credentials", "true")
	c.Set("Access-Control-Allow-Headers", "x-token, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization, accept, origin, Cache-Control, X-Requested-With")
	c.Set("Access-Control-Allow-Methods", "POST, OPTIONS, GET, PUT, DELETE")

	// -- esta cabecera permite durante el tiempo indicado almacenar en cache los
	// -- datos de las peticiones preflight CORS
	// -- que son las peticiones OPTIONS que se mandan antes de cada peticion
	c.Set("Access-Control-Max-Age", "300")

	// --- Validación del método y contenido de las variables del header
	// --- captura las peticiones preflight retornando 204
	if c.Method() == "OPTIONS" {
		return c.SendStatus(http.StatusNoContent)
	}

	return c.Next()
}
