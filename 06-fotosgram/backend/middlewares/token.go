package middlewares

import (
	"06-fotosgram/models"
	"errors"
	"net/http"
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/golang-jwt/jwt/v5"
)

const SEED = "este-es-el-seed-de-desarrollo"

type FotosgramToken struct {
	models.User
	jwt.RegisteredClaims
}

// funcion que genera un token jwt
func GetToken(user models.User) (string, error) {
	claims := FotosgramToken{
		user,
		jwt.RegisteredClaims{
			ExpiresAt: jwt.NewNumericDate(time.Now().Add(24 * time.Hour)),
			Issuer:    "test",
		},
	}

	// generamos el token con el cigrado Hash256 y lo firmamos
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenSign, err := token.SignedString([]byte(SEED))

	if err != nil {
		return "", err
	}

	return tokenSign, nil
}

func ValidateToken(c *fiber.Ctx) error {
	rawToken := c.Get("x-token")

	// validamos si llega el token por los headers
	if len(rawToken) == 0 {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
			"ok":      false,
			"message": "Send a authorization token",
		})
	}

	// parseamos el token
	token, err := jwt.Parse(rawToken, func(t *jwt.Token) (interface{}, error) {
		return []byte(SEED), nil
	})

	// si el token esta mal formado
	if errors.Is(err, jwt.ErrTokenMalformed) {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
			"ok":      false,
			"message": "Token malformed",
		})
	}

	// si el token esta vencido o no esta firmado
	if errors.Is(err, jwt.ErrTokenExpired) || errors.Is(err, jwt.ErrTokenNotValidYet) {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
			"ok":      false,
			"message": "User not autorized",
		})
	}

	// si el token no es valido
	if !token.Valid {
		return c.Status(http.StatusUnauthorized).JSON(fiber.Map{
			"ok":      false,
			"message": "User not autorized",
		})
	}

	// procedemos a extraer la informacion
	// para pasarlo a la siguiente funcion
	claims, _ := token.Claims.(jwt.MapClaims)
	c.Locals("user", fiber.Map{
		"id":     claims["id"].(float64),
		"name":   claims["name"].(string),
		"avatar": claims["avatar"].(string),
		"email":  claims["email"].(string),
	})

	// en este punto el token es valido y funciona
	return c.Next()
}
