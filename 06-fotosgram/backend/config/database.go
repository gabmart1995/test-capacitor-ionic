package config

import (
	"06-fotosgram/models"
	"fmt"
	"log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

var Db *gorm.DB

func LoadDB() {
	if Db != nil {
		return
	}

	err := godotenv.Load()

	if err != nil {
		log.Fatal(err)
	}

	// generamos la cadena de conexion
	DSN := fmt.Sprintf(
		"%s:%s@tcp(%s:%s)/%s?charset=utf8mb4&parseTime=True&loc=Local",
		os.Getenv("USER_DB"),
		os.Getenv("PASSWORD"),
		os.Getenv("HOST"),
		os.Getenv("PORT"),
		os.Getenv("DATABASE"),
	)

	Db, err = gorm.Open(mysql.Open(DSN), &gorm.Config{})

	if err != nil {
		log.Fatal(err)
	}

	// correr las migraciones en la base de datos
	runMigrations()
}

func runMigrations() {
	migrator := Db.Migrator()

	// corre la migracion si no existe la tabla en BD
	if (!migrator.HasTable(&models.User{})) {
		migrator.CreateTable(&models.User{})
	}

	if (!migrator.HasTable(&models.Post{})) {
		migrator.CreateTable(&models.Post{})
	}

	if (!migrator.HasTable(&models.Image{})) {
		migrator.CreateTable(&models.Image{})
	}
}
