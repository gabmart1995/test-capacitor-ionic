package services

import (
	"fmt"
	"io/fs"
	"mime/multipart"
	"os"
	"path/filepath"

	"github.com/gofiber/fiber/v2"
	"github.com/google/uuid"
)

func SaveTemporalImage(file *multipart.FileHeader, userID uint, c *fiber.Ctx) error {
	// creacion de directorios
	dirTemp, err := makeDirUser(userID)

	if err != nil {
		return err
	}

	fileName := generateNameFile(file.Filename)
	// fmt.Println(fileName)

	// guardamos los archivos en el direcotrio temporal
	if err := c.SaveFile(file, filepath.Join(dirTemp, fileName)); err != nil {
		return err
	}

	return nil
}

func MoveTemporalToPost(userID uint) ([]string, error) {
	pwd, _ := os.Getwd()
	pathTemp := filepath.Join(pwd, "uploads", fmt.Sprintf("user_%d", userID), "temp")
	pathPosts := filepath.Join(pwd, "uploads", fmt.Sprintf("user_%d", userID), "posts")

	// si no existe el directorio temp continua la ejecucion
	if fileNotExists(pathTemp) {
		return []string{}, nil
	}

	if fileNotExists(pathPosts) {
		// creamos el directorio
		if err := os.MkdirAll(pathPosts, 0775); err != nil {
			return []string{}, err
		}
	}

	var fileNames []string

	// obtenemos un slice con la nombre de los archivos
	err := filepath.Walk(pathTemp, func(path string, info fs.FileInfo, err error) error {
		if err != nil {
			return err
		}

		// fmt.Printf("dir: %v: name: %s\n", info.IsDir(), path)

		if info.IsDir() {
			return nil
		}

		fileNames = append(fileNames, info.Name())

		return nil
	})

	if err != nil {
		return []string{}, nil
	}

	// en este punto ya se tiene la informacion de cada uno de los archivos
	// temporales
	if len(fileNames) == 0 {
		return fileNames, nil
	}

	for _, fileName := range fileNames {

		// movemos el archivo
		err := os.Rename(
			(filepath.Join(pathTemp, fileName)),
			(filepath.Join(pathPosts, fileName)),
		)

		if err != nil {
			return []string{}, err
		}
	}

	return fileNames, nil
}

func makeDirUser(userID uint) (string, error) {
	pwd, _ := os.Getwd()
	pathUser := filepath.Join(pwd, "uploads", fmt.Sprintf("user_%d", userID))
	pathUserTemp := filepath.Join(pathUser, "temp")

	if fileNotExists(pathUserTemp) {
		// creamos el directorio
		if err := os.MkdirAll(pathUserTemp, 0775); err != nil {
			return "", err
		}
	}

	return pathUserTemp, nil
}

func fileNotExists(path string) bool {
	_, err := os.Stat(path)

	return os.IsNotExist(err)
}

func generateNameFile(name string) string {
	ext := filepath.Ext(name)
	id := uuid.NewString()

	return (id + ext)
}

func GetFotoUrl(userID uint, img string) string {
	// 1 crear path post
	pwd, _ := os.Getwd()
	pathImage := filepath.Join(pwd, "uploads", fmt.Sprintf("user_%d", userID), "posts", img)

	// 2 verificar si el archivo existe
	if fileNotExists(pathImage) {
		return filepath.Join(pwd, "assets", "400x250.jpg")
	}

	// 3 devolver el path de la foto
	return pathImage
}
