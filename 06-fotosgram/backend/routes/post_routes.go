package routes

import (
	"06-fotosgram/controllers/post"
	"06-fotosgram/middlewares"

	"github.com/gofiber/fiber/v2"
)

func SetPostRoutes(app *fiber.App) {
	postRouter := app.Group("/post")

	postRouter.Get(
		"/",
		[]func(c *fiber.Ctx) error{
			// middlewares.ValidateToken,
			post.GetAll,
		}...,
	)
	postRouter.Post(
		"/",
		[]func(c *fiber.Ctx) error{
			middlewares.ValidateToken,
			post.Insert,
		}...,
	)
	// ruta para subir imagenes
	postRouter.Post(
		"/upload",
		[]func(c *fiber.Ctx) error{
			middlewares.ValidateToken,
			post.InsertImage,
		}...,
	)
}
