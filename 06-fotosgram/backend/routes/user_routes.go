package routes

import (
	"06-fotosgram/controllers/user"
	"06-fotosgram/middlewares"

	"github.com/gofiber/fiber/v2"
)

func SetUserRouter(app *fiber.App) {
	userRouter := app.Group("/user")

	userRouter.Get(
		"/",
		[]func(c *fiber.Ctx) error{
			middlewares.ValidateToken,
			user.Get,
		}...,
	)

	userRouter.Post("/create", user.Insert)

	userRouter.Post("/login", user.Login)

	userRouter.Put(
		"/update",
		[]func(c *fiber.Ctx) error{
			middlewares.ValidateToken,
			user.Update,
		}...,
	)
}
