package routes

import (
	"06-fotosgram/controllers/image"

	"github.com/gofiber/fiber/v2"
)

func SetImageRoutes(app *fiber.App) {
	imageRouter := app.Group("/image")

	imageRouter.Get("/:user_id/:img", image.GetImage)
}
