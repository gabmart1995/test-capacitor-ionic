package post

import (
	"06-fotosgram/config"
	"06-fotosgram/models"
	"06-fotosgram/services"
	"net/http"
	"regexp"

	"github.com/gofiber/fiber/v2"
)

const SIZE_LIMIT_UPLOAD_MB = 1000000

func GetAll(c *fiber.Ctx) error {
	// calculamos la paginacion
	page := c.QueryInt("page", 1)
	skip := (page - 1) * 10

	var posts []models.Post

	result := config.Db.
		Table("posts").
		Order("id DESC").
		Offset(skip).
		Limit(10).
		Find(&posts)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	if result.RowsAffected == 0 {
		return c.Status(http.StatusNotFound).JSON(fiber.Map{
			"ok":    false,
			"error": "Registers not found",
		})
	}

	// para cada registro consultamos al usuario y las imagenes de la base de datos
	// subconsulta
	for index, post := range posts {

		// obtenemos las imagenes del post
		result = config.Db.
			Table("images").
			Where(models.Image{PostID: post.ID}).
			Find(&posts[index].Images)

		if result.Error != nil {
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"ok":    false,
				"error": result.Error.Error(),
			})
		}

		// buscamos el usuario asociado
		result = config.Db.
			Table("users").
			Omit("password").
			Where(models.User{ID: post.UserID}).
			Find(&posts[index].User)

		if result.Error != nil {
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"ok":    false,
				"error": result.Error.Error(),
			})
		}
	}

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"ok":    true,
		"posts": posts,
		"page": page,
	})
}

func Insert(c *fiber.Ctx) error {
	mapUser := c.Locals("user").(fiber.Map)
	post := models.Post{
		UserID: uint((mapUser["id"]).(float64)),
	}

	// movemos los archivos temporales a la carpeta post
	files, err := services.MoveTemporalToPost(post.UserID)

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	if err := c.BodyParser(&post); err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	errors, err := post.Validate([]string{})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	if errors != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": errors,
		})
	}

	result := config.Db.Create(&post)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	// consultamos al usuario de la base de datos
	result = config.Db.
		Table("users").
		Omit("password").
		Where(models.User{ID: post.UserID}).
		Find(&post.User)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	// recorremos si existen imagenes en la carpeta temporal
	// registramos las mismas en la base de datos
	if len(files) > 0 {

		for _, file := range files {
			result = config.Db.Create(&models.Image{PostID: post.ID, Url: file})

			if result.Error != nil {
				return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
					"ok":    false,
					"error": result.Error.Error(),
				})
			}
		}

	}

	return c.Status(http.StatusCreated).JSON(fiber.Map{
		"ok":   true,
		"post": post,
	})
}

func InsertImage(c *fiber.Ctx) error {
	mapUser := c.Locals("user").(fiber.Map)
	userId := uint((mapUser["id"]).(float64))

	form, err := c.MultipartForm()

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	files, ok := form.File["image"]

	// si no existe ninguna imagen
	if len(files) == 0 || !ok {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": "No upload images",
		})
	}

	regexp, err := regexp.Compile("^image/(png|jpg|jpeg|gif)$")

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	for _, file := range files {
		// validamos el tamano de archivo
		if file.Size > SIZE_LIMIT_UPLOAD_MB {
			return c.Status(http.StatusBadRequest).JSON(fiber.Map{
				"ok":    false,
				"error": "The file not must exceded of 1MB",
			})
		}

		// procesamos el tipo de archivo
		mimeType := file.Header["Content-Type"][0]

		if !regexp.Match([]byte(mimeType)) {
			return c.Status(http.StatusBadRequest).JSON(fiber.Map{
				"ok":    false,
				"error": "The file not is a image",
			})
		}

		// salvamos el archivo en el directorio temporal
		if err := services.SaveTemporalImage(file, userId, c); err != nil {
			return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
				"ok":    false,
				"error": err.Error(),
			})
		}
	}

	return c.Status(http.StatusCreated).JSON(fiber.Map{
		"ok":      true,
		"message": "file(s) upload succesfully",
	})
}
