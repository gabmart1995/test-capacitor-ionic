package user

import (
	"06-fotosgram/config"
	"06-fotosgram/middlewares"
	"06-fotosgram/models"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func Insert(c *fiber.Ctx) error {
	user := models.User{}

	if err := c.BodyParser(&user); err != nil {
		return err
	}

	errors, err := user.Validate([]string{})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	if errors != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": errors,
		})
	}

	// aplicamos hash a la contrasena
	hash, _ := config.HashPassword(user.Password)
	user.Password = hash

	result := config.Db.Create(&user)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"ok":      true,
		"message": "User created succesfully",
	})
}

func Login(c *fiber.Ctx) error {
	var userDB models.User
	user := models.User{}

	if err := c.BodyParser(&user); err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    "false",
			"error": err.Error(),
		})
	}

	errors, err := user.Validate([]string{"Email", "Password"})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	if errors != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": errors,
		})
	}

	// buscamos en la base de datos
	result := config.Db.
		Table("users").
		Where(&models.User{Email: user.Email}).
		First(&userDB)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	// revisamos si existen registros
	if result.RowsAffected == 0 {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": "user or password not valid",
		})
	}

	// revisamos la contrasena
	if !config.CheckPassword(user.Password, userDB.Password) {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": "user or password not valid",
		})
	}

	// obtenemos el token jwt
	userDB.Password = ""
	token, err := middlewares.GetToken(userDB)

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    "false",
			"error": err.Error(),
		})
	}

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"ok":    true,
		"token": token,
	})
}

func Update(c *fiber.Ctx) error {
	// extraemos de los locals la propiedad user
	mapUser := c.Locals("user").(fiber.Map)
	user := models.User{
		ID: uint((mapUser["id"]).(float64)),
	}

	if err := c.BodyParser(&user); err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	errors, err := user.Validate([]string{"Email", "Avatar", "Name"})

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	if errors != nil {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": errors,
		})
	}

	// procedemos a verificar en BD si existe el usuario
	result := config.Db.First(&models.User{}, user.ID)

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	// si no lo encuentra
	if result.RowsAffected == 0 {
		return c.Status(http.StatusBadRequest).JSON(fiber.Map{
			"ok":    false,
			"error": "No exists record",
		})
	}

	// procedemos a actualizar
	result = config.Db.
		Table("users").
		Select("Name", "Avatar", "Email").
		Where(models.User{ID: user.ID}).
		Updates(models.User{
			Name:   user.Name,
			Avatar: user.Avatar,
			Email:  user.Email,
		})

	if result.Error != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": result.Error.Error(),
		})
	}

	token, err := middlewares.GetToken(user)

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"ok":      true,
		"token":   token,
		"message": "User update succesfully",
	})
}

// ruta que nos permite obtener el usuario del token
func Get(c *fiber.Ctx) error {
	mapUser := c.Locals("user").(fiber.Map)

	return c.Status(http.StatusOK).JSON(fiber.Map{
		"ok":   true,
		"user": mapUser,
	})
}
