package image

import (
	"06-fotosgram/services"
	"net/http"

	"github.com/gofiber/fiber/v2"
)

func GetImage(c *fiber.Ctx) error {
	userID, err := c.ParamsInt("user_id", 0)
	img := c.Params("img", "")

	if err != nil {
		return c.Status(http.StatusInternalServerError).JSON(fiber.Map{
			"ok":    false,
			"error": err.Error(),
		})
	}

	pathFile := services.GetFotoUrl(uint(userID), img)

	return c.Status(http.StatusOK).SendFile(pathFile)
}
