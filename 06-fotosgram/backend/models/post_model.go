package models

import (
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type Post struct {
	ID          uint           `json:"id"`
	Message     string         `valdidate:"required" json:"message"`
	Coordinates string         `json:"coordinates"`
	Images      []Image        `gorm:"foreignkey:PostID" validate:"omitempty" json:"images"`
	UserID      uint           `gorm:"not null" validate:"required" json:"user_id"`
	CreatedAt   time.Time      `json:"create-at"`
	UpdatedAt   time.Time      `json:"updated_at"`
	DeletedAt   gorm.DeletedAt `gorm:"index" json:"deleted_at"`
	User        User           `gorm:"-" validate:"omitempty" json:"user"` // campo del usuario que se almacena el campo primario
}

func (p *Post) Validate(fields []string) (fiber.Map, error) {
	validate := validator.New()
	var err error

	if len(fields) > 0 {
		err = validate.StructPartial(p, fields...)

	} else {
		err = validate.Struct(p)

	}

	if err != nil {
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return nil, err
		}

		errors := fiber.Map{}

		for _, err := range err.(validator.ValidationErrors) {
			errors[err.Field()] = err.Error()
		}

		return errors, nil
	}

	return nil, nil
}
