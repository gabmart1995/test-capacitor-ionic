package models

import (
	"time"

	"gorm.io/gorm"
)

type Image struct {
	ID        uint           `json:"id"`
	Url       string         `json:"url"`
	PostID    uint           `gorm:"not null" json:"post_id"` // identificador clave foranea de la estructura tabla post
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}
