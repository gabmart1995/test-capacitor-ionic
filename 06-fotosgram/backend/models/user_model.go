package models

import (
	"time"

	"github.com/go-playground/validator/v10"
	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

type User struct {
	ID        uint           `gorm:"primaryKey;autoIncrement" json:"id"`
	Name      string         `gorm:"not null" json:"name" validate:"required"`
	Avatar    string         `gorm:"default:av-1.png;not null" json:"avatar"`
	Email     string         `gorm:"unique;not null" json:"email" validate:"required,email"`
	Password  string         `gorm:"not null" json:"password" validate:"required"`
	Posts     []Post         `gorm:"foreignkey:UserID" validate:"omitempty" json:"posts"`
	CreatedAt time.Time      `json:"created_at"`
	UpdatedAt time.Time      `json:"updated_at"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"deleted_at"`
}

func (u *User) Validate(fields []string) (fiber.Map, error) {
	validate := validator.New()
	var err error

	if len(fields) > 0 {
		err = validate.StructPartial(u, fields...)

	} else {
		err = validate.Struct(u)

	}

	if err != nil {
		// this check is only needed when your code could produce
		// an invalid value for validation such as interface with nil
		// value most including myself do not usually have code like this.
		if _, ok := err.(*validator.InvalidValidationError); ok {
			return nil, err
		}

		errors := fiber.Map{}

		for _, err := range err.(validator.ValidationErrors) {
			errors[err.Field()] = err.Error()
		}

		return errors, nil
	}

	return nil, nil
}
