# Fotosgram API
API para el servicio de la app de fotosgram, construido con GO, implementando
Fiber como biblioteca de desarrollo HTTP y GORM un Object Relational Mapping
para controlar las migraciones de la base de datos y las consultas.

## Estructura DB
al inicializar la APP GORM realiza las migraciones de forma automatica si no
existen la tabla en la BD. se deja el respaldo del mismo en un archivo .sql 

## Correr el API
Para correr el API se utiliza el puerto 8080 con el comando `go run .` o
si deseas compilarlo usa el comando `go build .`
