package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"

	"06-fotosgram/config"
	"06-fotosgram/middlewares"
	"06-fotosgram/routes"

	"github.com/gofiber/fiber/v2"
	"gorm.io/gorm"
)

/* enfoque recomendado cierra las conexiones correctamente */
func main() {
	config.LoadDB()

	// cerramos la conexion cuando termine el hila principal
	defer closeDB(config.Db)

	// levantamos el servidor en una goroutine
	server := fiber.New()
	server.Use(middlewares.CORSMiddleware)
	routes.SetUserRouter(server)
	routes.SetPostRoutes(server)
	routes.SetImageRoutes(server)

	go func() {
		server.Listen(":8080")
	}()

	shutdownServer(server)
}

func shutdownServer(server *fiber.App) {
	// creamos un canal que escuche los signals del SO
	// para deterner la conexion de la base de datos
	// y el server de forma correcta
	signalChannel := make(chan os.Signal, 1)
	signal.Notify(signalChannel, os.Interrupt)
	signal.Notify(signalChannel, syscall.SIGTERM)

	<-signalChannel

	// detenemos las peticiones al servidor para que cierre cuando se
	// terminen de realizar todas las peticiones
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()

	if err := server.ShutdownWithContext(ctx); err != nil {
		log.Fatal(err)

	} else {
		fmt.Println("servidor cerrado exitosamente")

	}
}

func closeDB(db *gorm.DB) {
	fmt.Println("cerramos la conexion BD")
	sql, err := db.DB()

	if err != nil {
		log.Fatal(err)
	}

	sql.Close()
}
