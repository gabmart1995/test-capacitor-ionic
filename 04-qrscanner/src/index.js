import React from 'react'
import ReactDom from 'react-dom/client';

// event reload (mode desarrollo)
new EventSource('/esbuild').addEventListener('change', () => location.reload());

import '@ionic/react/css/core.css';
import './style.css';

import 'leaflet/dist/leaflet.css';

import App from './App';
import {StorageService} from './services/storage-service';

// inicializamos el store
StorageService.getInstance();

const root = ReactDom.createRoot(document.querySelector('#root'));
root.render(<App />);