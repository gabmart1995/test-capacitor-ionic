import {create, globe, pin} from "ionicons/icons";

export class Register {
    constructor(content = '', format = '', hasContent = false) {
        this.content = content;
        this.format = format;
        this.hasContent = hasContent;
        this.type = '',
        this.icon = '',
        this.created = new Date();

        this.determinateType();
    }

    determinateType() {
        const text = this.content.substring(0, 4);
        
        switch (text) {
            case 'http': 
                this.type = 'http';
                this.icon = globe;
            break;

            case 'geo:': 
                this.type = 'geo';
                this.icon = pin;
            break;

            default:
                this.type = 'No reconocido';
                this.icon = create;
        }
    }
}