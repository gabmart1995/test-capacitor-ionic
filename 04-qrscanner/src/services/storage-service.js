import {Storage} from '@ionic/storage';
import {Browser} from '@capacitor/browser';
import {Filesystem, Directory, Encoding} from '@capacitor/filesystem';

import {Register} from "../models/register";

export class StorageService {
    /** @type {StorageService} */
    static instance
    
    constructor() {
        /** @type {Array<Register>} */
        this.registers = []

        /** @type {Storage | null} */
        this.storage = null;

        this.init();
    }

    get _registers() {
        return this.registers;
    }

    static getInstance() {
        if (!StorageService.instance) StorageService.instance = new StorageService();

        return StorageService.instance;
    }

    async init() {
        this.storage = await (new Storage()).create();
        
        await this.loadRegisters();
    }

    /**
     * Salva un registro
     * @param {string} format formato del codigo
     * @param {string} content contenido
     * @param {boolean} hasContent determina si tiene contenido
     * @returns {Promise<Register>} 
    */
    async saveRegister(format, content, hasContent) {
        // await this.loadRegisters();
        
        const register = new Register(content, format, hasContent);
        
        this.registers.unshift(register);

        await this.storage.set('registers', this._registers);
        
        return register;
    }

    async loadRegisters() {
        const registers = await this.storage.get('registers');
        
        this.registers = registers ?? [];

        return this.registers;
    }

    /**
     * Abre el registro en el navegagor  
     * @param {Register} register
     * @param {() => void} push metodo que realiza una redireccion
     */
    async openRegister(register, push) {        
        switch (register.type) {
            case 'http':
                await Browser.open({url: register.content});
           break;

           case 'geo':
                push(`/map/${register.content}`);
           break;

            default: 
                console.log('metodo no disponible');
        }
    }

    /**
     * simulacion para crear un correo
     * @param {(message: string, position: 'top' | 'middle' | 'bottom') => void} callbackToast 
     */
    async sendEmail(callbackToast) {
        /** @type {Array<string>} */
        const array = [];
        const titles = 'Tipo, Formato, Creado en, Texto\n';
        
        array.push(titles);

        this.registers.forEach(register => {
            const row = (`${register.type}, ${register.format}, ${register.created}, ${register.content.replace(',', ' ')}\n`)
            array.push(row);
        });

        try {
            await this.createFile(array.join(''));

            // llamamos a la funcion que actualiza el toast
            callbackToast('Archivo de registros fue modificado con exito');

        } catch (error) {
            console.error(error);
        }
    }

    /**
     * Escribe los datos en el dispositivo  
     * @param {string} text contenido del archivo
     * @returns {Promise<void>}
     */
    createFile(text) {
        return new Promise((resolve, reject) => {
            const OPTIONS = {
                path: 'registros.csv',
                directory: Directory.Documents
            };
    
            // 1.- determinar si el archivo existe en el directorio
            Filesystem.stat(OPTIONS)
                .then(fileStats => {
                    console.log(fileStats);
    
                    // en este punto se realiza un appendfile
                    Filesystem.writeFile({...OPTIONS, data: text, encoding: Encoding.UTF8})
                        .then(() => {
                            console.log('archivo modificado con exito');
                            resolve();
                        })
                        .catch((error) => {
                            console.error(error);
                            reject(error);
                        });
                })
                .catch((_) => {
                    // si pasa por aqui es que el archivo no existe en el direcotrio       
                    console.log('catch no se encontro el archivo');
    
                    // procedemos a crear el archivo
                    Filesystem.writeFile({...OPTIONS, data: text, encoding: Encoding.UTF8})
                        .then(() => {
                            console.log('archivo creado con exito');
                            resolve();
                        })
                        .catch((error) => {
                            console.error(error);
                            reject(error);
                        });
                });
        })
    }
}