import React, {useEffect, useRef} from 'react';
import {useParams} from 'react-router-dom';

import {IonBackButton, IonButtons, IonContent, IonHeader, IonTitle, IonToolbar} from '@ionic/react';
import * as L from 'leaflet';

const TabMap = () => {
    /** @type {{ geo: string }} */
    const {geo} = useParams();
    const coordinates = geo.substring(4).split(',').map(coor => Number(coor));

    /** @type {React.MutableRefObject<undefined | HTMLDivElement>} */
    const mapRef = useRef()

    useEffect(() => {
        if (mapRef.current) {
            const tile = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
                maxZoom: 19,
                attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
            });

            // utlizamos un timeout para cargar el tile del mapa
            setTimeout(() => {
                const map = L.map(mapRef.current).setView(coordinates, 16);
                tile.addTo(map);    
                L.marker(coordinates).addTo(map).openPopup();
            }, 1500);
        }
    }, []);

    return (
        <>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot='start'>
                        <IonBackButton defaultHref="/history" color="primary" />
                    </IonButtons>
                    <IonTitle>Mapa</IonTitle>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <div id="map" ref={mapRef}></div>
            </IonContent>
        </>
    );
}

export default TabMap;