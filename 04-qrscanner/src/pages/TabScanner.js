import React, {useMemo} from "react";
import {createPortal} from "react-dom";
import {BarcodeScanner} from '@capacitor-community/barcode-scanner';
import {IonContent, IonButton, useIonRouter} from "@ionic/react";

import {register} from 'swiper/element/bundle'

import {Header, ScannerUI} from "../components";
import {StorageService} from "../services/storage-service";


// registramos los web components de swiper
register();

const scannerUI = document.querySelector('#scanner-ui');

const TabScanner = () => {
    const {push} = useIonRouter();
    const storageService = useMemo(() => StorageService.getInstance(), []);

    /** Realiza el escaneo del codigo */
    const startScan = async () => {
        try {
            // verifica el permiso de la camara
            const permission = await BarcodeScanner.checkPermission({ force: true });

            if (permission.denied) return;

            // para mostrar el escanner debemos hacer la aplicacion transparente con CSS
            // usamos la funcion de hideBackground y adicionalmente una clase CSS que oculta la app 
            // mientras se realiza el escaneo.

            // se utiliza un portal de react para construir un elemento fuera de react
            BarcodeScanner.hideBackground();
            
            document.body.classList.add('qrscanner');

            // escanea el QR
            const result = await BarcodeScanner.startScan();
            
            if (result.hasContent) {
                const register = await storageService.saveRegister(
                    result.format, 
                    result.content, 
                    result.hasContent
                );
                
                document.body.classList.remove('qrscanner');

                // una vez seleccionado redireccionamos
                push('/history'); 

                setTimeout(() => storageService.openRegister(register, push), 1500);
            }
            
        } catch (error) {
            console.error(error);

            // inserta en el store (modo de desarrollo) 
            // para probar el store
            // const register = await storageService.saveRegister('QR_Code', 'https://gabmart1995.github.io/', true);
            const register = await storageService.saveRegister('QR_Code', 'geo:40.73151796986687,-74.06087294062502', true);

            push('/history');
            
            // llamar abrir registro
            setTimeout(() => storageService.openRegister(register, push), 1500);

            // cerramos el scanner para ahorrar bateria
            // BarcodeScanner.showBackground();
            // BarcodeScanner.stopScan();
        } 
    };

    /** cancela el escaneo del codigo */
    const stopScan = () => {
        BarcodeScanner.showBackground();
        
        document.body.classList.remove('qrscanner');
        
        BarcodeScanner.stopScan();
    }

    return (
        <>
            <Header title="Escanear" />
            <IonContent>
                <swiper-container>
                    <swiper-slide allow-slide-prev="false" allow-slide-next="false">
                        <IonButton 
                            shape="round" 
                            size="large" 
                            expand="full" 
                            fill="outline" 
                            onClick={startScan}
                        >
                            Escanear QR.
                        </IonButton>
                    </swiper-slide>
                </swiper-container>
            </IonContent>
            {scannerUI && createPortal( // colocamos un portal para construir la ui del scanner
                (<ScannerUI closeScanner={stopScan} />),
                scannerUI
            )}
        </>
    )
}

export default TabScanner;