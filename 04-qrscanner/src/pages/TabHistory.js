import React, {useEffect, useMemo, useState} from "react";
import {IonButton, IonButtons, IonContent, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonTitle, IonToolbar, useIonRouter, useIonToast} from "@ionic/react";
import {send} from "ionicons/icons";

import {StorageService} from "../services/storage-service";
import {datePipe} from '../pipes/date-pipe';

const TabHistory = () => {
    const storageService = useMemo(() => StorageService.getInstance(), []);
    const [registers, setRegisters] = useState(storageService._registers)
    const {push} = useIonRouter();
    const [present] = useIonToast();

    const showToast = (message = '', position = 'bottom') => {
        present({
            message,
            duration: 1500,
            position
        });
    }

    useEffect(() => {
        if (storageService._registers.length > 0) return;

        storageService.loadRegisters()
            .then(registers => setRegisters(registers))
            .catch(console.error)
    }, []);

    return (
        <>
            <IonHeader className="ion-no-border">
                <IonToolbar>
                    <IonTitle>Historial</IonTitle>
                    <IonButtons slot="end">
                        <IonButton color="primary" onClick={storageService.sendEmail.bind(storageService, showToast)}>
                            <IonIcon icon={send} slot="icon-only"></IonIcon>
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonHeader>
            <IonContent>
                <IonList>
                    {registers.map((register, index) => (
                        <IonItem 
                            detail 
                            key={index + 1} 
                            onClick={storageService.openRegister.bind(storageService, register, push)}
                        >
                            <IonIcon color="primary" slot="start" icon={register.icon}></IonIcon>
                            <IonLabel>
                                <h2>{register.content}</h2>
                                <p>{datePipe(register.created)}</p>
                            </IonLabel>
                        </IonItem>
                    ))}
                </IonList>
            </IonContent>
        </>
    )
}

export default TabHistory;