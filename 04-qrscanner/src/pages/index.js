import TabHistory from "./TabHistory";
import TabMap from "./TabMap";
import TabScanner from "./TabScanner";

export {
    TabScanner,
    TabHistory,
    TabMap
}