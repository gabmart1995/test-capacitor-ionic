import React from 'react';
import {Redirect, Route} from 'react-router-dom';

import {book, qrCode} from 'ionicons/icons';

import {IonApp, IonIcon, IonLabel, IonRouterOutlet, IonTabBar, IonTabButton, IonTabs} from '@ionic/react';
import {IonReactRouter} from '@ionic/react-router';

import {TabHistory, TabMap, TabScanner} from './pages';

const App = () => {
    return (
        <>
            <IonApp className="scanner-hide">
                <IonReactRouter>
                    <IonTabs>
                        <IonRouterOutlet>
                            <Route path="/scanner" component={TabScanner} />
                            <Route path="/history" component={TabHistory} />
                            <Route path="/map/:geo" component={TabMap} />
                            <Redirect exact from="/" to="/scanner" />
                        </IonRouterOutlet>
        
                        {/** tab bar */}
                        <IonTabBar slot="bottom">
                            <IonTabButton tab="/scanner" href="/scanner">
                                <IonIcon icon={qrCode}></IonIcon>
                                <IonLabel>Escanear</IonLabel>
                            </IonTabButton>
                            <IonTabButton tab="/history" href="/history">
                                <IonIcon icon={book}></IonIcon>
                                <IonLabel>Historial</IonLabel>
                            </IonTabButton>
                        </IonTabBar>
                    </IonTabs>
                </IonReactRouter>
            </IonApp>
        </>
    );
}

export default App;