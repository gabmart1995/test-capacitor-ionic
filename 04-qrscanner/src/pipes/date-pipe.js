/**
 * Transforma la fecha
 * @param {Date} date fecha a transformar
 * @returns {string} 
 */
const datePipe = (date = new Date()) => {
    const year = date.getFullYear();
    const mouth = date.toLocaleString('default', { month: 'short'});
    const day = date.getDate() > 9 ? (date.getDate()).toString() : '0' + (date.getDate()).toString();
    const hour = date.getHours() > 9 ? (date.getHours()).toString() : '0' + (date.getHours()).toString();
    const minutes = date.getMinutes() > 9 ? (date.getMinutes()).toString() : '0' + (date.getMinutes()).toString();
    const seconds = date.getSeconds() > 9 ? (date.getSeconds()).toString() : '0' + (date.getSeconds()).toString();

    return (`${mouth} ${day}, ${year} ${hour}:${minutes}:${seconds}`);
}

export {
    datePipe
}