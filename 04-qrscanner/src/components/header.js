import { IonHeader, IonTitle, IonToolbar } from '@ionic/react';
import React from 'react';

const Header = ({title}) => {
    return (
        <IonHeader className="ion-no-border">
            <IonToolbar>
                <IonTitle>{title}</IonTitle>
            </IonToolbar>
        </IonHeader>
    );
}

export default Header;