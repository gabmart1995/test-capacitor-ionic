import {IonButtons, IonFooter, IonToolbar, IonButton} from '@ionic/react';
import React from 'react';

const ScannerUI = ({closeScanner}) => {
    return (
        <>
            <div className="scanner-content">
                <div className="scanner-container"></div>
            </div>
            <IonFooter className="ion-no-border">
                <IonToolbar>
                    <IonButtons slot="start">
                        <IonButton style={{color: 'white'}} onClick={closeScanner}>
                            Cancelar
                        </IonButton>
                    </IonButtons>
                </IonToolbar>
            </IonFooter>
        </>
    );
}

export default ScannerUI;