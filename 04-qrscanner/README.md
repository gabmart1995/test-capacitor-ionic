# REACT JS + ESBUILD + IONIC

Este proyecto es un lector de condigos QR integrado
con React js, el objetivo de este proyecto es trabajar con plugin nativos de capacitor.

## Instalacion de Capacitor
`yarn install`

### Transpilacion y servidor de desarrollo
Para ejecutar esbuild debes tener [GO](https://go.dev) instalado en el equipo y correr el comando <br />
`yarn dev` levanta la transpilacion y un servidor de desarrollo en tiempo real.

## Compilar para android
Para generar el proyecto en android usa el comando `yarn generate_android` creara los 
directorios escritos en JAVA para la compilacion (usar este comando si no tienes el directorio de 
andrioid).<br /><br />

Una vez completado debes correr la sincronizacion del proyecto con `yarn sync` y por ultimo
corres el comando de ejecucion `yarn run_android` para correrlo en emulador de Android Studio para mas info consultar [aqui](https://capacitorjs.com/docs/android)

## Permisos del plugin 
Una vez generado el proyecto de android debes agregar en el `AndroidManifest.xml` el siguiente 
codigo:

```
    <!--  Manifest barcode -->
    <manifest ... xmlns:tools="http://schemas.android.com/tools"></manifest>

    <!-- aplication -->
    <application ... android:hardwareAccelerated="true"></application>

    <!-- permisos del barcode scanner -->
    <uses-permission android:name="android.permission.CAMERA" />
    <uses-sdk tools:overrideLibrary="com.google.zxing.client.android" />

    <!-- permisos del filesystem store -->
    <uses-permission android:name="android.permission.READ_EXTERNAL_STORAGE"/>
    <uses-permission android:name="android.permission.WRITE_EXTERNAL_STORAGE" />
```

con esto se habilita los permisos de tu aplicacion para el uso de la camara y almacenamiento 
de archivos