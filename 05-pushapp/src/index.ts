import './pages';

import {PushService} from './services/push-services';

new EventSource('/esbuild').addEventListener('change', () => location.reload());

// iniciamos los push services
(PushService.getInstance()).initialConfiguration();
