import OneSignal, {NotificationClickEvent, NotificationWillDisplayEvent} from "onesignal-cordova-plugin";

import {environment} from "../enviroment/enviroment";
import {StorageService} from "./storage-service";

export class PushService {
    storageService: StorageService;
    private userID: string;

    static instance?: PushService;

    get _userID() {
        return this.userID;
    }

    static getInstance() {
        if (!PushService.instance) PushService.instance = new PushService();

        return PushService.instance;
    }

    constructor() {
        this.storageService = StorageService.getInstance();
        this.userID = '';
    }

    initialConfiguration() {
        // permite mostrar los logs de consola en nativo
        OneSignal.Debug.setLogLevel(6);
        
        // anadimos la api Key 
        OneSignal.initialize(environment.ONE_SIGNAL_APP_ID);
        
        // importamos los event listeners de los push notifications de onesignal
        // esta es el evento que controla la llegada de las notificaciones
        OneSignal.Notifications.addEventListener(
            'foregroundWillDisplay', 
            this.handleNotification.bind(this)
        );

        // evento cuando el usuario hace click a la notificacion
        OneSignal.Notifications.addEventListener(
            'click', 
            this.handleNotification.bind(this)
        );

        // evento que escucha los cambios del permisos para recibir notificaciones
        // OneSignal.Notifications.requestPermission(true).then((success) => {
        //     console.log("Notification permission granted " + success);
        // });

        // obtener el ID del dispositivo suscrito
        OneSignal.User.pushSubscription.getIdAsync()
            .then(userID => this.userID = userID ?? '')
            .catch(console.error)
    }

    async handleNotification(event: NotificationWillDisplayEvent | NotificationClickEvent) {
        const notification = event instanceof NotificationWillDisplayEvent ? 
            event.getNotification() : event.notification;

        await this.storageService.insertMessage(notification);
    }
}