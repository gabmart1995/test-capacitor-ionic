import {Storage} from "@ionic/storage";
import {OSNotification} from "onesignal-cordova-plugin";

export class StorageService {
    static instance: StorageService
    
    private messages: OSNotification[] = [];
    
    storage?: Storage;
    dispatch: (() => void) | null;
    
    constructor() {
        this.dispatch = null;
        this.init();
    }

    static getInstance() {
        if (!StorageService.instance) StorageService.instance = new StorageService();

        return StorageService.instance;
    }

    get _messages() {
        return [...this.messages];
    }

    async init() {
        this.storage = await (new Storage()).create();
        await this.loadMessages();
    }

    async insertMessage(notification: OSNotification) {
        await this.loadMessages();
        
        const exists = this.messages.some(message => message.notificationId === notification.notificationId);
        if (exists) return;

        this.messages.unshift(notification);
        this.storage?.set('messages', this.messages);

        if (this.dispatch) this.dispatch();
    }

    async loadMessages() {
        const messages: OSNotification[] | undefined = await this.storage?.get('messages');
        this.messages = messages ?? [];
    }

    async deleteMessages() {
        await this.storage?.clear();
        this.messages = [];

        if (this.dispatch) this.dispatch();
    }

    subscribe(callback: () => void) {
        this.dispatch = callback;
    }

    unsubscribe() {
        this.dispatch = null;
    }
}