import {PushService} from "../services/push-services";
import {StorageService} from "../services/storage-service";

class InitPage extends HTMLElement {
    isLoading: boolean;
    skeletonElement: HTMLElement | null;
    listElement: HTMLElement | null;
    storeService: StorageService;
    pushService: PushService;

    constructor() {
        super();
        this.storeService = StorageService.getInstance();
        this.pushService = PushService.getInstance();
        this.isLoading = true;
        this.skeletonElement = null;
        this.listElement = null;
    }

    set _isLoading(isLoading) {
        this.isLoading = isLoading;

        if (this._isLoading) {
            this.skeletonElement!.style.display = '';
            this.listElement!.style.display = 'none';

        } else {
            this.skeletonElement!.style.display = 'none';
            this.listElement!.style.display = '';
        }
    }

    get _isLoading() {
        return this.isLoading;
    }
    
    connectedCallback() {
        this.innerHTML = (`
            <ion-header>
                <ion-toolbar>
                    <ion-title>Push Notifications</ion-title>
                    <ion-buttons slot="end">
                        <ion-button id="delete">
                            <ion-icon name="trash-outline" color="danger" slot="icon-only"></ion-icon>
                        </ion-button>
                    </ion-buttons>
                </ion-toolbar>
            </ion-header>
            <ion-content class="ion-padding">
                <h4>User ID: <span id="user-id"></span></h4>
                <ion-list id="content"></ion-list>
                <ion-list id="skeleton-text">
                    ${[0,0,0,0,0,0,0,0,0,0,0].map(_ => (`
                        <ion-item>
                            <ion-label>
                                <h3>
                                    <ion-skeleton-text style="width: 40%" animated></ion-skeleton-text>
                                </h3>
                                <p>
                                    <ion-skeleton-text style="width: 100%" animated></ion-skeleton-text>
                                </p>
                                <p>
                                    <ion-skeleton-text style="width: 100%" animated></ion-skeleton-text>
                                </p>
                            </ion-label>
                        </ion-item>
                    `)).join('')}    
                </ion-list>
            </ion-content>
        `);

        this.onMount();
    }

    onMount() {
        this.listElement = this.querySelector('ion-list#content');
        if (!this.listElement) return;

        this.skeletonElement = this.querySelector('ion-list#skeleton-text');
        if (!this.skeletonElement) return;

        const deleteButton = this.querySelector('ion-button#delete');
        if (!deleteButton) return;

        // capturamos el evento para limpiar el storage
        deleteButton.addEventListener(
            'click', 
            this.storeService.deleteMessages.bind(this.storeService)
        );

        // nos suscribimos al evento push notifications
        this.storeService.subscribe(() => {
            this._isLoading = true;

            // se aplica un timeout porque el sistema necesita leer los 
            // datos de indexedDB y tarda algunos segundos
            setTimeout(() => {
                this._isLoading = false;
                this.render();
            }, 1500);
        });

        // cargamos los mensajes
        this._isLoading = true;
        this.storeService.loadMessages()
            .then(() => {
                
                // se aplica un timeout porque el sistema necesita leer los 
                // datos de indexedDB y tarda algunos segundos
                setTimeout(() => {
                    this._isLoading = false;    
                    this.render();
                    this.renderSubscriptionId();
                }, 1500);
            })
            .catch(console.error);
        }
        
    /** meustra en la interfaz el id de la suscripcion */
    renderSubscriptionId() {
        const span: HTMLSpanElement | null = this.querySelector('span#user-id');
        if (!span) return;

        span.innerHTML = this.pushService._userID;
    }

    render() {
        this.listElement!.innerHTML = '';
        this.listElement!.innerHTML = this.storeService._messages.map(message => (`
            <ion-item>
                <ion-label>
                    <h3>${message.title}</h3>
                    <p>${message.body}</p>
                    <p>${JSON.stringify(message.additionalData)}</p>
                </ion-label>
            </ion-item>
        `)).join('');
    }

    disconnectCallback() {
        this.storeService.unsubscribe();
    }
}

window.customElements.define('app-ionic', InitPage);