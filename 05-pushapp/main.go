/*
*

	Archivo de configuracion del empaquetador EsBuild
	Escrito en GO.

*
*/
package main

import (
	"fmt"
	"log"

	"github.com/evanw/esbuild/pkg/api"
)

func main() {
	ctx, ctxErr := api.Context(api.BuildOptions{
		EntryPoints: []string{"./src/index.js"},
		Bundle:      true,
		Outdir:      "./www",
		Write:       true, // permite a esbuild escribir los cambios
		Tsconfig:    "tsconfig.json",
		Loader: map[string]api.Loader{
			".png": api.LoaderFile,
		},
		MinifyWhitespace:  true, // change a true for production
		MinifySyntax:      true, // change a true for production
		MinifyIdentifiers: true, // change a true for production
	})

	if ctxErr != nil {
		fmt.Println(ctxErr.Errors)
		log.Fatal(ctxErr.Errors)
	}

	// activamos la opcion para observar los cambios
	err := ctx.Watch(api.WatchOptions{})
	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err)
	}

	// levantamos el servidor de desarrollo en el
	// puerto 3000
	_, err = ctx.Serve(api.ServeOptions{
		Port:     3000,
		Servedir: "www",
		Fallback: "./www/index.html", // serve spa Configuration
	})

	if err != nil {
		fmt.Println(err.Error())
		log.Fatal(err)
	}

	fmt.Println("EsBuild: live server in port 3000")
	fmt.Printf("watching for changes ...\n")

	// Returning from main() exits immediately in Go.
	// Block forever so we keep watching and don't exit.
	<-make(chan struct{})
}
