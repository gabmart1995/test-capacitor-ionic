# Vanilla TypeScript + ESBUILD + IONIC

Este proyecto es un prototipo para integracion de Push Notifications usando OneSignal
con Typescript, el objetivo de este proyecto es trabajar con servicios de terceros 
en una aplicacion movil.

## Instalacion de Capacitor
`yarn install`

### Transpilacion y servidor de desarrollo
Para ejecutar esbuild debes tener [GO](https://go.dev) instalado en el equipo y correr el comando <br />
`yarn dev` levanta la transpilacion y un servidor de desarrollo en tiempo real.

## Compilar para android
Para generar el proyecto en android usa el comando `yarn generate_android` creara los 
directorios escritos en JAVA para la compilacion (usar este comando si no tienes el directorio de 
andrioid).<br /><br />

Una vez completado debes correr la sincronizacion del proyecto con `yarn sync` y por ultimo
corres el comando de ejecucion `yarn run_android` para correrlo en emulador de Android Studio para mas info consultar [aqui](https://capacitorjs.com/docs/android)

Se recomienda al configurar el proyecto se requiere de un proyecto en [firebase](https://documentation.onesignal.com/docs/android-firebase-credentials) y sincronizarlo con 
onesignal y seleccionar la plataforma nativa con android, usando ionic framework como medio de comunicacion
entre los dispositivos, para mas info consultar [aqui](https://documentation.onesignal.com/docs/ionic-capacitor-cordova-sdk-setup)